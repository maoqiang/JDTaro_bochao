import Taro, {Component} from '@tarojs/taro'
import Index from './pages/index'

import './app.scss'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {
    version = '1.4.5';
    config = {
        pages: [
            'pages/login/login',
            'pages/mine/mine',
            'pages/find/home',
            'pages/test/test',
            'pages/question/question',
            'pages/test_page/test_page',
            'pages/select_poker/select_poker',
            'pages/select_poker/select_master',
            'pages/answer/detail/detail',
            'pages/answer/detail/eval',
            'pages/find/activitys/activitys',
            'pages/mine/coupon/coupon',
            'pages/find/result/result',
            'pages/mine/feedback/feedback',
            'pages/mine/info/info',
            'pages/mine/mine_answer/mine_answer',
        ],
        window: {
            backgroundTextStyle: 'light',
            navigationBarBackgroundColor: '#fff',
            navigationBarTitleText: 'WeChat',
            navigationBarTextStyle: 'black'
        },
    }

    componentWillMount() {
        this.updateManage();
    }

    componentDidMount() {
        Taro.hideHomeButton();
    }

    updateManage() {
        let that = this;
        let updateManager = Taro.getUpdateManager();
        updateManager.onCheckForUpdate(function (res) {
            // 请求完新版本信息的回调
            console.log(res.hasUpdate)
            if (!res.hasUpdate) {
            }
        })
        // 监听新版本下载成功
        updateManager.onUpdateReady(function () {
            Taro.showModal({
                title: '更新提示',
                content: '新版本已经准备好，是否重启应用？',
                success: function (res) {
                    if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                    } else {
                        that.updateManage()
                    }
                }
            })
        })

        // 监听新版本下载失败
        updateManager.onUpdateFailed(function () {
            Taro.showModal({
                content: '新版本更新失败，是否重试？',
                confirmText: '重试',
                success: function (res) {
                    if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                    }
                }
            })
        })
    }

    // 在 App 类中的 render() 函数没有实际作用
    // 请勿修改此函数
    render() {
        return (
            <Index />
        )
    }

}

Taro.render(<App />, document.getElementById('app'))
