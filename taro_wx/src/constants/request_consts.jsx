export const versionCode='2.2.0';
// const API_URL = 'http://120.55.89.187:8888';
const API_URL = 'https://www.tarottest.com';
const API_ADDR = API_URL.concat('/api/');
export default {
    //占卜洗牌；洗牌动画钱调用此接口，获取当前类型占卜用到的塔罗牌列表数据
    SHUFFLES: API_ADDR.concat('Shuffles'),
    //每日一测首页聚合信息获取（含弹幕）[需token]
    DAILY_TITLES: API_ADDR.concat('DailyTitles'),
    //提交用户每日一测抽到的牌，并获取占卜结果。先调用Shuffles洗牌接口，之后洗牌动画、用户抽牌，再调用此接口。[需token]
    DAILY_ORDERS: API_ADDR.concat('DailyOrders'),
    //微信授权换取用户信息
    WX_OPEN: API_ADDR.concat('WxOpen/Login'),
    //微信登录信息上传
    WX_USERS: API_ADDR.concat('WxUsers'),

    PRE_QUESTIONS: API_ADDR.concat('PreQuestions'),
    FEEDBACKS: API_ADDR.concat('Feedbacks'),
    COUPONS: API_ADDR.concat('Coupons'),
    PAY_CALCS: API_ADDR.concat('PayCalcs'),
    QUESTION_PRE_PAY: API_ADDR.concat('QuestionPrePay'),
    GET_PHONE: API_ADDR.concat('WxOpen/GetPhone'),
    AUGUR_HOMES: API_ADDR.concat('AugurHomes'),
    QUESTIONS_LIST: API_ADDR.concat('Questions/List'),
    QUESTIONS: API_ADDR.concat('Questions'),
    Evaluate: API_ADDR.concat('Evaluates'),
    EvaluateCommit: API_ADDR.concat('Evaluates/Commit'),
    USER_QUESTIONS: API_ADDR.concat('userQuestions'),
    ACTIVITIES: API_ADDR.concat('Activities'),
    PREPAY: API_ADDR.concat('QuestionPrePay/Prepay'),
    HOTS: API_ADDR.concat('Hots'),
    AugurUsers: API_ADDR.concat('AugurUsers'),
}
