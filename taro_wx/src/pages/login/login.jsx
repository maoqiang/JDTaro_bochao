import Taro, {Component} from '@tarojs/taro'
import {View, Textarea, Image} from '@tarojs/components'
import './login.less'
import {versionCode} from "../../constants/request_consts";
import {showToast, wxLogin} from "../../util/app_tool";
import {IsPublish} from "../../constants/enum_consts";
import {logo} from "../../constants/oss_consts";

export default class Mine extends Component {

    config = {
        navigationBarTitleText: '登录',
        navigationStyle: "custom",
        disableScroll: true
    }

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {
    }

    componentDidMount() {
        let a=this.$router;
        let b=this.$router.id;
        let shareId = this.$router.params.id||0;
        let shareUId = this.$router.params.aid||0;
        let version = versionCode.replace(/\./g, '');
        wxLogin({userId: shareUId, version: version}, function (res) {
            showToast('正在加载...');
            setTimeout(function () {
                Taro.redirectTo({
                    url: '../find/home'
                })
            },2000)
        });
    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    render() {
        return (
            <View className='login-view'>
                <Image className='login-logo' src={logo}></Image>
                <View className='logo-text-view'>等你很久了哦！进入塔罗世界，预见你的未来！</View>
            </View>
        )
    }
}
