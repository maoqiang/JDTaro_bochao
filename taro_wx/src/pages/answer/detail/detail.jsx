import Taro, {Component} from '@tarojs/taro'
import {Button, View} from '@tarojs/components'
import './detail.less'
import {HEAD_A, resize, result_poker_icon, rotateFun, voice_icon, yes_ans,SERVICEWECHAT} from "../../../constants/oss_consts";
import {getQuestions} from "../../../action/request_action";
import {formatNumber, showToast, timeAgo} from "../../../util/app_tool";
import {IsPublish,ServicesWechat} from "../../../constants/enum_consts";

let inner = null;
let playStatus = false;
export default class Detail extends Component {
    config = {
        navigationBarTitleText: '问答详情'
    }

    constructor(props) {
        super(props)
        let serviceWechat=Taro.getStorageSync(ServicesWechat);
        //let serviceWechat='123'
        this.state = {
            display: 'none',
            animationList: [],
            isPublish: false,
            holder:0,
            ServicesWXNum:serviceWechat,
            num:-1,
            arr:[1,2,3,4,5],
            arrstr:[{url:"★"},{url:"★"},{url:"★"},{url:"★"},{url:"★"}]
        }
    }

    componentDidMount() {
        if (this.$router.params.type == 'pay') {
        }
        let that = this;
        setTimeout(function(){

               let i=that.state.num;
               let arr = that.state.arrstr;
               arr.map((val, index) => {
                if (index <= i) {
                  val.url='★';
                } else {
                  val.url='☆';
                }
              })
              that.setState({
                  arrstr: arr
              })

        },600)
    }


    subTap() {
        let that = this;
        Taro.requestSubscribeMessage({
            tmplIds: ['__u1wMBCDxL4vXhVViwe2gnrDvqMWeDU7p8zNnApf8U'],
            success(res) {

            },
            fail(res) {

            }
        })
    }
    onCopy() {
        clipboard.
        showToast('复制成功~');
    }
    goEval()
    {
        //url: '../find/result/result?id=' + dailyResultId
        let that = this;
        Taro.navigateTo({
            url: 'eval?id='+that.$router.params.id+'&name='+that.state.data.augurName+"&img="+that.state.data.augurHeadImg
        })
    }
    componentWillMount() {
        let that = this;
        inner = Taro.createInnerAudioContext();
        //let serviceWechat='123'
        inner.onEnded(() => {
            this.playStatus = false;
            this.setState({
                display: 'none'
            })
        });


        let animationList = [];
        for (let i = 0; i < 8; i++) {
            animationList.push({
                animationP: i
            })
        }

        getQuestions({id: this.$router.params.id}, function (data) {
             //data.state=2;
            that.setState({
                animationList: animationList,
                data: data,
                isPublish: Taro.getStorageSync(IsPublish),
                answerUrl: data.answerUrl,
                isSelf: data.isSelf,
                num:(data.evaluateScope-1)
            })  
        })

    }

    componentWillUnmount() {
        // inner.destroy();   
    }
    componentDidShow() {
      let that=this;
      let pages = Taro.getCurrentPages();
      let currPage = pages[pages.length - 1];
      if (currPage.__data__.holder==1 ||currPage.__data__.holder=='1') {
           //showToast('1')
           getQuestions({id: that.$router.params.id}, function (data) {
            
           that.setState({
                data: data,
                num:(data.evaluateScope-1)
            })
            if(data.evaluateScope&&data.evaluateScope>1)
            {
               let i=(data.evaluateScope-1);
               let arr = that.state.arrstr;
               arr.map((val, index) => {
                if (index <= i) {
                  val.url='★';
                } else {
                  val.url='☆';
                }
              })
             
            that.setState({
                  arrstr: arr
              })

            }
        })

        }
        //console.log(that.state.holder);
    }
    componentDidHide() {
        
    }

    play() {
        if (!this.state.isSelf) {
            showToast('只可以阅读自己的测测问答哦！')
            return
        }

        if (this.playStatus) {
            this.playStatus = false;
            inner.stop();
            this.setState({
                display: 'none'
            })
        } else {
            this.playStatus = true;
            inner.src = this.state.answerUrl;
            inner.obeyMuteSwitch = false
            inner.play();
            this.setState({
                display: 'block'
            })
        }
    }

    render() {
        const {display, animationList, data = {}, isPublish, isSelf,holder,ServicesWXNum,num,arr} = this.state;
        let questionCards = data.questionCards || {};


        let firstCard = questionCards.firstCard || {};
        let secondCard = questionCards.secondCard || {};
        let thirdCard = questionCards.thirdCard || {};
        return (
            <View className='detail-view'>
                <View className='item-view'>
                    <Image className='head-img' src={data.wxUserHeadImg || HEAD_A}></Image>
                    <View className='right-view p-relative'>
                        <View className='title-view'>
                            <View className='font-size-14 color-text-4'>{data.wxUserName || ''}</View>
                            {
                                isPublish ?
                                    <View
                                        className='font-size-14 color-text-5'>{formatNumber(data.payable || 0)}{' '}￥</View>
                                    :
                                    ''
                            }
                        </View>
                        <View
                            className='font-size-12 color-text-4'>{data.question || ''}</View>

                        <View
                            className='p-absolute right-0 font-size-8 color-text-6 mt-10'>{timeAgo(data.createTime) || ''}</View>
                    </View>
                </View>

                <View className='result_poker-view'>
                    {
                        firstCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={firstCard.picUrl.concat(resize).concat(rotateFun(firstCard.isInversion))}></Image>
                            :
                            ''
                    }
                    {
                        secondCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={secondCard.picUrl.concat(resize).concat(rotateFun(secondCard.isInversion))}></Image>
                            :
                            ''
                    }
                    {
                        thirdCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={thirdCard.picUrl.concat(resize).concat(rotateFun(thirdCard.isInversion))}></Image>
                            :
                            ''
                    }

                </View>

                {
                    (data.state == 3 || data.state == 4) ?
                        <View className='item-view mt-40'>
                            <Image className='head-img' src={data.augurHeadImg || HEAD_A}></Image>
                            <View className='right-view p-relative'>
                            <View className='title-view'>
                            <View className='font-size-14 color-text-4'>{data.augurName || ''}</View>
                                {
                                    data.state == 3 ?
                                    <View className='font-size-14 text-5'>
                                    <Button onClick={this.goEval.bind(this)} style="border-radius: 2px;height:30px;width:87px;color:#FFFFFF;line-height:30px;position:static;font-size:14px;background-color:#199852;}" className='button-view'>去评价</Button>
                                    </View>:''
                                }
                                
                            </View>
                                
                                <View className='display-flex align-items-center mt-20'>
                                    <View onClick={this.play.bind(this)} className='voice-view'>
                                        <View id="container" style={`display:${display}`}>
                                            {
                                                animationList.map((item, index) => (
                                                    <View key={index} id={"id_" + index} class="wave"></View>
                                                ))
                                            }
                                        </View>
                                        {
                                            display == 'none' ?
                                                <View
                                                    style='border-bottom: 1px #bbbbbb dotted; padding:8px 15px'></View>
                                                :
                                                ''
                                        }
                                    </View>
                                    <View className="font-size-12 color-text-6 ml-15">{data.answerTimingLength}s</View>
                                </View>
                                <View
                                    className='p-absolute right-0 font-size-8 color-text-6'>{timeAgo(data.answerTime) || ''}</View>
                            </View>
                        </View>
                        :
                        <View>
                            <View className='font-size-12 color-text-4 mt-40 text-align-center'>暂时还没有占卜师解答，请耐心等待！</View><Button onClick={this.subTap.bind(this)} className='button-view'>订阅解析通知</Button>
                        </View>
                }

                {
                    data.state == 4 || !isSelf ?
                    <View className='item-view' style="border-top:1px solid #ccc;margin-top:30px;">
                    <View className='right-view p-relative'>
                    <View className='title-view'>
                    <View className='font-size-14 text-4'>我的评价：</View>
                    </View>
                    <View className='font-size-14 text-5' style="margin-left:10px;margin-top:0px;">
                    <View class='starcss'>
                {
                    this.state.arrstr.map((item,index)=>{
                        return(
                        <Text style='' id={'sub'+index} name={'sub'+index} class='wirtestar'>{item.url}</Text>
                        )
                    })
                }
            </View>
                    </View>
                    <View className='font-size-14 text-5' style="margin-left:10px;margin-top:10px;">{data.evaluateStr}</View>
                    </View>
                </View>
                        :''
                }
                <View className='item-view' style="margin-top:0px;">
                    <View className='right-view p-relative'>
                    <View className='title-view'>
                    
                    <View className='feedback-content-view color-text-8'>
                    <View className='p-relative'>
                        <View style="font-size:12px;" editable="false" readonly='readonly' show-confirm-bar={false} className='textarea-view'>{'有任何疑问请加客服微信，更多精彩等你来！\r客服微信号：'+ServicesWXNum}</View>
                        <View className="current-word-num">点击复制</View>
                    </View>
                    </View>
                    </View>
                    
                   </View>
                </View>

            </View>
        )
    }
}
