import Taro, {Component} from '@tarojs/taro'
import {Button, View} from '@tarojs/components'
import './eval.less'
import {HEAD_A, resize, result_poker_icon, rotateFun, voice_icon, yes_ans,SERVICEWECHAT} from "../../../constants/oss_consts";
import {getQuestions, getEvaluates,postEvaluates} from "../../../action/request_action";
import {formatNumber, showToast, timeAgo} from "../../../util/app_tool";
import {IsPublish} from "../../../constants/enum_consts";

let inner = null;
let playStatus = false;
export default class Detail extends Component {
    config = {
        navigationBarTitleText: '评价'
    }

    constructor(props) {
        super(props)
        this.state = {
            headImg: '',
            augurName:'',
            temp1:'',
            textAreaValue:'',
            id:0,
            num:5,
            arr:[1,2,3,4,5],
            arrstr:[{url:"★"},{url:"★"},{url:"★"},{url:"★"},{url:"★"}]
        }
    }

    componentDidMount() {
        let that = this;
        if (this.$router.params.obj) {

        }
    }
    subTap() {
        let that = this;
        Taro.requestSubscribeMessage({
            tmplIds: ['__u1wMBCDxL4vXhVViwe2gnrDvqMWeDU7p8zNnApf8U'],
            success(res) {
            },
            fail(res) {
            }
        })
    }

    componentWillMount() {
        let that = this;
        that.setState({
            headImg:that.$router.params.img,
            augurName:that.$router.params.name,
        })
        getEvaluates({id:''},function (data) {
            //debugger
                that.setState({
                    labdata:data,
                    //headImg:that.$router.params.img,
                    //augurName:that.$router.params.name,
                    id:that.$router.params.id,
                    temp1:''
                })
                //console.log(data);
        })
    }

    componentWillUnmount() {
        // inner.destroy();
    }

    componentDidShow() {
    }

    componentDidHide() {
    }
    inputListener(e) {
        let value = e.detail.value;
        this.setState({
            textAreaValue:value
        })
    }
    wirteTextarea(txt,obj)
    {
        //let that=this;
        let vals = this.state.textAreaValue;
        //let vals=this.state.textAreaValue;
        this.setState({
            textAreaValue:vals+' '+txt
        })
    }
    changeStar(i,obj)
    {
        //let nx=(obj+1)*2;
        //this.setState({num:(nx)});
        let arr = this.state.arrstr;
              arr.map((val, index) => {
                if (index <= i) {
                  val.url='★';
                } else {
                  val.url='☆';
                }
              })
        let nums = (i+1);
              this.setState({
                  arrstr: arr,
                  num:nums
              })

    }

    commitEval()
    {
        let that=this;
        if(that.state.textAreaValue)
        {
            if(that.state.textAreaValue.length<1)
            {
                showToast('评价内容不能为空~');
                return;
            }
        }else
        {
            showToast('评价内容不能为空~');
            return;
        }
        
        let map = {
                star:that.state.num,
                content:that.state.textAreaValue
        }

        postEvaluates(map,'?orderId='+that.state.id,function (data) {

            showToast('提交成功~');
let pages = Taro.getCurrentPages();
let prevPage = pages[pages.length-2];
prevPage.setData({
  holder:1
});
            setTimeout(() => {
                Taro.navigateBack({
                    delta: 1
                  });
            }, 1000);
        });
    }

    render() {
        const {headImg,augurName,temp1,id,textAreaValue,labdata={},num,arr} = this.state;
        
        return (
            <View className='detail-view'>
                <View className='item-view'>
                    <Image className='head-img' src={headImg}></Image>
                    <View className='right-view p-relative'>
                        <View className='title-view'>
                            <View className='font-size-14 color-text-4'>{augurName || ''}</View>
                        </View>
                    </View>
                </View>

                <View className='item-view' style="margin-top:20px;">
                    <View className='title-view'>
                    <View className='font-size-14 text-4' style='padding-top:16px;'>综合评分 </View>
                    <View className='font-size-14 text-5' style="margin-left:20px;">
                    <View class='starcss'>
                {
                    this.state.arrstr.map((item,index)=>{
                        return(
                        <Text style='' onClick={this.changeStar.bind(this,index)} id={'sub'+index} name={'sub'+index} class='wirtestar'>{item.url}</Text>
                        )
                    })
                }
            </View>
                    </View>
                    </View>
                    
                </View>

<View className='item-view' style="margin-top:20px;">
                    <View className='title-view'>
                    <View className='font-size-14 text-4'>评论内容</View>
                    </View>
                </View>

                <View className='item-view' style="border-top:1px solid #ccc;margin-top:20px;">
                    <View className='title-view'>

                    <View className='feedback-content-view font-size-12 color-text-8'>
                    <View className='p-relative'>
                        <Textarea style="font-size:12px;" show-confirm-bar={false} onInput={this.inputListener.bind(this)}
                                  maxlength={100} className='textarea-view' value={textAreaValue}
                                  placeholder='请从下方点选标签或直接输入'></Textarea>
                        
                    </View>
                </View>

                    </View>
                </View>

                <View className='item-view' style="">
                    <View className=''>
                    {
                        labdata.map((item, index) => (
                        <Text onClick={this.wirteTextarea.bind(this,item.word)} key={index} style="padding:4px;border-radius:6px;height:34px;width:auto;color:#199852;line-height:34px;font-size:12px;border:1px solid #199852;text-align: center;margin-left:4px;margin-right:4px}" className=''>{item.word}</Text>
                        ))
                    }
                    </View>
                </View>

                <View className='item-view' style="margin-top:10px;text-align: center">
                <Button onClick={this.commitEval.bind(this)} style="border-radius: 2px;height:40px;width:90%;color:#FFFFFF;line-height:40px;font-size:14px;background-color:#199852;text-align: center;}" className='button-view'>提交</Button>
                </View>

            </View>
        )
    }
}
