import Taro, {Component} from '@tarojs/taro'
import {Button, View} from '@tarojs/components'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import './detail.less'
import {HEAD_A, resize, result_poker_icon, rotateFun, voice_icon, yes_ans,SERVICEWECHAT} from "../../../constants/oss_consts";
import {getQuestions} from "../../../action/request_action";
import {formatNumber, showToast, timeAgo} from "../../../util/app_tool";
import {IsPublish} from "../../../constants/enum_consts";

let inner = null;
let playStatus = false;
export default class Detail extends Component {
    config = {
        navigationBarTitleText: '问答详情'
    }

    constructor(props) {
        super(props)
        this.state = {
            display: 'none',
            animationList: [],
            isPublish: false
        }
    }

    componentDidMount() {
        if (this.$router.params.type == 'pay') {

        }
    }


    subTap() {
        let that = this;
        Taro.requestSubscribeMessage({
            tmplIds: ['__u1wMBCDxL4vXhVViwe2gnrDvqMWeDU7p8zNnApf8U'],
            success(res) {

            },
            fail(res) {

            }
        })
    }

    componentWillMount() {
        let that = this;
        inner = Taro.createInnerAudioContext();


        inner.onEnded(() => {
            this.playStatus = false;
            this.setState({
                display: 'none'
            })
        });


        let animationList = [];
        for (let i = 0; i < 8; i++) {
            animationList.push({
                animationP: i
            })
        }

        getQuestions({id: this.$router.params.id}, function (data) {
            that.setState({
                animationList: animationList,
                data: data,
                isPublish: Taro.getStorageSync(IsPublish),
                answerUrl: data.answerUrl,
                isSelf: data.isSelf
            })

        })

    }

    componentWillUnmount() {
        // inner.destroy();
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    play() {
        if (!this.state.isSelf) {
            showToast('只可以阅读自己的测测问答哦！')
            return
        }

        if (this.playStatus) {
            this.playStatus = false;
            inner.stop();
            this.setState({
                display: 'none'
            })
        } else {
            this.playStatus = true;
            inner.src = this.state.answerUrl;
            inner.obeyMuteSwitch = false
            inner.play();
            this.setState({
                display: 'block'
            })
        }
    }

    render() {
        const {display, animationList, data = {}, isPublish, isSelf} = this.state;
        let questionCards = data.questionCards || {};


        let firstCard = questionCards.firstCard || {};
        let secondCard = questionCards.secondCard || {};
        let thirdCard = questionCards.thirdCard || {};
        return (
            <View className='detail-view'>
                <View className='item-view'>
                    <Image className='head-img' src={data.wxUserHeadImg || HEAD_A}></Image>
                    <View className='right-view p-relative'>
                        <View className='title-view'>
                            <View className='font-size-14 color-text-4'>{data.wxUserName || ''}</View>
                            {
                                isPublish ?
                                    <View
                                        className='font-size-14 color-text-5'>{formatNumber(data.payable || 0)}{' '}￥</View>
                                    :
                                    ''
                            }
                        </View>
                        <View
                            className='font-size-12 color-text-4'>{data.question || ''}</View>

                        <View
                            className='p-absolute right-0 font-size-8 color-text-6 mt-10'>{timeAgo(data.createTime) || ''}</View>
                    </View>
                </View>

                <View className='result_poker-view'>
                    {
                        firstCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={firstCard.picUrl.concat(resize).concat(rotateFun(firstCard.isInversion))}></Image>
                            :
                            ''
                    }
                    {
                        secondCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={secondCard.picUrl.concat(resize).concat(rotateFun(secondCard.isInversion))}></Image>
                            :
                            ''
                    }
                    {
                        thirdCard.picUrl ?
                            <Image className='result_poker-img'
                                   src={thirdCard.picUrl.concat(resize).concat(rotateFun(thirdCard.isInversion))}></Image>
                            :
                            ''
                    }


                </View>

                {
                    data.state == 3 ?
                        <View className='item-view mt-40'>
                            <Image className='head-img' src={data.augurHeadImg || HEAD_A}></Image>
                            <View className='right-view p-relative'>
                                <View className='font-size-14 color-text-4'>{data.augurName || ''}</View>
                                <View className='display-flex align-items-center mt-20'>
                                    <View onClick={this.play.bind(this)} className='voice-view'>
                                        <View id="container" style={`display:${display}`}>
                                            {
                                                animationList.map((item, index) => (
                                                    <View key={index} id={"id_" + index} class="wave"></View>
                                                ))
                                            }
                                            <View className='right-view p-relative'>
                                                去评价
                                            </View>
                                        </View>
                                        {
                                            display == 'none' ?
                                                <View
                                                    style='border-bottom: 1px #bbbbbb dotted; padding:8px 15px'></View>
                                                :
                                                ''
                                        }
                                    </View>
                                    <View className="font-size-12 color-text-6 ml-15">{data.answerTimingLength}s</View>
                                </View>
                                <View
                                    className='p-absolute right-0 font-size-8 color-text-6'>{timeAgo(data.answerTime) || ''}</View>
                            </View>
                        </View>
                        :
                        <View className='font-size-12 color-text-4 mt-40 text-align-center'>暂时还没有占卜师解答，请耐心等待！</View>
                }

                {
                    data.state == 3 || !isSelf ?
                        ''
                        :
                        <Button onClick={this.subTap.bind(this)} className='button-view'>订阅解析通知</Button>

                }

<View className='item-view' style="border-top:1px solid #ccc;margin-top:20px;">
                    <View className='right-view p-relative'>
                    <View className='title-view'>
                    <View className='font-size-14 text-4'>我的评价：</View>
                    <View className='font-size-14 text-5'>评论内容</View>
                    </View>
                    </View>
                </View>


                <View className='item-view' style="margin-top:10px;">
                    <View className='right-view p-relative'>
                    <View className='title-view'>
                    <View className='font-size-14 text-4'>微信号：fenstein&nbsp;&nbsp;&nbsp;&nbsp;
                    <CopyToClipboard  text='fenstein'>
                     <a href="javascript:;">复制</a>
                    </CopyToClipboard>
                     </View>
                    <View className='font-size-14 text-5'>有任何疑问请加微信<br/><br/>更多活动等你来！</View>
                    </View>
                    </View>
                </View>

            </View>
        )
    }
}
