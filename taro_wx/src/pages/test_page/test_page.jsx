import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Button, ScrollView} from '@tarojs/components'
import './test_page.less'
import {IsPublish, MLoginInfo, MuserInfo} from "../../constants/enum_consts";
import {AtTabs, AtTabsPane} from "taro-ui";
// import "taro-ui/dist/style/index.scss"


import {EPage} from "eft-cool-taro-ui";

import {arrow_right, HEAD_A, home1_04} from "../../constants/oss_consts";
import {formatNumber, showToast, timeAgo} from "../../util/app_tool";
import {getQuestions, getQuestionsList, getUserQuestions} from "../../action/request_action";


const tabList = [{title: '感情'}, {title: '婚姻'}, {title: '事业'}, {title: '财运'}];
let pageIndex = 1;

export default class Test_page extends Component {

    config = {
        navigationBarTitleText: '塔罗测测',
        enablePullDownRefresh: true,
        backgroundTextStyle: 'dark',
        onReachBottomDistance: 50
    }

    constructor(props) {
        super(props);
        this.state = {
            mLoginInfo: {},
            current: 0,
            noMore: false,
            hasMore: true,
            scrollTop: 0,
            list: [],
            noAut: false,
            noData: false,
            hasMoreText: '稍安勿躁,加载中...',
            isPublish: false
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        let mLoginInfo = Taro.getStorageSync(MLoginInfo);

        this.setState({
            isPublish: Taro.getStorageSync(IsPublish)
        })
        // this.refresh();

        let list1 = []
        for (let i = 0; i < 3; i++) {
            list1.push({
                text: '123' + i
            })
        }

        this.setState({
            mUserInfo: mLoginInfo.userInfo,
            mLoginInfo: mLoginInfo,
            list1: list1
        })
    }

    componentWillUnmount() {
    }

    componentDidShow() {
        this.onPullDownRefresh()
    }

    componentDidHide() {
    }

    handleClick(value) {
        this.setState({
            current: value
        })
    }

    getData = (pIndex = pageIndex, callback) => {
        let that = this;
        getQuestionsList({'PageIndex': pIndex}, function (data) {
            let list = data.items;

            callback({list: list});
        });
    };

    onPullDownRefresh() {
        let that = this;
        that.getData(0, function (res) {
            that.setState({
                ...res,
                noData: res.list.length < 1,
                refreshStatus: 2,
            });
            Taro.stopPullDownRefresh()
        });
    }

    onReachBottom() {
        console.log("页面上拉触底事件的处理函数")
    }

    detailBtn(item, e) {
        if (item.state == 1) {
            Taro.navigateTo({
                url: '/pages/select_poker/select_poker?type=answer&sourceName=test&questions=' + item.question + '&orderId=' + item.id
            })
        } else {
            Taro.navigateTo({
                url: '/pages/answer/detail/detail?id=' + item.id
            })
        }

    }

    startTest(item = {}) {
        let questions = item.question || '';
        let id = item.id || '';

        if (this.state.isPublish) {
            Taro.navigateTo({
                url: '../select_poker/select_poker?sourceName=test&questions=' + questions + '&id=' + id
            })
        } else {
            Taro.navigateTo({
                url: '../select_poker/select_poker?sourceName=find'
            })
        }

    }


    refresh = () => {
        let that = this;
        pageIndex = 0;
        this.setState({
            refreshStatus: 1,
            hasMore: true,
            noMore: false
        });

        that.getData(0, function (res) {
            pageIndex++;
            if (res.list.length < 20) {
                that.loadMore();
            }
            that.setState({
                ...res,
                noData: res.list.length < 1,
                refreshStatus: 2,
            });
        });
    }

    loadMore = () => {
        let that = this;
        that.getData(pageIndex, function (res) {
            pageIndex++;
            if (res.list.length > 0) {
                that.setState({
                    list: that.state.list.concat(res.list),
                    hasMore: true,
                    noMore: false
                });
            } else {
                that.setState({
                    hasMore: false,
                    noMore: true,
                    hasMoreText: '没有更多数据了'
                });
            }
        });
    }

    handleScrollEnd = (e) => {
        this.setState({
            scrollTop: e.detail.scrollTop
        })
    }

    initListView(list) {

        if (list) {

            return <ScrollView scrollY={true} className='tab-view'>
                {
                    list.map((item, index) => (
                        <View onClick={this.startTest.bind(this, item)} className='display-flex justify-contentr-sb'
                              style='padding 10px 20px;margin-top:10px;margin-bottom:10px'>
                            <View key={index} className='problem-item-view  plr-10'>{item.question}</View>
                            <Image className='arrow_icon-view plr-10' src={arrow_right} />
                        </View>
                    ))
                }
            </ScrollView>
        }
    }


    receiveCouponsBtn() {
        Taro.navigateTo({
            url: '/pages/find/activitys/activitys'
        })
    }


    render() {
        debugger
        const {
            list1,
            hasMoreText,
            noMore,
            hasMore,
            refreshStatus,
            scrollTop,
            list,
            isPublish,
            mLoginInfo = {}
        } = this.state;

        let hotTopics = mLoginInfo.hotTopics || {};
        let typeItems = hotTopics.typeItems || [];
        let tabList = [];
        for (let i = 0; i < typeItems.length; i++) {
            tabList.push({
                title: typeItems[i].typeName
            })
        }
        const refresherConfig = {
            recoverTime: 300,
            refreshTime: 1000
        };
        return (
            <View className='test-page-view'>

                <View className='top-view'>
                    <View className='title-button'>
                        {
                            isPublish ?
                                <View className='font-size-14'>{mLoginInfo.questionHomeCouponTitle}</View>
                                : ''
                        }

                        {
                            isPublish ?
                                <View onClick={this.receiveCouponsBtn.bind(this)} className='button-view'>领优惠券</View>
                                : ''
                        }
                    </View>

                    <View onClick={this.startTest.bind(this, undefined)} className='display-flex align-items-center border-bottom-1-purple' style='padding:10px 0;'>
                        <Image style='position:absolute;top:53px;z-index:0;height:90px;width:100vw' mode='aspectFill'
                               src={mLoginInfo.questionHomeBackImg}></Image>
                        <View className='flex1 ml-15' style='z-index:1'>
                            <View className='font-size-14  pd-10 fff'>{mLoginInfo.questionHomeTitle || ''}</View>
                            <View className='font-size-12 pd-10 fff'>{mLoginInfo.questionHomeDesc || ''}</View>
                        </View>

                        <View style='z-index:1;margin-right:15px'
                              className='quiz-view'>提<Text className='ml-5'>问</Text></View>
                    </View>


                    <View className='border-bottom-1-purple'>
                        {
                            typeItems.length > 3 ?
                                <AtTabs current={this.state.current} tabList={tabList}
                                        onClick={this.handleClick.bind(this)}>
                                    <AtTabsPane current={this.state.current} index={0}>
                                        {this.initListView(typeItems[0].items, 0)}
                                    </AtTabsPane>
                                    <AtTabsPane current={this.state.current} index={1}>
                                        {this.initListView(typeItems[1].items, 1)}
                                    </AtTabsPane>
                                    <AtTabsPane current={this.state.current} index={2}>
                                        {this.initListView(typeItems[2].items, 2)}
                                    </AtTabsPane>
                                    <AtTabsPane current={this.state.current} index={3}>
                                        {this.initListView(typeItems[3].items, 3)}
                                    </AtTabsPane>
                                </AtTabs>
                                :
                                ''
                        }
                    </View>

                </View>


                {/*<EPage*/}
                {/*    onRefresh={this.refresh}*/}
                {/*    onLoadMore={this.loadMore}*/}
                {/*    noMore={noMore}*/}
                {/*    hasMore={hasMore}*/}
                {/*    hasMoreText={hasMoreText}*/}
                {/*    refresherConfig={refresherConfig}*/}
                {/*    refreshStatus={refreshStatus}*/}
                {/*    scrollTop={scrollTop}*/}
                {/*    onScrollEnd={this.handleScrollEnd}*/}
                {/*    className='EPage-view'*/}
                {/*>*/}
                <View className='list-view'>
                    <View className='title-item pt-10'>
                        <Image className='image-item-title' src={home1_04}></Image>
                        <Text className='text-item-title'>最新问答</Text>
                    </View>
                    {list.map((item, index) => (
                        <View key={index} className='mt-10'>
                            <View className='item-title-view plr-10'>
                                {/*<Image className='ans-img' src={item.isAnswered ? yes_ans : no_ans}></Image>*/}
                                {/*<Image className='wen-img' style={`opacity:${item.isAnswered ?'0.9':'0.4'}`} src={wen}></Image>*/}

                                {

                                    item.state == 0 ?
                                        <View className='label-status-view color-bg-7'>待支付</View>
                                        :
                                        item.state == 1 ?
                                            <View className='label-status-view color-bg-5'>待占卜</View>
                                            :
                                            item.state == 2 ?
                                                <View className='label-status-view color-bg-4'>待回答</View>
                                                :
                                                item.state == 3 ?
                                                    <View className='label-status-view color-bg-6'>已完成</View>
                                                    :
                                                    ''
                                }

                            </View>
                            <View className='border-lr'></View>
                            <View onClick={this.detailBtn.bind(this, item)} className='item-bottom-view pd-10'>
                                <View>
                                    <Image className='head-img' src={item.wxUserHeadImg || HEAD_A}></Image>
                                </View>

                                <View className='width100 ml-10'>
                                    <View className='d-flex justify-contentr-sb'>
                                        <View className=' font-size-14 color-text-4'>{item.wxUserName || ''}</View>
                                        {
                                            isPublish ?
                                                <View
                                                    className=' font-size-14 color-text-5'>{formatNumber(item.priced || 0)}{' '}￥</View>
                                                :
                                                <View></View>
                                        }
                                    </View>
                                    <View
                                        className='content-view font-size-12 color-text-4'>{item.question || ''}</View>

                                    <View className='fr font-size-8 color-text-6'>{timeAgo(item.createTime)}</View>
                                </View>
                            </View>
                        </View>
                    ))}
                </View>

                {/*</EPage>*/}

            </View>
        )
    }
}
