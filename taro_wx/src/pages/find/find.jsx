import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Swiper, SwiperItem, Button, Input} from '@tarojs/components'
import './home.less'
import {arrow_right, four, home_bg, home_mine, home_title, one, three, two, home_order, home_anwser, home_lqzx} from "../../constants/oss_consts";
import {getUserInfoTool, interceptArray} from "../../util/app_tool";
import {IsPublish, MuserInfo} from "../../constants/enum_consts";
import {getAugurHomes, getDailyTitles} from "../../action/request_action";
import {versionCode} from "../../constants/request_consts";
import {AtModal, AtModalAction, AtModalContent, AtModalHeader} from "taro-ui";

export default class HOME extends Component {

    config = {
        navigationBarTitleText: '塔罗测测'
    }

    constructor(props) {
        super(props);
        this.state = {
            userId: '0',
            isWxAuthed: true,
            data: {},
            swiperList: [],
            isPublish: false,
            isOpened: false
        }
    }

    componentWillMount() {
        let that = this;
        let mUserInfo = Taro.getStorageSync(MuserInfo);
        getDailyTitles({}, function (res) {
            let activities = interceptArray(res.activities, 2)
            that.setState({
                data: res,
                swiperList: activities,
                mUserInfo: mUserInfo,
                isPublish: Taro.getStorageSync(IsPublish)
            })
        });
    }

    componentDidMount() {

    }

    componentDidShow() {
    }

    componentDidHide() {
    }


    getUserInfo(e) {
        let that = this;
        getUserInfoTool(e.detail, function (res) {
            that.setState({
                mUserInfo: Taro.getStorageSync(MuserInfo)
            }, () => {
                that.everyDayTest();
            })
        })
    };

    everyDayTest() {
        getDailyTitles({}, function (data) {
            let dailyResultId = data.dailyResultId;
            if (dailyResultId && dailyResultId.length > 10) {
                Taro.navigateTo({
                    url: '../find/result/result?id=' + dailyResultId
                })
            } else {
                Taro.navigateTo({
                    url: '../select_poker/select_poker?sourceName=find'
                })
            }
        });
    }

    goodsButton(indexItem, indexLi, e) {
        let swiperList = this.state.swiperList;
        let goodItem = {};
        if (indexItem == -1 && indexLi == -1) {
            goodItem = swiperList[0]
        } else {
            goodItem = swiperList[indexItem][indexLi]
        }

        Taro.navigateTo({
            url: './activitys/activitys'
        })
    }

    openMineBtn() {
        Taro.navigateTo({
            url: '../mine/mine'
        })
    }
    openTestBtn() {
        Taro.navigateTo({
            url: '../test/test'
        })
    }

    settingClick(index, e) {
        switch (index) {
            case 0:
                this.setState({isOpened: true})
                break;
            case 1:
                this.setState({isOpened: false})
                break;
        }
    }

    render() {
        const {mUserInfo, isPublish,isOpened} = this.state;
        return (
            <View className='home-view'>

                <AtModal isOpened={isOpened}>
                    <AtModalHeader></AtModalHeader>
                    <AtModalContent>
                        <View>将跳转至客服咨询，客服人员会为您预约塔罗师进行一对一占卜！</View>
                    </AtModalContent>
                    <AtModalAction>
                        <Button onClick={this.settingClick.bind(this, 1)}>取消</Button>
                        <Button open-type='contact' onClick={this.settingClick.bind(this, 1)}>确定</Button>
                    </AtModalAction>
                </AtModal>

                <Image className='top-image p-absolute' src={home_bg}></Image>
                {
                    isPublish ?
                        <View className='p-relative content-view'>
                            <Image onClick={this.openMineBtn.bind(this)} mode='widthFix' className='mine-image mt-20'
                                   src={home_mine} />
                            <Image mode='widthFix' className='title-image' src={home_title} />

                            <View className='p-relative'>
                                <Button className='button-img ' openType='getUserInfo' onGetUserInfo={this.getUserInfo.bind(this)}
                                        type='primary'
                                        lang='zh_CN'>
                                    <View className='p-absolute text-view'>
                                        <View>每日一测</View>
                                        <View className='font-size-12'>ON THE DAILY</View>
                                    </View>
                                    <Image mode='widthFix' className='item-image' src={one}></Image>
                                </Button>
                            </View>


                            <View onClick={this.openTestBtn.bind(this)} className='p-relative'>
                                <View className='p-absolute text-view'>
                                    <View>塔罗占卜</View>
                                    <View className='font-size-12'>TAROT ANSWER</View>
                                </View>
                                <Image mode='widthFix' className='item-image' src={two}></Image>
                            </View>

                            <View onClick={this.settingClick.bind(this,0)} className='p-relative'>
                                <Button className='button-img '>
                                    <View className='p-absolute text-view'>
                                        <View>在线占卜</View>
                                        <View className='font-size-12'>ONLINE DIVINATION</View>
                                    </View>
                                    <Image mode='widthFix' className='item-image' src={three}></Image>
                                </Button>
                            </View>
                            <View className='p-relative'>
                                <View className='p-absolute text-view'>
                                    <View className='right-0'><Text className='expect-text'>敬请期待!</Text>转运饰品</View>
                                    <View className='font-size-12'>TRANSSHIPMENT ACCESSORIES</View>
                                </View>
                                <Image mode='widthFix' className='item-image' src={four}></Image>
                            </View>
                            
                        </View>
                        :
                        mUserInfo.isWxAuthed ?
                            <Button onClick={this.everyDayTest.bind(this)} className='p-absolute dayImage'></Button>
                            :
                            <Button openType='getUserInfo' onGetUserInfo={this.getUserInfo.bind(this)}
                                    type='primary'
                                    lang='zh_CN' className='p-absolute dayImage'></Button>
                }
            </View>
        )
    }
}
