import Taro, {Component} from '@tarojs/taro'
import {View, Image, Button, Text} from '@tarojs/components'
import './activitys.less'
import {getActivities, getDailyOrders} from "../../../action/request_action";
import {huodong_bg} from "../../../constants/oss_consts";
import {MLoginInfo, UserId} from "../../../constants/enum_consts";
import {EPage} from "eft-cool-taro-ui";

export default class Activitys extends Component {

    config = {
        navigationBarTitleText: '活动'
    }

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            inviteImgUrl:''
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        let that = this;
        getActivities({}, function (data) {
            that.setState({
                data: data,
                inviteImgUrl:data.inviteImgUrl
            })
        });
    }

    componentWillUnmount() {
    }

    componentDidShow() {

    }

    componentDidHide() {
    }

    goButton(e) {
        Taro.switchTab({
            url: '../../mine/mine'
        })
    }

    onShareAppMessage(res) {
        let inviteImgUrl =  this.state.inviteImgUrl;
        let shareTitle = Taro.getStorageSync(MLoginInfo).shareTitle;
        debugger
        return {
            title: shareTitle,
            path: '/pages/login/login?id=' + Taro.getStorageSync(UserId),
            imageUrl: inviteImgUrl+'?time='+Date.now(),
        }
    }

    render() {
        const {data} = this.state;
        return (
            <View className='ractivitys-view'>
                <Image className='img-view' mode='widthFix' src={huodong_bg}></Image>
                <View className='content-view'>

                    {/* <View className='card-view mt-20 font-size-10 color-text-14'>
                        <View className='font-size-16 '>完善个人信息领取测测问答5折券</View>
                        <View className='display-flex mt-5'>
                            <View className='flex1'></View>
                            {
                                data.isPhoneTaskCompeleted ?
                                    <Button className='button-view' style='background-color:#a3a18f'>已领取</Button>
                                    :
                                    <Button onClick={this.goButton.bind(this)} className='button-view '>去完善</Button>
                            }
                        </View>
                    </View> */}
                    <View className='card-view mt-20 font-size-14 color-text-14'>
                        <View className='font-size-16 mt-10'>邀请新用户获测测问答1元体验券</View>

                        {data.activityRole.map((item, index) => (
                            <View key={index} className=' mt-10 '>{item}</View>
                        ))}
                        <View className='display-flex mt-10'>
                            <View className='flex1'>已邀请<Text
                                className='color-text-5 font-size-18'>{data.inviteCount}</Text>位</View>
                            <View className='flex1'>已赠券<Text
                                className='color-text-5 font-size-18'>{data.activityCouponCount}</Text>帐</View>
                            <Button openType='share' className='button-view'>去邀请</Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
