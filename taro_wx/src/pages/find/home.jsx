import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Swiper, SwiperItem, Button, Input} from '@tarojs/components'
//import TaroPop from '@components/taroPop'
//import TaroPop from 'components/taroPop'
import './home.less'
import {arrow_right, find_bg, four, home_bg, home_mine, home_title, one, three, two, home_anwser, home_lqzx, home_surpris,home_freeicon} from "../../constants/oss_consts";
import {getUserInfoTool, interceptArray,wxLogin} from "../../util/app_tool";
import {IsPublish, MuserInfo} from "../../constants/enum_consts";
import {getAugurHomes, getDailyTitles} from "../../action/request_action";
import {versionCode} from "../../constants/request_consts";
import {AtModal, AtModalAction, AtModalContent, AtModalHeader} from "taro-ui";

export default class HOME extends Component {

    config = {
        navigationBarTitleText: '塔罗测测'
    }

    constructor(props) {
        super(props);
        this.state = {
            userId: '0',
            isWxAuthed: true,
            data: {},
            swiperList: [],
            isPublish: false,
            isOpened: false,
            isNewUser:false
        }
    }

    componentWillMount() {
        let that = this;
        let mUserInfo = Taro.getStorageSync(MuserInfo);
        getDailyTitles({}, function (res) {
            let activities = interceptArray(res.activities, 2)
            that.setState({
                data: res,
                swiperList: activities,
                mUserInfo: mUserInfo,
                isPublish: Taro.getStorageSync(IsPublish)
            })
        });

        let shareId = this.$router.params.id||0;
        let version = versionCode.replace(/\./g, '');
        wxLogin({userId: shareId, version: version}, function (res) {
            //debugger
            if(res && res.isNewUser)
            {
                 that.setState({
                     //isNewUser:res.isNewUser
                     isNewUser:true
                 })
            }
        });
    }

    componentDidMount() {
        let that = this;
        //let taroPop = this.refs.taroPop;
    }

    componentDidShow() {
    }

    componentDidHide() {
    }


    getUserInfo(e) {
        let that = this;
        getUserInfoTool(e.detail, function (res) {
            that.setState({
                mUserInfo: Taro.getStorageSync(MuserInfo)
            }, () => {
                that.everyDayTest();
            })
        })
    };

    everyDayTest() {
        getDailyTitles({}, function (data) {
            let dailyResultId = data.dailyResultId;
            if (dailyResultId && dailyResultId.length > 10) {
                Taro.navigateTo({
                    url: '../find/result/result?id=' + dailyResultId
                })
            } else {
                Taro.navigateTo({
                    url: '../select_poker/select_poker?sourceName=find'
                })
            }
        });
    }

    goodsButton(indexItem, indexLi, e) {
        let swiperList = this.state.swiperList;
        let goodItem = {};
        if (indexItem == -1 && indexLi == -1) {
            goodItem = swiperList[0]
        } else {
            goodItem = swiperList[indexItem][indexLi]
        }

        Taro.navigateTo({
            url: './activitys/activitys'
        })
    }

    openMineBtn() {
        Taro.navigateTo({
            url: '../mine/mine'
        })
    }
    openAnswerBtn() {
        Taro.navigateTo({
            url: '../mine/mine_answer/mine_answer'
            //url: '../test/test'
        })
    }
    openLQZXBtn() {
        Taro.navigateTo({
            url:'../find/activitys/activitys'
        })
    }
    openTestBtn() {
        Taro.navigateTo({
            url: '../test/test'
        })
    }
    hideSurpris() {
        let that = this;
        //this.refs.taroPop.close()
                        that.setState({
                                isNewUser:false
                                //isNewUser:true
                            })
    }
    openList() {
        let that = this;
        //this.refs.taroPop.close()
                        that.setState({
                                isNewUser:false
                                //isNewUser:true
                            })
        Taro.navigateTo({
            url: '../test/test'
        })
    }
    settingClick(index, e) {
        switch (index) {
            case 0:
                this.setState({isOpened: true})
                break;
            case 1:
                this.setState({isOpened: false})
                break;
        }
    }

    render() {
        const {mUserInfo, isPublish, isOpened,isNewUser} = this.state;
        return (
            <View className='home-view'>
            <View ref="taroPop" className={this.state.isNewUser?'home_surpris':'home_surprisnone'}>
            <View style="" className="home_bgblack">
               <Image mode='widthFix' onClick={this.openList.bind(this)} style="opacity:1" className='' src={home_surpris}></Image>
               <View onClick={this.hideSurpris.bind(this)} style="position: absolute;top:380px;z-index: 9;width:100%;height: 80px;"></View>                            
            </View>
            </View>
                <AtModal isOpened={isOpened}>
                    <AtModalHeader></AtModalHeader>
                    <AtModalContent>
                        <View>将跳转至客服咨询，客服人员会为您预约塔罗师进行一对一占卜！</View>
                    </AtModalContent>
                    <AtModalAction>
                        <Button onClick={this.settingClick.bind(this, 1)}>取消</Button>
                        <Button open-type='contact' onClick={this.settingClick.bind(this, 1)}>确定</Button>
                    </AtModalAction>
                </AtModal>
                
                {
                    isPublish ?
                        <Image className='top-image p-absolute' mode='heightFix' src={home_bg}></Image>
                        :
                        <Image className='top-image p-absolute' mode='heightFix' src={find_bg}></Image>

                }
                {
                    isPublish ?
                        <View className='p-relative content-view' style="padding-top: 0px;">
<View className='p-relative' style="height:80px;">
    <View style="position: absolute;left:4px;top:4px;width:60px;">
    <Image onClick={this.openMineBtn.bind(this)} mode='widthFix' style="height:54px;width:54px;" className='mine-image mt-20' src={home_mine} />
    </View>
    <View style="color:#fff;position:absolute;top:60px;left:15px;font-size:28rpx;">我的</View>
    <View style="position: absolute;right:0px;top:4px;width:60px;">
    <Image onClick={this.openAnswerBtn.bind(this)} mode='widthFix' style="height:54px;width:54px;" className='mine-image mt-20' src={home_anwser}/>
    </View>
    <View style="position:absolute;top:60px;color:#fff;right:5px;font-size:28rpx;">我的问答</View>
</View>


                            <Image mode='widthFix' className='title-image' src={home_title} />

                            <View className='p-relative'>
                                <Button className='button-img ' openType='getUserInfo'
                                        onGetUserInfo={this.getUserInfo.bind(this)}
                                        type='primary'
                                        lang='zh_CN'>
                                    <View className='p-absolute text-view'>
                                        <View>每日一测</View>
                                        <View className='font-size-12'>ON THE DAILY</View>
                                    </View>

                                    <Image mode='widthFix' style="width:110rpx !important;height:110rpx !important;position:absolute;right:0%;top:8%;" className='' src={home_freeicon} />
                                    <Image mode='widthFix' className='item-image' src={one}></Image>
                                </Button>
                            </View>


                            <View onClick={this.openTestBtn.bind(this)} className='p-relative'>
                                <View className='p-absolute text-view'>
                                    <View>塔罗占卜</View>
                                    <View className='font-size-12'>TAROT ANSWER</View>
                                </View>
                                <Image mode='widthFix' className='item-image' src={two}></Image>
                            </View>

                            <View onClick={this.settingClick.bind(this, 0)} className='p-relative'>
                                <Button className='button-img '>
                                    <View className='p-absolute text-view'>
                                        <View>在线占卜</View>
                                        <View className='font-size-12'>ONLINE DIVINATION</View>
                                    </View>
                                    <Image mode='widthFix' className='item-image' src={three}></Image>
                                </Button>
                            </View>
                            <View className='p-relative'>
                                <View className='p-absolute text-view'>
                                    <View className='right-0'><Text className='expect-text'>敬请期待!</Text>转运饰品</View>
                                    <View className='font-size-12'>TRANSSHIPMENT ACCESSORIES</View>
                                </View>
                                <Image mode='widthFix' className='item-image' src={four}></Image>
                            </View>
                            <View className='p-relative' style="margin-top:10px;text-align: center;padding-left:16%;">
                                <Image mode='widthFix' onClick={this.openLQZXBtn.bind(this)} className='item-image' style="width:80%;" src={home_lqzx}></Image>
                            </View>
                        </View>
                        :
                        mUserInfo.isWxAuthed ?
                            <Button onClick={this.everyDayTest.bind(this)} className='p-absolute dayImage'></Button>
                            :
                            <Button openType='getUserInfo' onGetUserInfo={this.getUserInfo.bind(this)}
                                    type='primary'
                                    lang='zh_CN' className='p-absolute dayImage'></Button>
                }
            </View>
        )
    }
}
