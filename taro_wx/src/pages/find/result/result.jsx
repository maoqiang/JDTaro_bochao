import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image} from '@tarojs/components'
import './result.less'
import {result_btn, result_jd, result_poker_icon, result_bg, resize} from "../../../constants/oss_consts";
import {getDailyOrders} from "../../../action/request_action";

export default class Result extends Component {

    config = {
        navigationBarTitleText: '结果'
    }

    componentDidMount() {
    }

    componentWillMount() {
        let that = this;
        getDailyOrders({id: this.$router.params.id}, function (data) {
            // getDailyOrders({id: '1246749569509752832'}, function (data) {
            let divinationResult = data.divinationResult;
            let todayFortune = divinationResult.todayFortune;
            let suggestion = divinationResult.suggestion;
            let boostLuck = divinationResult.boostLuck;
            let minorCardUrl = data.minorCardUrl;
            let majorCardUrl = data. majorCardUrl;

            that.setState({
                todayFortune: todayFortune,
                suggestion: suggestion,
                boostLuck: boostLuck,
                minorCardUrl:minorCardUrl,
                majorCardUrl:majorCardUrl
            })
        });
    }

    componentWillUnmount() {
    }

    componentDidShow() {

    }

    componentDidHide() {
    }

    findOutClick() {
        Taro.redirectTo({
            url:'../../test/test'
        });
    }

    render() {
        const {
            todayFortune,
            suggestion,
            boostLuck,
            minorCardUrl='',
            majorCardUrl=''
        } = this.state;
        return (
            <View className='result-view'>
                <Image  className='width100 gb-img' mode='widthFix' src={result_bg}></Image>
                <View className='result-content-view'>
                    {
                        majorCardUrl?
                            <View>
                                <Image className='poker-img' src={majorCardUrl.concat(resize)}></Image>
                                <Image className='poker-img' src={minorCardUrl.concat(resize)}></Image>
                            </View>
                            :
                            ''
                    }


                    <Image className='result_jd-img' src={result_jd}></Image>

                    <View className='color-text-7 '>
                        <View className='title-view font-size-20 font-weight-bold'>
                            <View>今日运势分析</View>
                            {/*<View>75</View>*/}
                        </View>
                        <View className='border-bottom mt-10 mb-10'></View>

                        {
                            todayFortune.map((item, index) => (
                                <View key={index} className='text-item-view text-align-left font-size-14'>{item}</View>
                            ))
                        }

                    </View>


                    <View className='mt-40 color-text-7'>
                        <View className='title-view  font-size-20 font-weight-bold'>
                            <View>生活建议</View>
                            <View></View>
                        </View>
                        <View className='border-bottom mt-10 mb-10'></View>
                        {
                            suggestion.map((item, index) => (
                                <View key={index} className='text-item-view text-align-left font-size-14'>{item}</View>
                            ))
                        }
                    </View>

                    <View className='mt-40 color-text-7'>
                        <View className='title-view  font-size-20 font-weight-bold'>
                            <View>转运配饰</View>
                            <View></View>
                        </View>
                        <View className='border-bottom mt-10 mb-10'></View>
                        {
                            boostLuck.map((item, index) => (
                                <View key={index} className='text-item-view text-align-left font-size-14'>{item}</View>
                            ))
                        }
                    </View>

                    <Image onClick={this.findOutClick.bind(this)} className='result_btn-img' src={result_btn}></Image>


                </View>
            </View>
        )
    }
}
