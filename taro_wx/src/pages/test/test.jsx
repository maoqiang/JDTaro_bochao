import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Button} from '@tarojs/components'
import './test.less'
import {
    border1, border3, border4, border5,
    btn1, btn3, btn4, btn5, HEAD_A,
    home_bg, icon1, icon3, icon4, icon5, input, input_bg, test_item1, test_item3, test_item4, test_item5,
    test_title_bg
} from "../../constants/oss_consts";
import {getUserInfoTool, showToast} from "../../util/app_tool";
import {IsPublish, MLoginInfo, MuserInfo} from "../../constants/enum_consts";
import {getHots} from "../../action/request_action";

export default class Test extends Component {

    config = {
        navigationBarTitleText: '塔罗测测'
    }

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            isPublish: false,

        }
    }

    componentWillMount() {
        let that = this;
        this.setState({
            isPublish: Taro.getStorageSync(IsPublish)
        })
        getHots({}, function (res) {
            let list = [];
            if(res&&res.length>0){
                for (let i = 0; i < res.length; i++) {
                    let  map = that.judgmentImg(res[i].questionType)
                    list.push({
                        ...res[i],
                        ...map
                    })
                }
            }
            that.setState({
                list: list
            })
        })
    }


    componentDidMount() {
    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    startTest(item = {}) {
        
        let questions = item.question || '';
        let id = item.id || '';

        if (this.state.isPublish) {
            Taro.navigateTo({
                url: '../select_poker/select_poker?sourceName=test&questions=' + questions + '&id=' + id
            })
        } else {
            Taro.navigateTo({
                url: '../select_poker/select_poker?sourceName=find'
            })
        }

    }

    selectMaster(){
        Taro.navigateTo({
            url: '../select_poker/select_master'
        })
    }

    judgmentImg(type) {
        //debugger
        let test_item = '';
        let border = '';
        let icon = '';
        let btn = '';

        switch (type) {
            case 1:
                test_item = test_item3;
                border = border3;
                icon = icon3;
                btn = btn3;
                break;
            case 2:
                test_item = test_item1;
                border = border1;
                icon = icon1;
                btn = btn1;
                break;
            case 3:
                test_item = test_item4;
                border = border4;
                icon = icon4;
                btn = btn4;
                break;
            default:
                test_item = test_item5;
                border = border5;
                icon = icon5;
                btn = btn5;
                break;
        }

        return {
            test_item: test_item,
            border: border,
            icon: icon,
            btn: btn,
        }
    }

    render() {
        const {list} = this.state;

        return (
            <View className='test-view'
                  style={`background-image: url(${home_bg});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                <View className='title_bg_view' style={`background-image: url(${test_title_bg});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                    <View onClick={this.startTest.bind(this)} mode='widthFix' className='input-view fff font-size-16'  style={`background-image: url(${input_bg});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                        输入您想要问的问题
                    </View>
                </View>

                <View className='bg-content-view'>

                <View className='title-text fff font-size-16  mt-10 mb-10'>热门问题</View>
                {list.map((item, index) => (

                    <View onClick={this.startTest.bind(this, item)}   key={index} className='item-view'
                          style={`background-image: url(${item.test_item});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                        <Image mode='scaleToFill' className='item-img-header pl-10' src={item.titleImgUrl||HEAD_A}></Image>
                        <View className='text-align-left fff flex1  ml-15'>
                            <View className='font-size-14  item-title'
                                  style={`background-image: url(${item.border});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                                {item.questionTypeStr}
                            </View>
                            <View className='font-size-16 mt-10'>{item.question}</View>
                            <View className='score-view'>
                                <Image mode='scaleToFill' className='item-img-icon' src={item.icon}></Image>
                                <Text className='ml-5 font-size-12 '>{item.score}人已测</Text>
                            </View>
                        </View>
                        <View  className='item-img-btn'
                              style={`background-image: url(${item.btn});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                            提问
                        </View>
                    </View>

                ))}
                </View>

            </View>
        )
    }
}
