import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Textarea, Swiper, SwiperItem} from '@tarojs/components'
import './question.less'
import {
    arrow_down,
    arrow_up,
    ce1_backgroud, checkmark,
    coupon_icon,
    question_bg,
    question_button
} from "../../constants/oss_consts";
import {formatNumber, interceptArray, showToast} from "../../util/app_tool";
import {getDailyTitles, getPayCalcs, getPreQuestions, getQuestionPrePay} from "../../action/request_action";
import {IsPublish} from "../../constants/enum_consts";

let timer = null;

export default class Question extends Component {

    config = {
        navigationBarTitleText: '提问'
    }

    constructor(props) {
        super(props);
        this.state = {
            questionProduct: 0.00,
            textAreaValue: '',
            questionsId: 0,
            barrageList_arr: [],
            barrageList_obj: [],
            barrageList: [],
            phoneWidth: 0,
            couponOpen: false,
            isPublish: false,
            selectedIndex: -1,
            selectedItemIndex: -1,
        }
    }

    componentWillMount() {
        const that = this;
        Taro.getSystemInfo({
            success: function (res) {
                console.log('ww', res)
                that.setState({
                    phoneWidth: res.windowWidth - 100
                })
            }
        });
    }

    componentDidMount() {
        let that = this;
        this.setState({
            isPublish: Taro.getStorageSync(IsPublish)
        });
        that.getData();

    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    componentDidHide() {
        clearInterval(timer);
    }


    componentDidShow() {
    }

    getData() {
        const that = this;
        // 添加弹幕所需的时间，初始位置
        getPreQuestions({}, function (data) {
            let hotTopics = [];
            for (let i = 0; i < 4; i++) {
            }
            hotTopics = hotTopics.concat(data.hotTopics);

            let questionProduct = data.questionProduct;
            let coupons = data.coupons ? data.coupons : [];
            let list = [];
            for (let i = 0; i < hotTopics.length; i++) {
                list.push({
                    width: that.state.phoneWidth,
                    time: 17,
                    height: 20,
                    content: hotTopics[i].question,
                    id: hotTopics[i].id,
                    score: hotTopics[i].score,
                    createTime: hotTopics[i].createTime,
                    modifyTime: hotTopics[i].modifyTime,
                    isDel: hotTopics[i].isDel,
                })
            }
            that.state.barrageList_arr = list;
            that.state.questionProduct = questionProduct;

            let couponsList = interceptArray(coupons, 2)
            that.state.couponsList = couponsList;
            that.state.coupons = coupons;

            that.setState({
                barrageList: interceptArray(list, Math.ceil(list.length / 4)),
                salesPrice: questionProduct.salesPrice
            })

            if (couponsList && couponsList.length > 0 && couponsList[0].length > 0) {
                that.couponButton(0, 0);
            }
            // setTimeout(() => {
            //     that.barrageListObj();
            // }, 200)
        });
    }

    /**
     *因为从后台获取到的是全部的数据，所以要把数据分开，让每条数据有先后之分
     *每隔一秒往barrageList 数组插入一条数据
     * */
    barrageListObj() {
        const that = this
        let {barrageList_arr, barrageList} = that.state;
        let i = 0;
        timer = setInterval(function () {
            if (barrageList.length > 1000) {
                barrageList = [];
            }
            barrageList.push(barrageList_arr[i]);
            console.log(barrageList.length);
            that.setState({
                barrageList: barrageList
            });

            if (i == barrageList_arr.length - 1) {
                i = 0;
            } else {
                i++;
            }
        }, 1000)
    }


    couponButton(indexItem, indexLi, e) {
        if (e) {
            e.stopPropagation();
        }
        let that = this;
        if (indexItem == -1 || indexLi == -1) {
            return
        }

        let selectedIndex = this.state.selectedIndex;
        let selectedItemIndex = this.state.selectedIndex;
        let salesPrice = this.state.questionProduct.salesPrice;

        if (selectedIndex == indexLi && selectedItemIndex == indexItem) {
            //二次点击,取消勾选优惠券
            this.setState({
                selectedIndex: -1,
                selectedItemIndex: -1,
                CouponId: 0,
                salesPrice: salesPrice
            })
        } else {
            let CouponId = this.state.couponsList[indexItem][indexLi].id;
            getPayCalcs({SalesPrice: salesPrice, CouponId: CouponId}, '', function (res) {

                that.setState({
                    salesPrice: res,
                    selectedIndex: indexLi,
                    selectedItemIndex: indexItem,
                    CouponId: CouponId
                })
            }, function (err) {

            });
        }
    }

    receiveCouponsBtn() {
        Taro.navigateTo({
            url: '/pages/find/activitys/activitys'
        })
    }

    initCouponView() {
        const {couponOpen, couponsList, selectedIndex, selectedItemIndex} = this.state
        return (
            <View className='coupon-parent-view' style={`height:${couponOpen ? 115 : 30}px`}>
                {
                    couponsList && couponsList.length > 0 ?
                        <Image onClick={this.couponClose2Open.bind(this)} className='down-img'
                               src={couponOpen ? arrow_down : arrow_up}></Image>
                        :
                        ''
                }
                {
                    couponOpen ?
                        <Swiper
                            className='test-h'
                            indicatorColor='#999'
                            indicatorActiveColor='#333'
                            displayMultipleItems={1}
                            vertical={false}
                            circular={false}
                            indicatorDots={false}
                            interval={2000}
                            autoplay={false}>
                            {
                                couponsList.map((item, indexItem) => (
                                    <SwiperItem key={indexItem}>
                                        {
                                            item.map((li, indexLi) => (
                                                <View key={indexLi}
                                                      onClick={this.couponButton.bind(this, indexItem, indexLi)}
                                                      className='coupon-view'>
                                                    {
                                                        selectedItemIndex == indexItem && selectedIndex == indexLi ?
                                                            <Image className='checkmark-img' src={checkmark}></Image>
                                                            : ''
                                                    }

                                                    <Image className='coupon-view-img' src={coupon_icon}></Image>

                                                    <View className='coupon-text'>
                                                        <View className='font-size-14'>{li.title}</View>
                                                        <View className='font-size-12'>{li.amountStr}</View>
                                                        <View
                                                            className='font-size-12 '>时限: {li.usableStartTime.substring(5, 10).replace('-', '.')}--{li.usableEndTime.substring(5, 10).replace('-', '.')}</View>
                                                    </View>
                                                </View>
                                            ))
                                        }
                                    </SwiperItem>
                                ))
                            }
                        </Swiper>
                        :
                        couponsList && couponsList.length > 0 && couponsList[0].length > 0 ?
                            <View className='coupon-text-view'>
                                <Text>优惠券:</Text>
                                <Text>￥ {couponsList[0][0].amountStr}</Text>
                                <Text>{couponsList[0][0].title}</Text>
                            </View>
                            :
                            <View className='coupon-text-view'>
                                <Text></Text>
                                <Text onClick={this.receiveCouponsBtn.bind(this)}>你还没有优惠券，免费去领取</Text>
                                <Text></Text>
                            </View>
                }


            </View>
        )
    }

    inputListener(e) {
        let value = e.detail.value;
        this.setState({
            textAreaValue: value
        })
    }

    clickBarrage(item) {
        this.setState({
            textAreaValue: item.content,
            questionsId: item.id
        })
    }

    everyDayTest() {
        Taro.navigateTo({
            url: '../select_poker/select_poker?sourceName=find'
        })
    }

    questionCommit() {
        let that = this;

        let QuestionStr = that.state.textAreaValue;
        if (!QuestionStr) {
            showToast('请输入你要占卜的问题~')
            return
        }
        let questionsId = that.state.questionsId;

        let questionProductId = that.state.questionProduct.id ? that.state.questionProduct.id : 0;
        let CouponId = that.state.CouponId ? that.state.CouponId : 0;
        let urlParam = '?ProductId=' + questionProductId + '&CouponId=' + CouponId + '&HotTopicId=' + questionsId + '&QuestionStr=' + QuestionStr
        getQuestionPrePay({}, urlParam, function (data) {
            debugger
            let map = {
                timeStamp: data.timeStamp,
                package: data.package,
                paySign: data.paySign,
                signType: data.signType,
                nonceStr: data.nonceStr,
            }
            Taro.requestPayment({
                ...map,
                success: function (res) {

                    let questions = that.state.textAreaValue;
                    Taro.redirectTo({
                        url: '../select_poker/select_poker?sourceName=test&questions=' + questions + '&orderId=' + data.orderId
                    })
                },
                fail: function (res) {
                    showToast('请重新支付或联系管理员');
                }
            })


        });
    }

    couponClose2Open(type, e) {
        if (type == 'input' && !this.state.couponOpen) {
            this.setState({
                couponOpen: false,
            })
        } else {
            this.setState({
                couponOpen: !this.state.couponOpen,
            })
        }
    }

    amountProcessing(num) {
        let amout = formatNumber(num || 0.00);
        return amout.split('.');
    }

    render() {
        const {textAreaValue, barrageList, salesPrice, isPublish, couponOpen} = this.state;
        return (
            <View className='question-view'>


                <Image className='image-view' src={question_bg} mode='aspectFill'></Image>


                <View className='content-view'>
                    <View className='barrage-fly-view'>
                        <View className='barrage-fly' style={`animation: first 10s linear 0s infinite normal;`}>
                            {barrageList.map((item, i) => (
                                <View key={i}>
                                    {
                                        item.map((li, j) => (
                                            <Text key={j} onClick={this.clickBarrage.bind(this, li)}
                                                  className='barrage-textFly '>{li.content}</Text>
                                        ))
                                    }
                                </View>
                            ))}
                        </View>
                    </View>


                    <View className='input-parent-view'>
                        <View className='fff' style='text-align:left;padding-left:25px'>请输入问题</View>
                        <View className='p-relative'>
                            <Textarea onClick={this.couponClose2Open.bind(this, 'input')} show-confirm-bar={false}
                                      cursor-spacing="110" onInput={this.inputListener.bind(this)}
                                      maxlength={150} className='input-view'
                                      value={textAreaValue} placeholder='请输入'></Textarea>
                            <View className="current-word-num">还可以输入{150 - textAreaValue.length}个字</View>
                        </View>

                        {
                            isPublish ?
                                <View className='bottom-view'>
                                    {
                                        this.initCouponView()
                                    }
                                    <View className='pay-view'>
                                        <View className='pay-price-view'>
                                            <Text className='fz-18'>￥</Text>
                                            <Text
                                                className='fz-36'>{this.amountProcessing(salesPrice)[0]}</Text>
                                            <Text
                                                className='fz-24'>.{this.amountProcessing(salesPrice)[1]}</Text>
                                        </View>
                                        <Text onClick={this.questionCommit.bind(this)}
                                              className='go-pay-view'>支付去抽牌</Text>
                                    </View>
                                </View>

                                :

                                <View className='input-parent-view mt-20'>
                                    <Image onClick={this.everyDayTest.bind(this)} className='button-image'
                                           src={question_button}></Image>
                                </View>


                        }
                    </View>

                </View>


            </View>
        )
    }
}
