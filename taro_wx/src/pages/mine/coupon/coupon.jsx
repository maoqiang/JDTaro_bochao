import Taro, {Component} from '@tarojs/taro'
import {View, Textarea, Image, ScrollView} from '@tarojs/components'
import {AtTabs, AtTabsPane, AtGrid} from 'taro-ui'
// import "taro-ui/dist/style/index.scss"
import './coupon.less'
import {coupon_icon, coupon_used} from "../../../constants/oss_consts";
import {getCoupons} from "../../../action/request_action";


export default class Mine extends Component {

    config = {
        navigationBarTitleText: '优惠券',
        disableScroll: true
    }

    constructor(props) {
        super(props);
        this.state = {
            current: 0,

            list: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        let that = this;
        getCoupons({}, '', function (data) {
            let usableList = data.usableList;
            let usedList = data.usedList;
            let expiredList = data.expiredList;
            that.setState({
                usableList: usableList,
                usedList: usedList,
                expiredList: expiredList
            })
        });
    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    initListView(list, type) {
        if (list) {
            let url = '';
            if (type == 0) {
                url = coupon_icon;
            } else if (type == 1) {
                url = coupon_used;
            } else {
                url = coupon_used;
            }

            return <ScrollView scrollY={true} className='tab-view'>
                {
                    list.map((item, index) => (
                        <View key={index} className='coupon-item-view'>
                            <Image className='coupon-view-img' mode='scaleToFill' src={url}></Image>
                            <View className='coupon-text'>
                                <View className='display-flex align-items-center'>
                                    <View>{item.title}</View>
                                    <View className='font-size-16 ml-15'>{item.amountStr}</View>
                                </View>


                                <View className='font-size-16 mt-20'>{item.usable}</View>
                                <View
                                    className='font-size-16 mt-20'>有效期: {item.usableStartTime.substring(5, 10).replace('-', '.')}--{item.usableEndTime.substring(5, 10).replace('-', '.')}</View>
                            </View>
                        </View>
                    ))
                }

                {//如果最后一行是单数, 将布局挤开
                    list.length % 2 == 0 ? '' :
                        <View className='coupon-item-view'>

                        </View>
                }
            </ScrollView>
        }
    }

    handleClick(value) {
        this.setState({
            current: value
        })
    }

    render() {
        const tabList = [{title: '可使用'}, {title: '已使用'}, {title: '已过期'}];
        const {usableList, usedList, expiredList} = this.state
        return (
            <View className='coupon-view'>
                <AtTabs current={this.state.current} tabList={tabList} onClick={this.handleClick.bind(this)}>
                    <AtTabsPane current={this.state.current} index={0}>
                        {this.initListView(usableList, 0)}
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={1}>
                        {this.initListView(usedList, 1)}
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={2}>
                        {this.initListView(expiredList, 2)}
                    </AtTabsPane>
                </AtTabs>

            </View>
        )
    }
}
