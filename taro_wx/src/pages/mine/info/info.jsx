import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Input, Picker, Button} from '@tarojs/components'
import {AtFloatLayout, AtRadio, AtModal, AtModalHeader, AtModalContent, AtModalAction} from "taro-ui"
import "taro-ui/dist/style/components/float-layout.scss"
import "taro-ui/dist/style/components/radio.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/modal.scss";
import './info.less'
import {arrow_icon, arrow_right, HEAD_A, me_bg1} from "../../../constants/oss_consts";
import {showToast} from "../../../util/app_tool";
import {IsPublish, MuserInfo, UserId} from "../../../constants/enum_consts";
import {getPhone, postFeedbacks, wxUserPut} from "../../../action/request_action";

export default class Mine extends Component {
    config = {
        navigationBarTitleText: '个人信息',
        disableScroll: true
    }

    constructor(props) {
        super(props);
        this.state = {
            birthday: undefined,
            constellation: '',
            constellationList: ['未设置','白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '室女座', '天秤座', '天蝎座', '人马座', '摩羯座', '宝瓶座', '双鱼座'],
            sexList: ['未设置','男', '女'],
            gender: '',
            openAtFloatLayout: false,
            show_btn: true,
            count: 60,
            code_ts: '获取验证码',
            nickName: undefined,
            isOpened: false,

            openAtFLSex: false,
            mUserInfo:{},
            phoneText:'请绑定手机号'
        }
    }

    componentWillMount() {
        let mUserInfo = Taro.getStorageSync(MuserInfo);
        this.setState({
            mUserInfo: mUserInfo,
            phoneText:Taro.getStorageSync(IsPublish)?'完善信息可获优惠券一张':'请绑定手机号'
        })
    }

    componentDidMount() {

    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    settingClick(index, e) {
        switch (index) {
            case 0:
                this.setState({isOpened: true})
                break;
            case 1:
                this.setState({isOpened: false})
                break;
            case 2://修改昵称
                this.commitUser();
                break;
            case 4:
                this.setState({
                    openAtFloatLayout: true
                })
                break;

        }
    }

    onChange = (type, e) => {
        if (type == 'sex') {
            this.setState({
                gender: e.detail.value,
            },()=>{
                this.commitUser()
            })
        } else {
            this.setState({
                constellation: e.detail.value
            },()=>{
                this.commitUser()
            })
        }

    }

    onDateChange = e => {
        this.setState({
            birthday: e.detail.value
        },()=>{
            this.commitUser()
        })
    };


    phoneCommit() {

    }

    inputName(e) {
        this.setState({
            nickName: e.currentTarget.value
        })
    }


    getPhoneNumber(e) {
        let that = this;
        let result = e.detail
        if(!result.encryptedData){
         return
        }
        let map = {
            encryptedData: result.encryptedData,
            IV: result.iv
        }

        getPhone(map, function (data) {
            that.setState({
                phone: data.phoneNumber
            },()=>{
                that.commitUser()
            })
        });
    }

    commitUser() {
        let that = this;
        const {nickName, phone, gender, birthday, constellation} = this.state;
        let map = {
            nickName: nickName,
            phone: phone,
            gender: gender,
            birthday: birthday,
            constellation: constellation,
        }
        wxUserPut(map,'?id='+Taro.getStorageSync(UserId), function (data) {
            Taro.setStorageSync(MuserInfo,data);
            that.setState({
                isOpened:false,
                mUserInfo:data
            },()=>{
                let pages = Taro.getCurrentPages();
                let beforePage = pages[pages.length - 2];
                beforePage.setData({
                    mUserInfo:data
                })
            })
        });
    }


    render() {
        const {constellationList, sexList,isOpened,mUserInfo,phoneText} = this.state
        return (
            <View className='mine-view'>
                <AtModal isOpened={isOpened}>
                    <AtModalHeader>修改昵称</AtModalHeader>
                    <AtModalContent>
                        <Input onInput={this.inputName.bind(this)} value={mUserInfo.userName} placeholder='请输入'></Input>
                    </AtModalContent>
                    <AtModalAction> <Button onClick={this.settingClick.bind(this, 1)}>取消</Button> <Button
                        onClick={this.settingClick.bind(this, 2)}>确定</Button> </AtModalAction>
                </AtModal>

                <Image className='me-bg1-img' src={me_bg1}></Image>
                <View className='artist__ring'></View>
                <View className='artist__ring artist__ring--outer'></View>
                <Image mode='aspectFill' className='head-img'
                       src={mUserInfo.headImgUrl||HEAD_A}></Image>

                <View className='me-content-view font-size-12 color-text-8'>
                    <View onClick={this.settingClick.bind(this, 0)}
                          className='me-item-view mt-85 d-flex justify-contentr-sb'>
                        <View>昵称</View>
                        <View>{mUserInfo.userName || '未设置'}<Image className='arrow_icon-view' src={arrow_right}></Image></View>
                    </View>

                    <View className='me-item-view d-flex justify-contentr-sb'>
                        <View>生日</View>
                        <Picker mode='date' onChange={this.onDateChange}>
                            <View>{mUserInfo.birthday?mUserInfo.birthday.substring(0,10):'' || '未设置'}<Image className='arrow_icon-view'
                                                            src={arrow_right}></Image></View>
                        </Picker>
                    </View>

                    <View className='me-item-view d-flex justify-contentr-sb'>
                        <View>性别</View>

                        <Picker mode='selector' range={sexList} onChange={this.onChange.bind(this, 'sex')}>
                            <View>{sexList[mUserInfo.gender]|| '未设置'}<Image className='arrow_icon-view'
                                                          src={arrow_right}></Image></View>
                        </Picker>
                    </View>

                    <View className='me-item-view d-flex justify-contentr-sb'>
                        <View>星座</View>
                        <Picker mode='selector' range={constellationList} onChange={this.onChange.bind(this, '')}>
                            <View>{constellationList[mUserInfo.constellation] || '未设置'}<Image className='arrow_icon-view'
                                                                 src={arrow_right}></Image></View>
                        </Picker>
                    </View>

                    <View className='me-item-view d-flex justify-contentr-sb'>
                        <View>手机号码</View>
                        <Button type="primary" openType="getPhoneNumber"
                                onGetPhoneNumber={this.getPhoneNumber.bind(this)}
                                className='phone-button font-size-14'>{mUserInfo.phone||phoneText}</Button>
                        <Image className='arrow_icon-view' src={arrow_right}></Image>
                    </View>
                </View>

            </View>
        )
    }
}
