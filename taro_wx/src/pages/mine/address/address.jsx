import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Picker} from '@tarojs/components'
import {AtFloatLayout, AtRadio} from "taro-ui"
import "taro-ui/dist/style/components/float-layout.scss"
import "taro-ui/dist/style/components/radio.scss";
import "taro-ui/dist/style/components/icon.scss";
import './address.less'
import {HEAD_A, me_bg1} from "../../../constants/oss_consts";

export default class Mine extends Component {
    config = {
        navigationBarTitleText: '地址管理',
        disableScroll: true
    }

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentWillMount() {
    }

    componentDidMount() {

    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }


    render() {
        const {} = this.state
        return (
            <View className='address-view'>
                <Image className='me-bg1-img' src={me_bg1}></Image>
                <View className='artist__ring'></View>
                <View className='artist__ring artist__ring--outer'></View>
                <Image mode='aspectFill' className='head-img' src={HEAD_A}></Image>

                <View className='me-content-view font-size-12 color-text-8'>
                    <View className='mt-60 color-text-9 font-size-12 font-weight-bold text-align-center'>这是一个比较长的ID大概有多长我也不知道</View>

                    <View className='m-20'>
                        <View className='color-text-9 font-size-20 mt-30  mb-10'>地址管理</View>

                        <View className='address-item-view mb-20'>
                            <View className='display-flex mt-10'>
                                <View className='color-text-13 font-size-14 flex1 text-align-left'>名字</View>
                                <View className='color-text-13 font-size-14 flex2'>18802910500</View>
                                <View className='flex3 text-align-right align-items-center'> <Image className='address-item-img ' src={HEAD_A}></Image></View>
                            </View>
                            <View className='font-size-12 mb-20'>发动机卡就分开了放假打卡了就分开了大姐附件的撒酒疯安防监控来得及老会计发来的撒娇联发科激动撒路口家里咖啡机阿卡丽减肥了看大家六块腹肌打卡就分开了多久啊看来解放东路卡劵李开复</View>
                        </View>

                        <View className='address-item-view mb-20'>
                            <View className='display-flex mt-10'>
                                <View className='color-text-13 font-size-14 flex1 text-align-left'>名字</View>
                                <View className='color-text-13 font-size-14 flex2'>18802910500</View>
                                <View className='flex3 text-align-right align-items-center'> <Image className='address-item-img ' src={HEAD_A}></Image></View>
                            </View>
                            <View className='font-size-12 mb-20'>发动机卡就分开了放假打卡了就分开了大姐附件的撒酒疯安防监控来得及老会计发来的撒娇联发科激动撒路口家里咖啡机阿卡丽减肥了看大家六块腹肌打卡就分开了多久啊看来解放东路卡劵李开复</View>
                        </View>

                    </View>

                    <View onClick={this.phoneCommit.bind(this)} className='phone-commit-view color-bg-2'>+ 新建收货地址</View>

                </View>
            </View>
        )
    }
}
