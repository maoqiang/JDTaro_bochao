import Taro, {Component} from '@tarojs/taro'
import {View, Button, Image, ScrollView} from '@tarojs/components'
import './mine.less'
import {
    arrow_icon,
    arrow_right,
    ce1_backgroud,
    HEAD_A,
    me_bg1,
    me_icon1,
    me_icon2,
    me_icon3, mine_huodong
} from "../../constants/oss_consts";
import {showToast} from "../../util/app_tool";
import {IsPublish, MuserInfo} from "../../constants/enum_consts";

export default class Mine extends Component {

    config = {
        navigationBarTitleText: '塔罗测测',
        disableScroll:true
    }

    constructor(props) {
        super(props);
        this.state = {
            isPublish:false
        }
    }

    componentWillMount() {
        let mUserInfo = Taro.getStorageSync(MuserInfo);
        this.setState({
            mUserInfo: mUserInfo,
            isPublish:Taro.getStorageSync(IsPublish)
        })
    }

    componentDidMount() {

    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    settingClick(index, e) {
        switch (index) {
            case 0:

                break;
            case 1:
                Taro.navigateTo({
                    url: './info/info'
                })
                break;
            case 2:
                Taro.navigateTo({
                    url: './address/address'
                })
                break;
            case 3:
                Taro.navigateTo({
                    url: './feedback/feedback'
                })
                break;
            case 4:

                break;
        }
    }

    paymentClick(index,e){
        switch (index) {
            case 0:
                Taro.navigateTo({
                    url:'./mine_answer/mine_answer'
                })
                break;
            case 1:

                showToast('敬请期待~')
                break;
            case 2:
            Taro.navigateTo({
                url:'./coupon/coupon'
            })
                break;


        }

    }


    goodsButton(e) {
        Taro.navigateTo({
            url:'../find/activitys/activitys'
        })
    }

    render() {
        const {mUserInfo,isPublish}=this.state
        return (
            <View className='mine-view'>
                <Image className='me-bg1-img' src={me_bg1}></Image>
                <View className='artist__ring'></View>
                <View className='artist__ring artist__ring--outer'></View>
                <Image mode='aspectFill' className='head-img' src={mUserInfo.headImgUrl||HEAD_A}></Image>

                <View className='me-content-view font-size-12 color-text-8' style={`height:${isPublish?'35':'20'}vh`}>
                    <View className='mt-60 color-text-9 font-size-12 font-weight-bold text-align-center'>{mUserInfo.userName||''}</View>
                    <View className='mt-10 color-text-10 font-size-10 text-align-center'>{mUserInfo.province?mUserInfo.province+'•'+mUserInfo.country+'•'+mUserInfo.city:'中国'}</View>

                    {
                        isPublish?
                            <View className='me-item-view-top mt-28 d-flex justify-contentr-sb'>

                                {/*<View className='flex-left'>我的订单</View>*/}
                                <View className='top-icon-view'>
                                    <View onClick={this.paymentClick.bind(this, 0)}
                                          className='me-icon text-align-center'>
                                        <Image className='me_icon-img' src={me_icon1}></Image>
                                        <View>我的问答</View>
                                    </View>

                                    <View onClick={this.paymentClick.bind(this, 1)}  className='me-icon text-align-center'>
                                        <Image className='me_icon-img' src={me_icon2}></Image>
                                        <View>我的咨询</View>
                                    </View>

                                    <View onClick={this.paymentClick.bind(this, 2)} className='me-icon text-align-center'>
                                        <Image className='me_icon-img' src={me_icon3}></Image>
                                        <View>优惠券</View>
                                    </View>

                                </View>
                            </View>

                            :
                            ''
                    }




                </View>
                <View style={`margin-top:${isPublish?'25':'10'}vh;`}>
                    <View onClick={this.settingClick.bind(this, 1)} className='me-item-view mt-10 d-flex justify-contentr-sb'>
                        <View>个人信息</View>
                        <View><Image className='arrow_icon-view' src={arrow_right}></Image></View>
                    </View>

                    <View onClick={this.settingClick.bind(this, 3)} className='me-item-view d-flex justify-contentr-sb'>
                        <View>意见反馈</View>
                        <View><Image className='arrow_icon-view' src={arrow_right}></Image></View>
                    </View>


                    <Button className='me-item-view' open-type='contact'>
                        <View className=' d-flex justify-contentr-sb'>
                            <View>联系客服</View>
                            <View> <Image className='arrow_icon-view' src={arrow_right}></Image></View>
                        </View>
                    </Button>
                </View>
                {/* {
                    isPublish ?
                        <View onClick={this.goodsButton.bind(this)} className='huodong-view'>
                            <Image mode='widthFix' style='width:100%' src={mine_huodong}></Image>
                        </View>
                        :''
                } */}
            </View>
        )
    }
}
