import Taro, {Component} from '@tarojs/taro'
import {View, Textarea, Image} from '@tarojs/components'
import './feedback.less'
import {arrow_icon, HEAD_A, me_bg1, me_icon1, me_icon2, me_icon3} from "../../../constants/oss_consts";
import {showToast} from "../../../util/app_tool";
import {postFeedbacks} from "../../../action/request_action";

export default class Mine extends Component {

    config = {
        navigationBarTitleText: '意见反馈',
        disableScroll:true
    }

    constructor(props) {
        super(props);
        this.state = {
            textAreaValue: ''
        }
    }

    componentWillMount() {
    }

    componentDidMount() {

    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }


    inputListener(e) {
        let value = e.detail.value;
        this.setState({
            textAreaValue: value
        })
    }

    commit(){
        let map = {
            Feedback:this.state.textAreaValue
        }
        postFeedbacks(map,function (data) {
            showToast('提交成功~');
            Taro.navigateBack();
        });
    }
    render() {
        const {textAreaValue} = this.state
        return (
            <View className='feedback-view'>
                <Image className='feedback-bg1-img' src={me_bg1}></Image>
                <View className='feedback-content-view font-size-12 color-text-8'>
                    <View className='p-relative'>
                        <Textarea show-confirm-bar={false} onInput={this.inputListener.bind(this)}
                                  maxlength={100} className='textarea-view' value={textAreaValue}
                                  placeholder='请输入'></Textarea>
                        <View className="current-word-num">还可以输入{100 - textAreaValue.length}个字</View>
                    </View>

                    <View onClick={this.commit.bind(this)} className='phone-commit-view color-bg-2'>提交</View>

                </View>
            </View>
        )
    }
}
