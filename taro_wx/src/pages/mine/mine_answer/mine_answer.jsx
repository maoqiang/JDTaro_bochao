import Taro, {Component} from '@tarojs/taro'
import {View, Text} from '@tarojs/components'
import './mine_answer.less'

import {EPage} from 'eft-cool-taro-ui'
import 'eft-cool-taro-ui/style/index.scss';
import {HEAD_A, no_ans, wen, yes_ans} from "../../../constants/oss_consts";
import {getQuestionsList, getUserQuestions} from "../../../action/request_action";
import {formatNumber, timeAgo} from "../../../util/app_tool";
import {IsPublish} from "../../../constants/enum_consts";

let pageIndex = 1;

export default class Answer extends Component {

    config = {
        navigationBarTitleText: '我的问答' +
            ''
    }

    constructor(props) {
        super(props)
        this.state = {
            noMore: false,
            hasMore: true,
            scrollTop: 0,
            list: [],
            noAut: false,
            noData: false,
            hasMoreText: '稍安勿躁,加载中...',
            isPublish: false
        }
    }

    componentDidMount() {
        this.setState({
            isPublish: Taro.getStorageSync(IsPublish)
        })
        this.refresh();
    }

    componentWillUnmount() {
    }

    getData = (pIndex = pageIndex, callback) => {
        let that = this;
        getUserQuestions({'PageIndex': pIndex}, function (data) {
            let list = data.items;

            callback({list: list});
        });
    };

    refresh = () => {
        let that = this;
        pageIndex = 0;
        this.setState({
            refreshStatus: 1,
            hasMore: true,
            noMore: false
        });

        that.getData(0, function (res) {
            pageIndex++;
            if (res.list.length < 20) {
                that.loadMore();
            }
            that.setState({
                ...res,
                noData: res.list.length < 1,
                refreshStatus: 2,
            });
        });
    }

    loadMore = () => {
        let that = this;
        that.getData(pageIndex, function (res) {
            pageIndex++;
            if (res.list.length > 0) {
                that.setState({
                    list: that.state.list.concat(res.list),
                    hasMore: true,
                    noMore: false
                });
            } else {
                that.setState({
                    hasMore: false,
                    noMore: true,
                    hasMoreText: '没有更多数据了'
                });
            }
        });
    }

    handleScrollEnd = (e) => {
        this.setState({
            scrollTop: e.detail.scrollTop
        })
    }

    detailBtn(item, e) {
        if(item.state==1){
            Taro.navigateTo({
                url: '/pages/select_poker/select_poker?type=answer&sourceName=test&questions=' + item.question + '&orderId=' + item.id
            })
        }else{
            Taro.navigateTo({
                url: '/pages/answer/detail/detail?id=' + item.id
            })
        }

    }

    render() {
        const {
            hasMoreText,
            noMore,
            hasMore,
            refreshStatus,
            scrollTop,
            list,
            isPublish,
            wen_opacity
        } = this.state
        const refresherConfig = {
            recoverTime: 300,
            refreshTime: 1000
        };
        return (
            <View className='answer-view'>
                <EPage
                    onRefresh={this.refresh}
                    onLoadMore={this.loadMore}
                    noMore={noMore}
                    hasMore={hasMore}
                    hasMoreText={hasMoreText}
                    refresherConfig={refresherConfig}
                    refreshStatus={refreshStatus}
                    scrollTop={scrollTop}
                    onScrollEnd={this.handleScrollEnd}
                    className='EPage-view'
                >
                    {list.map((item, index) => (
                        <View key={index} className='mt-10'>
                            <View className='item-title-view plr-10'>
                                {/*<Image className='ans-img' src={item.isAnswered ? yes_ans : no_ans}></Image>*/}
                                {/*<Image className='wen-img' style={`opacity:${item.isAnswered ?'0.9':'0.4'}`} src={wen}></Image>*/}

                                {

                                    item.state == 0 ?
                                        <View className='label-status-view color-bg-7'>待支付</View>
                                        :
                                        item.state == 2 ?
                                            <View className='label-status-view color-bg-5'>待占卜</View>
                                            :
                                            item.state == 3 ?
                                                <View className='label-status-view color-bg-4'>待评价</View>
                                                :
                                                item.state == 4 ?
                                                    <View className='label-status-view color-bg-6'>已完成</View>
                                                    :
                                                    ''
                                }

                            </View>
                            <View className='border-lr'></View>
                            <View onClick={this.detailBtn.bind(this, item)} className='item-bottom-view pd-10'>
                                <View>
                                    <Image className='head-img' src={item.wxUserHeadImg || HEAD_A}></Image>
                                </View>

                                <View className='width100 ml-10'>
                                    <View className='d-flex justify-contentr-sb'>
                                        <View className=' font-size-14 color-text-4'>{item.wxUserName || ''}</View>
                                        {
                                            isPublish ?
                                                <View className=' font-size-14 color-text-5'>{formatNumber(item.priced || 0)}{' '}￥</View>
                                                :
                                                <View></View>
                                        }
                                    </View>
                                    <View className='content-view font-size-12 color-text-4'>{item.question || ''}</View>

                                    <View className='fr font-size-8 color-text-6'>{timeAgo(item.createTime)}</View>
                                </View>
                            </View>
                        </View>
                    ))}
                </EPage>
            </View>

        )

    }
}
