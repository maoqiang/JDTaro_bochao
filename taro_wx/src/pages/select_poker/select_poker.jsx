import Taro, {Component} from '@tarojs/taro'
import {View, Button, Image} from '@tarojs/components'
import './select_poker.less'
import {
    select_poker_bg1,
    select_poker,
    poker_bg, resize,rotateFun,donghua2,donghuastatic
} from "../../constants/oss_consts";
import {getShuffles, postDailyOrders, postQuestions} from "../../action/request_action";

export default class Mine extends Component {

    config = {
        navigationBarTitleText: '抽牌',
        disableScroll: true
    }
    // 移动效果处理
    state = {
        moveVioew: {
            transform: 'translate(0,0)'
        },
        appNameColor: 'whiteName',
        pageNum: 0,
    };
    // 滑动方向
    deriction = true;
    // 页码
    pageNum = 0;
    // 手势滑动最小距离
    MIN_TOUCH_DISTENCE = 50;

    startX = 0;
    
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            moveVioew: {
                transform: 'translate(0,0)'
            },
            appNameColor: 'whiteName',
            pageNum: 0,
            distance: 0,
            winWidth: 0,
            currentAngle: 243,
            angle: 243,
            isShow:false,
            starShow:true,
            //moveValue: new Animated.Value(0),
            selectList: [
                {
                    img: select_poker_bg1,
                    selectIng: false
                },
                {
                    img: select_poker_bg1,
                    selectIng: false

                }
            ]
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        let that = this;
        let sourceName = this.$router.params.sourceName;
        this.state.answerType = this.$router.params.type;

        this.state.questions = this.$router.params.questions;
        //this.state.questions_id = this.$router.params.questions_id;
        this.state.questions_id = this.$router.params.id;
        this.state.orderId = this.$router.params.orderId;
        let type;
        let selectList = this.state.selectList;
        if (sourceName == 'find') {//发现
            type = 1;
        } else {//测
            type = 0;
            selectList.push(
                {
                    img: select_poker_bg1,
                    selectIng: false
                },
            )
        }
        getShuffles({type: type}, function (data) {
            that.state.majorArcanas = data.majorArcanas;//22
            that.state.minorArcanas = data.minorArcanas;//22

            that.state.divinationCards = data.divinationCards;//78
            that.state.tarotGroupId = data.tarotGroupId;
            that.state.urlBase = data.urlBase
            that.rotationPosition(selectList, sourceName);
        });   
    }

    hideStarBtn()
    {
        let that = this;
        that.setState({starShow:false});
        that.setState({isShow:true});
        setTimeout(function () {that.setState({isShow:false})},3000)
    }
    hideGif() {
        let that = this;
        that.setState.isShow=false;
    }
    // 计算旋转定位
    rotationPosition(selectList, sourceName) {
        let winWidth = Taro.getSystemInfoSync().windowWidth;
        this.state.winWidth = winWidth;

        let list = [];
        let length = 78;
        for (let i = 0; i < length; i++) {
            list.push({
                index: i,
            })
        }

        let rotateList = [];
        let i = 1;
        list.map(function (e, index) {
            rotateList.push((i * 3))
            i++;
        });

        this.setState({
            list: list,
            rotateList: rotateList,
            sourceName: sourceName,
            selectList: selectList
        })
    }


    /*手接触屏幕*/
    handleTouchStart = (e) => {
        this.startX = e.touches[0].clientX;
    };
    /*手在屏幕上移动*/
    handleTouchMove = (e) => {
        let winWidth = this.state.winWidth;
        let currentAngle = this.state.currentAngle;

        this.endX = e.touches[0].clientX;
        let distance = this.startX - this.endX;

        let angle = -(distance / (winWidth) * 117) + currentAngle

        if (distance < 0) {
            angle = angle > 360 ? 360 : angle;
        } else {
            angle = angle < 125 ? 125 : angle;
        }
        this.setState({
            angle: angle
        })
    };
    /*手离开屏幕*/
    handleTouchEnd = (e) => {
        // 获取滑动范围
        this.state.currentAngle = this.state.angle
    };

    selectOnclick(index, e) {
        let divinationCards = this.state.divinationCards;//78
        let majorArcanas = this.state.majorArcanas;//22
        let minorArcanas = this.state.minorArcanas;
        let urlBase = this.state.urlBase

        let selectList = this.state.selectList;

        if (selectList[selectList.length - 1].selectIng) {
            return
        }
        let list = this.state.list;

        let oldIndex = this.state.oldIndex;
        list[index].height = 165;
        if (oldIndex && oldIndex > -1) {
            list[oldIndex].height = 160;
        }

        //选中
        for (let i = 0; i < selectList.length; i++) {
            if (!selectList[i].selectIng) {
                if (this.state.sourceName == 'find') {//发现
                    selectList[i].img = select_poker;
                    if (i == 0) {
                        let majorIndex = index % majorArcanas.length;
                        selectList[i].majorCardIndex = majorArcanas[majorIndex].cardIndex;
                        selectList[i].isMajorInversion = majorArcanas[majorIndex].isInversion;
                        selectList[i].MajorCardUrl = urlBase.concat(majorArcanas[majorIndex].picName);
                    } else {
                        let minorIndex = index % minorArcanas.length;
                        selectList[i].minorCardIndex = minorArcanas[minorIndex].cardIndex;
                        selectList[i].isMinorInversion = minorArcanas[minorIndex].isInversion;
                        selectList[i].MinorCardUrl = urlBase.concat(minorArcanas[minorIndex].picName);
                    }

                } else {
                    selectList[i].img = urlBase.concat(divinationCards[index].picName)
                    selectList[i].cardIndex = divinationCards[index].cardIndex
                    selectList[i].cardName = divinationCards[index].cardName
                    selectList[i].isInversion = divinationCards[index].isInversion
                }

                selectList[i].selectIng = true;
                selectList[i].selectPoker = list[index]
                break
            }
        }
        
        list.splice(index, 1);
        if (this.state.sourceName == 'find') {//发
            if (index == 0) {
                majorArcanas.splice(index, 1);
            } else {
                minorArcanas.splice(index, 1);
            }
        } else {
            divinationCards.splice(index, 1);
        }
        index = -1
        // if (index == oldIndex) {
        //
        // }
        //debugger// add animation

        this.setState({
            list: list,
            oldIndex: index,
            selectList: selectList
        }, () => {
            if (selectList[selectList.length - 1].selectIng) {
                this.commit();
            }
        })
    }

    commit() {
        let that = this;
        let map = {};
        let selectList = this.state.selectList;
        let urlBase = this.state.urlBase

        if (this.state.sourceName == 'find') {//发现

            let major = selectList[0];
            let minor = selectList[1];
            map = {
                "cardGroupId": this.state.tarotGroupId,
                "majorCardIndex": major.majorCardIndex,
                "isMajorInversion": major.isMajorInversion,
                MajorCardUrl: major.MajorCardUrl,

                "minorCardIndex": minor.minorCardIndex,
                "isMinorInversion": minor.isMinorInversion,
                MinorCardUrl: minor.MinorCardUrl
            }

            postDailyOrders(map, '?id=' + this.state.orderId, function (data) {
                Taro.redirectTo({
                    url: '../find/result/result?id=' + data.id
                })
            });
        } else {
            
            map = {
                "hotTopicId": this.state.questions_id,
                "questions": this.state.questions,
                "firstCard": {
                    "cardIndex": selectList[0].cardIndex,
                    "cardName": selectList[0].cardName,
                    "picUrl": selectList[0].img,
                    "isInversion": selectList[0].isInversion
                },
                "secondCard": {
                    "cardIndex": selectList[1].cardIndex,
                    "cardName": selectList[1].cardName,
                    "picUrl": selectList[1].img,
                    "isInversion": selectList[1].isInversion
                },
                "thirdCard": {
                    "cardIndex": selectList[2].cardIndex,
                    "cardName": selectList[2].cardName,
                    "picUrl": selectList[2].img,
                    "isInversion": selectList[2].isInversion
                },
            }

            let paramStr = encodeURIComponent(JSON.stringify(map));
            debugger
            Taro.redirectTo({
                url: '../question/question?paramStr='+paramStr
            })

        }
    }

    render() {
        const {list, angle, rotateList, selectList, sourceName,isShow,starShow,moveValue} = this.state;
        
        return (
            <View className='poker-view'>
                
            <View ref="taroPop" onClick={this.hideStarBtn.bind(this)} className={this.state.starShow?'poker_starshow':'poker_starnone'}>
            <Image mode='widthFix' style="opacity:1" className='' src={donghuastatic}></Image>               
            </View>

            <View ref="taroPop" className={this.state.isShow?'poker_show':'poker_none'}>
            <View style="" className="">
               <Image mode='widthFix' style="opacity:1" className='' src={donghua2}></Image>
               <View style="text-shadow: 6rpx 0 16rpx #5b08a4, -6rpx 0 16rpx #5b08a4, 0 6rpx 16rpx #5b08a4, 0 -6rpx 16rpx #ffffff;color:#fff;font-size:32rpx;" className="">正在洗牌请稍后......</View>
            </View>
            </View>

                <Image className='backgroud-img' mode='aspectFill' src={poker_bg}></Image>
                <View className='poker-img-view'>
                    {
                        selectList.map((item, index) => (
                            item.selectIng && sourceName != 'find' ?
                                <Image key={index} className='poker-img'
                                       src={item.img.concat(resize).concat(rotateFun(item.isInversion))}></Image>
                                :
                                <Image className='poker-img' src={item.img}></Image>
                        ))
                    }

                </View>
                <View className='text-view font-size-16 fff text-align-center'>心中默念您占卜的问题</View>
                <View className='text-view font-size-16 fff text-align-center'>左右滑动下方78张牌,选出其中{sourceName=='find'?'2':'3'}张</View>

                <View className='bg-view'
                      style={{
                          transform: [`rotate(${(angle)}deg)`]
                      }}
                >

                    {
                        list.map((item, i) => (
                            <Image
                                key={i}
                                onTouchStart={this.handleTouchStart.bind(this)}
                                onTouchMove={this.handleTouchMove.bind(this)}
                                onTouchEnd={this.handleTouchEnd.bind(this)}
                                onClick={this.selectOnclick.bind(this, i)} className='poker-img-rotate'
                                style={{transform: [`rotate(${(rotateList[i])}deg)`], height: `${item.height}px`}}
                                src={select_poker} />
                        ))
                    }
                </View>
            </View>
        )
    }
}
