import Taro, {Component} from '@tarojs/taro'
import {View, Text, Image, Button} from '@tarojs/components'
import './select_master.less'

import {EPage} from 'eft-cool-taro-ui'
import {AtModal, AtModalHeader, AtModalContent, AtModalAction} from "taro-ui"
import 'eft-cool-taro-ui/style/index.scss';
import {
    border1, border3, border4, border5,
    btn1, btn3, btn4, btn5, HEAD_A,
    home_bg, icon1, icon3, icon4, icon5, input, input_bg, AugurSelect_Rect1,AugurSelect_Rect2,AugurSelect_Rect3,AugurSelect_Rect4,
    test_title_bg,AugurSelect_RectLevel1,AugurSelect_RectLevel2,AugurSelect_RectLevel3,AugurSelect_RectLevel4,AugurSelect_Star,AugurSelect_Half,AugurSelect_bgImg
} from "../../constants/oss_consts";
import {getUserInfoTool, showToast} from "../../util/app_tool";
import {IsPublish, MLoginInfo, MuserInfo} from "../../constants/enum_consts";
import {geAugurUsers,putQuestions,getPrepay} from "../../action/request_action";
let pageIndex = 1;
export default class Test extends Component {

    config = {
        navigationBarTitleText: '选择占卜师'
    }

    constructor(props) {
        super(props);
        this.state = {
            noMore: false,
            hasMore: true,
            scrollTop: 0,
            list: [],
            noAut: false,
            noData: false,
            isOpenedModal: false,
            hasMoreText: '稍安勿躁,加载中...',
            isPublish: false,
            arrstr:[{url:"★"},{url:"★"},{url:"★"},{url:"★"},{url:"★"}],
            selectPoker:{},
            questionsObj: {},
        }
    }

    componentWillMount() {
        // let that = this;
        // this.setState({
        //     isPublish: Taro.getStorageSync(IsPublish)
        // })
        // geAugurUsers({},'?pageIndex=0', function (res) {
        //     let list = [];
        //     debugger
        //     if(res&&res.length>0){
        //         for (let i = 0; i < res.length; i++) {
        //             let  map = that.judgmentImg(res[i].questionType)
        //             list.push({
        //                 ...res[i],
        //                 ...map
        //             })
        //         }
        //     }
        //     that.setState({
        //         list: list
        //     })
        // })
    }

    componentDidMount() {
        let paramStr = this.$router.params.paramStr;
        let selectPoker = JSON.parse(decodeURIComponent(paramStr));
        debugger
        this.setState({
            isPublish: Taro.getStorageSync(IsPublish),
            selectPoker:selectPoker
        })
        this.refresh();
    }

    componentWillUnmount() {
    }

    componentDidShow() {
    }

    componentDidHide() {
    }

    selectMaster(item,obj){
        //debugger
        let that = this;
        let angid=item.id;
        let selectPoker = that.state.selectPoker;
        selectPoker.augurId=item.id;
        debugger
        putQuestions(selectPoker, '', function (res) {
            that.state.questionsObj = res
            debugger
            that.setState({
                isOpenedModal: true
            })
        });

    }
    questionCommit() {
        let that = this;

        getPrepay({orderId: that.state.questionsObj.orderId}, function (data) {
            let map = {
                timeStamp: data.timeStamp,
                package: data.package,
                paySign: data.paySign,
                signType: data.signType,
                nonceStr: data.nonceStr,
            }
            Taro.requestPayment({
                ...map,
                success: function (res) {
                    Taro.redirectTo({
                        url: '/pages/answer/detail/detail?id=' + data.orderId + '&type=pay'
                    })
                },
                fail: function (res) {
                    showToast('请重新支付或联系管理员');
                }
            })
        });
    }
    getData = (pIndex = pageIndex, callback) => {
        let that = this;
        //getUserQuestions({'PageIndex': pIndex}, function (data) {
          geAugurUsers({},'?pageIndex='+pIndex, function (data) {
            let list = data.items;
            let list2 = [];
            for (let i = 0; i < list.length; i++) {
                let  map = that.judgmentImg(list[i].augurLevel)
                list2.push({
                    ...list[i],
                    ...map
                })
            }

            callback({list: list2});
        });

    };

    refresh = () => {
        showToast('正在加载...');
        let that = this;
        pageIndex = 0;
        this.setState({
            refreshStatus: 1,
            hasMore: true,
            noMore: false
        });

        that.getData(0, function (res) {
            pageIndex++;
            if (res.list.length < 20) {
                that.loadMore();
            }
            that.setState({
                ...res,
                noData: res.list.length < 1,
                refreshStatus: 2,
            });
        });
    }

    loadMore = () => {
        let that = this;
        that.getData(pageIndex, function (res) {
            pageIndex++;
            if (res.list.length > 0) {
                that.setState({
                    list: that.state.list.concat(res.list),
                    hasMore: true,
                    noMore: false
                });
            } else {
                that.setState({
                    hasMore: false,
                    noMore: true,
                    hasMoreText: '没有更多数据了'
                });
            }
        });
    }

    handleScrollEnd = (e) => {
        this.setState({
            scrollTop: e.detail.scrollTop
        })
    }
    judgmentImg(type) {
        //debugger
        let test_item = '';
        let border = '';
        let icon = '';
        let btn = '';

        switch (type) {
            case 1:
                test_item = AugurSelect_Rect4;
                border = AugurSelect_RectLevel4;
                icon = icon3;
                btn = btn3;
                break;
            case 2:
                test_item = AugurSelect_Rect3;
                border = AugurSelect_RectLevel3;
                icon = icon1;
                btn = btn1;
                break;
            case 3:
                test_item = AugurSelect_Rect2;
                border = AugurSelect_RectLevel2;
                icon = icon4;
                btn = btn4;
                break;
                case 4:
                test_item = AugurSelect_Rect1;
                border = AugurSelect_RectLevel1;
                icon = icon4;
                btn = btn4;
                break;
            default:
                test_item = AugurSelect_Rect1;
                border = AugurSelect_RectLevel1;
                icon = icon3;
                btn = btn3;
                break;
        }

        return {
            test_item: test_item,
            border: border,
            icon: icon,
            btn: btn,
        }
    }
    closeModal() {
        this.setState({
            isOpenedModal: false
        })
    }
    render() {
        const {
            hasMoreText,
            noMore,
            hasMore,
            refreshStatus,
            scrollTop,
            list,
            isPublish,
            wen_opacity,
            selectPoker,
            isOpenedModal,
            questionsObj
        } = this.state
        const refresherConfig = {
            recoverTime: 300,
            refreshTime: 1000
        };
        let coupon = questionsObj.coupon || {};
        let questionProductData = questionsObj.questionProduct || {};
        let normalPrice = questionProductData.normalPrice;
        let salesPrice = questionProductData.salesPrice;
        let isShowCouponButton = questionsObj.isShowCouponButton;
        return (
            <View className='test-view'
                  style={`background-image: url(${AugurSelect_bgImg});-moz-background-size:100% 100%; background-size:100% 100%;`}>

           {
                    isOpenedModal ?
                        <AtModal isOpened onClose={this.closeModal.bind(this)}>
                            <AtModalHeader>支付等待占卜师解答</AtModalHeader>
                            <AtModalContent>
                            <View className='tip-view'>你选择了 {questionsObj.augurName || ''} 为您解答！</View>
                                {/* <View className='pd-10'>需支付:<Text
                                    className=' ml-10 font-size-20'>￥{questionsObj.payable / 100}</Text>元</View> */}

                                <View className='pd-10 color-text-5'>原价:<Text
                                    className='ml-10 font-size-14'>￥{salesPrice / 100}</Text>元</View>
                                {
                                    !coupon || coupon == null || coupon == {} ?
                                        isShowCouponButton ?
                                            <View className='display-flex'>
                                                <View className='text-align-center flex1'
                                                      style='line-height:30px'>
                                                    {questionsObj.couponStr}
                                                </View>
                                                <View onClick={this.goToCollect.bind(this)}
                                                      className='goto-view'>去领取</View>
                                            </View>
                                            :
                                            ''
                                        :
                                        <View className='pd-10'>优惠券:<Text
                                            className='ml-10 font-size-14 '>￥{coupon.amountStr}</Text></View>

                                }

                                <View className='tip-view'>{mLoginInfo.questionPayInfo || ''}</View>
                            </AtModalContent>
                            <AtModalAction> <Button onClick={this.questionCommit.bind(this)}>支付</Button> <Button
                                onClick={this.closeModal.bind(this)}>狠心离开</Button> </AtModalAction>
                        </AtModal>
                        :
                        ''
            }

                <View className='bg-content-view'>
                <EPage
                    onRefresh={this.refresh}
                    onLoadMore={this.loadMore}
                    noMore={noMore}
                    hasMore={hasMore}
                    hasMoreText={hasMoreText}
                    refresherConfig={refresherConfig}
                    refreshStatus={refreshStatus}
                    scrollTop={scrollTop}
                    onScrollEnd={this.handleScrollEnd}
                    className='EPage-view'
                >
                {list.map((item, index) => (

                    <View onClick={this.selectMaster.bind(this, item)}   key={index} className='item-view'
                          style={`background-image: url(${item.test_item});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                        <View style="display:flex;padding-top:10px;">
                        <Image mode='scaleToFill' className='item-img-header' src={item.headImgUrl||HEAD_A}></Image>
                        <View className='text-align-left fff flex1  ml-15' style="">
                            {/* <View className='font-size-14  item-title'
                                  style={`background-image: url(${item.border});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                                {item.augurLevelStr}
                            </View> */}
                            <View className='font-size-14 mt-10'>{item.nickName}<Text className='font-size-10  item-title'
                                  style={`background-image: url(${item.border});-moz-background-size:100% 100%; background-size:100% 100%;padding-left: 15px;padding-right: 15px;padding-top: 4px;padding-bottom: 4px;`}>{item.augurLevelStr}</Text>
                                  <Text className='ml-5 font-size-12' style="float: right;margin-right: 10px;">已解答{item.tipPrice}单</Text>
                                  </View>
                            <View className='score-view'>
                            <View class='starcss'>
                {
                    this.state.arrstr.map((item,index)=>{
                        return(
                        <Text style='' id={'sub'+index} name={'sub'+index} class='wirtestar'>{item.url}</Text>
                        )
                    })
                }
                {
                  item.isOnService ? <Text className='ml-5 font-size-10' style="float: right;margin-right: 10px;text-shadow: 3px 0px 8px #89ddd5,-3px 0px 8px #ffffff,0px 3px 8px #ffffff,0px -3px 8px #ffffff;padding-top: 6px;">在线</Text>
                  :
                  <Text className='ml-5 font-size-10' style="float: right;margin-right: 10px;text-shadow: 3px 0px 8px #d9d9d9,-3px 0px 8px #ffffff,0px 3px 8px #ffffff,0px -3px 8px #ffffff;padding-top: 6px;">离线</Text>
                }
            </View>
                                {/* <Image mode='scaleToFill' className='item-img-icon' src=''></Image>*/}
                                
                            </View>
                        </View>
                        </View>
                        {/* <View className='item-img-btn'
                              style={`background-image: url(${item.evaluationScore});-moz-background-size:100% 100%; background-size:100% 100%;`}>
                            提问
                        </View> */}

                        <View className='text-align-left fff flex1 ml-15' style='border-top:0.1px solid #ccc;margin-top:10px;font-size:14px;padding:6px;'>
                                个性签名：{item.signature}
                            </View>
                    </View>

                ))}
                </EPage>
                </View>
            </View>
        )
    }
}
