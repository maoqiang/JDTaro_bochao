import Taro from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { Modal, ActivityIndicator, TouchableHighlight } from 'react-native'
import classNames from 'classnames'
import './index.scss'
 
export default class TaroPop extends Taro.Component {
    /** 
     * @ 弹窗默认配置 
     */
    static defaultProps = {
        isVisible: false,       //弹窗显示
 
        title: '',              //标题
        content: '',            //内容
        contentStyle: null,     //内容样式
        style: null,            //自定义弹窗样式
        skin: '',               //弹窗风格
        icon: '',               //弹窗图标
        xclose: false,          //自定义关闭按钮
 
        shade: true,            //遮罩层
        shadeClose: true,       //点击遮罩关闭
        opacity: '',            //遮罩透明度
        time: 0,                //自动关闭时间
        end: null,              //销毁弹窗回调函数
 
        anim: 'scaleIn',        //弹窗动画
        position: '',           //弹窗位置显示
 
        btns: null,             //弹窗按钮 [{...args}, {...args}]
    }
 
    constructor(props) {
        super(props)
        this.state = {
            ...this.props,
        }
        this.timer = null
    }
 
    /** 
     * @ 显示弹窗事件 
     */
    show = (options) => {
        this.setState({
            ...this.props, ...options, isVisible: true
        })
    }
 
    /** 
     * @ 关闭弹窗事件 
     */
    close = () => {
        this.setState({...this.props})
 
        this.timer && clearTimeout(this.timer)
        delete this.timer
 
        typeof this.state.end === 'function' && this.state.end.call(this)
    }
 
    /** 
     * @ 点击遮罩关闭 
     */
    shadeClick = () => {
        if(!this.state.shadeClose) return
        this.close()
    }
 
    render() {
        let { isVisible, title, content, contentStyle, style, skin, icon, xclose, shade, shadeClose, opacity, time, end, anim, position, btns } = this.state
        
        let toastIcon = {
            loading: require('./skin/loading.png'),
            success: require('./skin/success.png'),
            error: require('./skin/error.png'),
            info: require('./skin/info.png'),
        }
 
        let taroEnv = process.env.TARO_ENV
        // 渲染H5、RN模板
        const renderTpl = (
            <View className="taroPop">
                {/* 遮罩 */}
                {shade ? <View className="atpop__ui_mask" style={{opacity: opacity == '' ? .6 : opacity}} onClick={this.shadeClick} /> : null}
                {/* 窗体 */}
                <View className="atpop__ui_main">
                    <View className={classNames('atpop__ui_child', skin && 'atpop__' + skin, position && 'atpop__ui_child-' + position)} style={style}>
                        {/* 标题 */}
                        {title ? <Text className={classNames('atpop__ui_tit', skin && 'atpop__ui_tit-' + skin)}>{title}</Text> : null}
                        {/* 内容 */}
                        {content ? <View className="atpop__ui_cnt">
                            {/* toast内容 */}
                            {icon && skin === 'toast' ?
                                <View className="atpop__ui_toast">
                                    {icon === 'loading' && taroEnv === 'rn' ?
                                    <ActivityIndicator color="rgba(255,255,255,.5)" size={24} /> : <Image className={classNames('atpop__ui_toast-img', icon=='loading' && 'atpop__ui_toast-img-loading')} src={toastIcon[icon]} mode="aspectFit" />
                                    }
                                </View>
                                :
                                null
                            }
                            {/* 文本内容 */}
                            <Text className={classNames('atpop__ui_cntxt', skin && 'atpop__ui_cntxt-' + skin)} style={contentStyle}>{content}</Text>
                        </View>
                        :
                        this.props.children
                        }
                        {/* 按钮 */}
                        {btns ? <View className={classNames('atpop__ui_btns', skin && 'atpop__ui_btns-' + skin)}>
                            {btns.map((item, i) => {
                                return taroEnv === 'rn' ? 
                                <TouchableHighlight className={classNames('atpop__ui_btn', skin && 'atpop__ui_btn-' + skin)} activeOpacity={1} underlayColor='rgba(200,200,200,.3)' key={i} onPress={item.onClick}>
                                    <Text className={classNames('atpop__ui_btntxt', skin && 'atpop__ui_btntxt-' + skin)} style={item.style}>{item.text}</Text>
                                </TouchableHighlight>
                                :
                                <View className={classNames('atpop__ui_btn', skin && 'atpop__ui_btn-' + skin)} key={i} onClick={item.onClick}>
                                    <Text className={classNames('atpop__ui_btntxt', skin && 'atpop__ui_btntxt-' + skin)} style={item.style}>{item.text}</Text>
                                </View>
                            })}
                        </View>
                        :
                        null
                        }
                    </View>
                    {/* xclose */}
                    {xclose ? <View className="atpop__ui_xclose" onClick={this.close}><Image className="atpop__ui_xclose-img" src={require('./skin/error.png')} mode="aspectFit" /></View> : null}
                </View>
            </View>
        )
 
        // 渲染窗体
        if (taroEnv === 'rn') {
            return (
                <Modal transparent={true} visible={isVisible} onRequestClose={this.close}>
                    {renderTpl}
                </Modal>
            )
        }else if (taroEnv === 'h5' || taroEnv === 'weapp'){
            return isVisible && renderTpl
        }
    }
}