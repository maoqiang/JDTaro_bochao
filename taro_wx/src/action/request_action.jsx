import rConsts from '../constants/request_consts'
import {getApi, postApi, deleteApi, putApi} from '../util/request'
import {showToast} from "../util/app_tool";
import app_tool from "../util/app_tool";

const dataFilter = (res, callback,errback) => {
    if (res && res.code == 0) {
        callback(res.result)
    } else if (res && res.code == 200) {
        app_tool.wxLogin()
    } else if (res) {
        showToast(res.msg);
        if(errback){
            errback(res)
        }
    }
}
export const getDailyTitles = (param, callback) => {
    getApi(rConsts.DAILY_TITLES, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const WxOpen = (param, callback) => {
    getApi(rConsts.WX_OPEN, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const wxUserPut = (param, urlParam, callback) => {
    putApi(rConsts.WX_USERS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const wxUserGet = (param, urlParam, callback) => {
    getApi(rConsts.WX_USERS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getShuffles = (param, callback) => {
    getApi(rConsts.SHUFFLES, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getDailyOrders = (param, callback) => {
    getApi(rConsts.DAILY_ORDERS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const postDailyOrders = (param, urlParam, callback) => {
    postApi(rConsts.DAILY_ORDERS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getPreQuestions = (param, callback) => {
    getApi(rConsts.PRE_QUESTIONS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const postFeedbacks = (param, callback) => {
    postApi(rConsts.FEEDBACKS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getQuestionPrePay = (param, urlParam, callback) => {
    getApi(rConsts.QUESTION_PRE_PAY.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getCoupons = (param={}, urlParam = '', callback) => {
    getApi(rConsts.COUPONS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getPayCalcs = (param={}, urlParam = '', callback,errback) => {
    getApi(rConsts.PAY_CALCS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback,errback)
    });
};

export const getPhone = (param={}, callback) => {
    getApi(rConsts.GET_PHONE, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getAugurHomes = (param={}, callback) => {
    getApi(rConsts.AUGUR_HOMES, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getQuestionsList = (param={}, callback) => {
    getApi(rConsts.QUESTIONS_LIST, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getUserQuestions= (param={}, callback) => {
    getApi(rConsts.USER_QUESTIONS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const postQuestions = (param={},urlParam, callback) => {
    postApi(rConsts.QUESTIONS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const putQuestions = (param={},urlParam, callback) => {
    putApi(rConsts.QUESTIONS.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getQuestions = (param={}, callback) => {
    getApi(rConsts.QUESTIONS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getEvaluates = (param={}, callback) => {
    getApi(rConsts.Evaluate, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const postEvaluates = (param, urlParam, callback) => {
    postApi(rConsts.EvaluateCommit.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getActivities = (param={}, callback) => {
    getApi(rConsts.ACTIVITIES, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getPrepay = (param={}, callback) => {
    getApi(rConsts.PREPAY, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const getHots = (param={}, callback) => {
    getApi(rConsts.HOTS, param).then(function (res) {
        dataFilter(res, callback)
    });
};
export const geAugurUsers = (param={},urlParam,callback) => {
    getApi(rConsts.AugurUsers.concat(urlParam), param).then(function (res) {
        dataFilter(res, callback)
    });
};
