"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getHots = exports.getPrepay = exports.getActivities = exports.getQuestions = exports.putQuestions = exports.postQuestions = exports.getUserQuestions = exports.getQuestionsList = exports.getAugurHomes = exports.getPhone = exports.getPayCalcs = exports.getCoupons = exports.getQuestionPrePay = exports.postFeedbacks = exports.getPreQuestions = exports.postDailyOrders = exports.getDailyOrders = exports.getShuffles = exports.wxUserGet = exports.wxUserPut = exports.WxOpen = exports.getDailyTitles = undefined;

var _request_consts = require("../constants/request_consts.js");

var _request_consts2 = _interopRequireDefault(_request_consts);

var _request = require("../util/request.js");

var _app_tool = require("../util/app_tool.js");

var _app_tool2 = _interopRequireDefault(_app_tool);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dataFilter = function dataFilter(res, callback, errback) {
  if (res && res.code == 0) {
    callback(res.result);
  } else if (res && res.code == 200) {
    _app_tool2.default.wxLogin();
  } else if (res) {
    (0, _app_tool.showToast)(res.msg);
    if (errback) {
      errback(res);
    }
  }
};
var getDailyTitles = exports.getDailyTitles = function getDailyTitles(param, callback) {
  (0, _request.getApi)(_request_consts2.default.DAILY_TITLES, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var WxOpen = exports.WxOpen = function WxOpen(param, callback) {
  (0, _request.getApi)(_request_consts2.default.WX_OPEN, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var wxUserPut = exports.wxUserPut = function wxUserPut(param, urlParam, callback) {
  (0, _request.putApi)(_request_consts2.default.WX_USERS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var wxUserGet = exports.wxUserGet = function wxUserGet(param, urlParam, callback) {
  (0, _request.getApi)(_request_consts2.default.WX_USERS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getShuffles = exports.getShuffles = function getShuffles(param, callback) {
  (0, _request.getApi)(_request_consts2.default.SHUFFLES, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getDailyOrders = exports.getDailyOrders = function getDailyOrders(param, callback) {
  (0, _request.getApi)(_request_consts2.default.DAILY_ORDERS, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var postDailyOrders = exports.postDailyOrders = function postDailyOrders(param, urlParam, callback) {
  (0, _request.postApi)(_request_consts2.default.DAILY_ORDERS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getPreQuestions = exports.getPreQuestions = function getPreQuestions(param, callback) {
  (0, _request.getApi)(_request_consts2.default.PRE_QUESTIONS, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var postFeedbacks = exports.postFeedbacks = function postFeedbacks(param, callback) {
  (0, _request.postApi)(_request_consts2.default.FEEDBACKS, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getQuestionPrePay = exports.getQuestionPrePay = function getQuestionPrePay(param, urlParam, callback) {
  (0, _request.getApi)(_request_consts2.default.QUESTION_PRE_PAY.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getCoupons = exports.getCoupons = function getCoupons() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var urlParam = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var callback = arguments[2];

  (0, _request.getApi)(_request_consts2.default.COUPONS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getPayCalcs = exports.getPayCalcs = function getPayCalcs() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var urlParam = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var callback = arguments[2];
  var errback = arguments[3];

  (0, _request.getApi)(_request_consts2.default.PAY_CALCS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback, errback);
  });
};

var getPhone = exports.getPhone = function getPhone() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.GET_PHONE, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getAugurHomes = exports.getAugurHomes = function getAugurHomes() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.AUGUR_HOMES, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getQuestionsList = exports.getQuestionsList = function getQuestionsList() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.QUESTIONS_LIST, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getUserQuestions = exports.getUserQuestions = function getUserQuestions() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.USER_QUESTIONS, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var postQuestions = exports.postQuestions = function postQuestions() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var urlParam = arguments[1];
  var callback = arguments[2];

  (0, _request.postApi)(_request_consts2.default.QUESTIONS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var putQuestions = exports.putQuestions = function putQuestions() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var urlParam = arguments[1];
  var callback = arguments[2];

  (0, _request.putApi)(_request_consts2.default.QUESTIONS.concat(urlParam), param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getQuestions = exports.getQuestions = function getQuestions() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.QUESTIONS, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getActivities = exports.getActivities = function getActivities() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.ACTIVITIES, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getPrepay = exports.getPrepay = function getPrepay() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.PREPAY, param).then(function (res) {
    dataFilter(res, callback);
  });
};
var getHots = exports.getHots = function getHots() {
  var param = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  (0, _request.getApi)(_request_consts2.default.HOTS, param).then(function (res) {
    dataFilter(res, callback);
  });
};