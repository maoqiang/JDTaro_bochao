"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../constants/oss_consts.js");

var _app_tool = require("../../util/app_tool.js");

var _request_action = require("../../action/request_action.js");

var _enum_consts = require("../../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var timer = null;

var Question = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Question, _BaseComponent);

  function Question() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Question);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Question.__proto__ || Object.getPrototypeOf(Question)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp5", "anonymousState__temp6", "anonymousState__temp7", "anonymousState__temp8", "$compid__1", "isOpenedModal", "isShowCouponButton", "firstCard", "secondCard", "thirdCard", "textAreaValue", "start", "questionsObj", "salesPrice", "coupon", "mLoginInfo", "questionProduct", "questionsId", "barrageList_arr", "barrageList_obj", "barrageList", "phoneWidth", "couponOpen", "isPublish", "selectedIndex", "selectedItemIndex", "selectPoker"], _this.config = {
      navigationBarTitleText: '提问'
    }, _this.customComponents = ["AtModal", "AtModalHeader", "AtModalContent", "AtModalAction"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Question, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Question.prototype.__proto__ || Object.getPrototypeOf(Question.prototype), "_constructor", this).call(this, props);
      this.state = {
        questionProduct: 0.00,
        textAreaValue: '',
        questionsId: 0,
        barrageList_arr: [],
        barrageList_obj: [],
        barrageList: [],
        phoneWidth: 0,
        couponOpen: false,
        isPublish: false,
        selectedIndex: -1,
        selectedItemIndex: -1,
        isOpenedModal: false,
        questionsObj: {},
        selectPoker: {}
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var that = this;
      _index2.default.getSystemInfo({
        success: function success(res) {
          console.log('ww', res);
          that.setState({
            phoneWidth: res.windowWidth - 100
          });
        }
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var that = this;
      var paramStr = this.$router.params.paramStr;
      var mLoginInfo = _index2.default.getStorageSync(_enum_consts.MLoginInfo);
      var selectPoker = JSON.parse(decodeURIComponent(paramStr));
      debugger;
      this.setState({
        isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish),
        selectPoker: selectPoker,
        textAreaValue: selectPoker.questions,
        mLoginInfo: mLoginInfo
      });
      // that.getData();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(this.timer);
    }
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {
      clearInterval(timer);
    }
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "getData",
    value: function getData() {
      var that = this;
      // 添加弹幕所需的时间，初始位置
      (0, _request_action.getPreQuestions)({}, function (data) {
        var hotTopics = [];
        for (var i = 0; i < 4; i++) {}
        hotTopics = hotTopics.concat(data.hotTopics);

        var questionProduct = data.questionProduct;
        var coupons = data.coupons ? data.coupons : [];
        var list = [];
        for (var _i = 0; _i < hotTopics.length; _i++) {
          list.push({
            width: that.state.phoneWidth,
            time: 17,
            height: 20,
            content: hotTopics[_i].question,
            id: hotTopics[_i].id,
            score: hotTopics[_i].score,
            createTime: hotTopics[_i].createTime,
            modifyTime: hotTopics[_i].modifyTime,
            isDel: hotTopics[_i].isDel
          });
        }
        that.state.barrageList_arr = list;
        that.state.questionProduct = questionProduct;

        var couponsList = (0, _app_tool.interceptArray)(coupons, 2);
        that.state.couponsList = couponsList;
        that.state.coupons = coupons;

        that.setState({
          barrageList: (0, _app_tool.interceptArray)(list, Math.ceil(list.length / 4)),
          salesPrice: questionProduct.salesPrice
        });

        if (couponsList && couponsList.length > 0 && couponsList[0].length > 0) {
          that.couponButton(0, 0);
        }
        // setTimeout(() => {
        //     that.barrageListObj();
        // }, 200)
      });
    }

    /**
     *因为从后台获取到的是全部的数据，所以要把数据分开，让每条数据有先后之分
     *每隔一秒往barrageList 数组插入一条数据
     * */

  }, {
    key: "barrageListObj",
    value: function barrageListObj() {
      var that = this;
      var _that$state = that.state,
          barrageList_arr = _that$state.barrageList_arr,
          barrageList = _that$state.barrageList;

      var i = 0;
      timer = setInterval(function () {
        if (barrageList.length > 1000) {
          barrageList = [];
        }
        barrageList.push(barrageList_arr[i]);
        console.log(barrageList.length);
        that.setState({
          barrageList: barrageList
        });

        if (i == barrageList_arr.length - 1) {
          i = 0;
        } else {
          i++;
        }
      }, 1000);
    }
  }, {
    key: "couponButton",
    value: function couponButton(indexItem, indexLi, e) {
      if (e) {
        e.stopPropagation();
      }
      var that = this;
      if (indexItem == -1 || indexLi == -1) {
        return;
      }

      var selectedIndex = this.state.selectedIndex;
      var selectedItemIndex = this.state.selectedIndex;
      var salesPrice = this.state.questionProduct.salesPrice;

      if (selectedIndex == indexLi && selectedItemIndex == indexItem) {
        //二次点击,取消勾选优惠券
        this.setState({
          selectedIndex: -1,
          selectedItemIndex: -1,
          CouponId: 0,
          salesPrice: salesPrice
        });
      } else {
        var CouponId = this.state.couponsList[indexItem][indexLi].id;
        (0, _request_action.getPayCalcs)({ SalesPrice: salesPrice, CouponId: CouponId }, '', function (res) {

          that.setState({
            salesPrice: res,
            selectedIndex: indexLi,
            selectedItemIndex: indexItem,
            CouponId: CouponId
          });
        }, function (err) {});
      }
    }
  }, {
    key: "receiveCouponsBtn",
    value: function receiveCouponsBtn() {
      _index2.default.navigateTo({
        url: '/pages/find/activitys/activitys'
      });
    }
  }, {
    key: "_createInitCouponViewData",
    value: function _createInitCouponViewData(_$uid) {
      var _this2 = this;

      return function () {
        var _state = _this2.state,
            couponOpen = _state.couponOpen,
            couponsList = _state.couponsList,
            selectedIndex = _state.selectedIndex,
            selectedItemIndex = _state.selectedItemIndex;

        var loopArray1 = couponOpen ? couponsList.map(function (item, indexItem) {
          item = {
            $original: (0, _index.internal_get_original)(item)
          };
          var $anonymousCallee__0 = couponOpen ? item.$original.map(function (li, indexLi) {
            li = {
              $original: (0, _index.internal_get_original)(li)
            };
            var $loopState__temp2 = couponOpen ? li.$original.usableStartTime.substring(5, 10).replace('-', '.') : null;
            var $loopState__temp4 = couponOpen ? li.$original.usableEndTime.substring(5, 10).replace('-', '.') : null;
            return {
              $loopState__temp2: $loopState__temp2,
              $loopState__temp4: $loopState__temp4,
              $original: li.$original
            };
          }) : [];
          return {
            $anonymousCallee__0: $anonymousCallee__0,
            $original: item.$original
          };
        }) : [];
        return {
          loopArray1: loopArray1,
          couponOpen: couponOpen,
          couponsList: couponsList,
          arrow_down: _oss_consts.arrow_down,
          arrow_up: _oss_consts.arrow_up,
          selectedItemIndex: selectedItemIndex,
          selectedIndex: selectedIndex,
          checkmark: _oss_consts.checkmark,
          coupon_icon: _oss_consts.coupon_icon
        };
      };
    }
  }, {
    key: "inputListener",
    value: function inputListener(e) {
      var value = e.detail.value;
      this.setState({
        textAreaValue: value
      });
    }
  }, {
    key: "clickBarrage",
    value: function clickBarrage(item) {
      this.setState({
        textAreaValue: item.content,
        questionsId: item.id
      });
    }
  }, {
    key: "everyDayTest",
    value: function everyDayTest() {
      _index2.default.navigateTo({
        url: '../select_poker/select_poker?sourceName=find'
      });
    }
  }, {
    key: "questionCommit",
    value: function questionCommit() {
      var that = this;

      (0, _request_action.getPrepay)({ orderId: that.state.questionsObj.orderId }, function (data) {
        var map = {
          timeStamp: data.timeStamp,
          package: data.package,
          paySign: data.paySign,
          signType: data.signType,
          nonceStr: data.nonceStr
        };
        _index2.default.requestPayment(_extends({}, map, {
          success: function success(res) {
            _index2.default.redirectTo({
              url: '/pages/answer/detail/detail?id=' + data.orderId + '&type=pay'
            });
          },
          fail: function fail(res) {
            (0, _app_tool.showToast)('请重新支付或联系管理员');
          }
        }));
      });
    }
  }, {
    key: "couponClose2Open",
    value: function couponClose2Open(type, e) {
      if (type == 'input' && !this.state.couponOpen) {
        this.setState({
          couponOpen: false
        });
      } else {
        this.setState({
          couponOpen: !this.state.couponOpen
        });
      }
    }
  }, {
    key: "amountProcessing",
    value: function amountProcessing(num) {
      var amout = (0, _app_tool.formatNumber)(num || 0.00);
      return amout.split('.');
    }
  }, {
    key: "startBtn",
    value: function startBtn() {
      var that = this;
      if (this.state.textAreaValue && this.state.textAreaValue.length > 0) {
        var selectPoker = that.state.selectPoker;
        selectPoker.questions = that.state.textAreaValue;
        (0, _request_action.putQuestions)(selectPoker, '', function (res) {
          that.state.questionsObj = res;
          debugger;
          that.setState({
            isOpenedModal: true
          });
        });
      } else {
        (0, _app_tool.showToast)('请输入您要占卜的问题!');
      }
    }
  }, {
    key: "goToCollect",
    value: function goToCollect() {

      _index2.default.navigateTo({
        url: '../find/activitys/activitys'
      });
      this.setState({
        isOpenedModal: false
      });
    }
  }, {
    key: "closeModal",
    value: function closeModal() {
      this.setState({
        isOpenedModal: false
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__1"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__1 = _genCompid2[0],
          $compid__1 = _genCompid2[1];

      var _state2 = this.__state,
          textAreaValue = _state2.textAreaValue,
          isOpenedModal = _state2.isOpenedModal,
          selectPoker = _state2.selectPoker,
          questionsObj = _state2.questionsObj,
          mLoginInfo = _state2.mLoginInfo,
          barrageList = _state2.barrageList,
          isPublish = _state2.isPublish,
          couponOpen = _state2.couponOpen;

      var coupon = questionsObj.coupon || {};
      var questionProductData = questionsObj.questionProduct || {};
      var normalPrice = questionProductData.normalPrice;
      var salesPrice = questionProductData.salesPrice;
      var isShowCouponButton = questionsObj.isShowCouponButton;

      var firstCard = selectPoker.firstCard || {};
      var secondCard = selectPoker.secondCard || {};
      var thirdCard = selectPoker.thirdCard || {};

      var anonymousState__temp5 = firstCard.picUrl ? firstCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(firstCard.isInversion)) : null;
      var anonymousState__temp6 = secondCard.picUrl ? secondCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(secondCard.isInversion)) : null;
      var anonymousState__temp7 = thirdCard.picUrl ? thirdCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(thirdCard.isInversion)) : null;
      var anonymousState__temp8 = !coupon || coupon == null || coupon == {};
      isOpenedModal && _index.propsManager.set({
        "isOpened": true,
        "onClose": this.closeModal.bind(this)
      }, $compid__1, $prevCompid__1);
      Object.assign(this.__state, {
        anonymousState__temp5: anonymousState__temp5,
        anonymousState__temp6: anonymousState__temp6,
        anonymousState__temp7: anonymousState__temp7,
        anonymousState__temp8: anonymousState__temp8,
        $compid__1: $compid__1,
        isShowCouponButton: isShowCouponButton,
        firstCard: firstCard,
        secondCard: secondCard,
        thirdCard: thirdCard,
        start: _oss_consts.start,
        salesPrice: salesPrice,
        coupon: coupon,
        mLoginInfo: mLoginInfo
      });
      return this.__state;
    }
  }]);

  return Question;
}(_index.Component), _class.$$events = ["couponClose2Open", "couponButton", "receiveCouponsBtn", "goToCollect", "questionCommit", "closeModal", "couponClose2Open", "inputListener", "startBtn"], _class.$$componentPath = "pages/question/question", _temp2);
exports.default = Question;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Question, true));