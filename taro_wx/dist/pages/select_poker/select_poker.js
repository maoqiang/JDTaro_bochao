"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../constants/oss_consts.js");

var _request_action = require("../../action/request_action.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Mine = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Mine, _BaseComponent);

  function Mine() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Mine);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Mine.__proto__ || Object.getPrototypeOf(Mine)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp3", "loopArray238", "loopArray239", "donghua2", "poker_bg", "selectList", "sourceName", "select_poker", "list", "moveVioew", "appNameColor", "pageNum", "distance", "winWidth", "currentAngle", "angle", "isShow"], _this.config = {
      navigationBarTitleText: '抽牌',
      disableScroll: true
      // 移动效果处理
    }, _this.state = {
      moveVioew: {
        transform: 'translate(0,0)'
      },
      appNameColor: 'whiteName',
      pageNum: 0
    }, _this.deriction = true, _this.pageNum = 0, _this.MIN_TOUCH_DISTENCE = 50, _this.startX = 0, _this.handleTouchStart = function (e) {
      _this.startX = e.touches[0].clientX;
    }, _this.handleTouchMove = function (e) {
      var winWidth = _this.state.winWidth;
      var currentAngle = _this.state.currentAngle;

      _this.endX = e.touches[0].clientX;
      var distance = _this.startX - _this.endX;

      var angle = -(distance / winWidth * 117) + currentAngle;

      if (distance < 0) {
        angle = angle > 360 ? 360 : angle;
      } else {
        angle = angle < 125 ? 125 : angle;
      }
      _this.setState({
        angle: angle
      });
    }, _this.handleTouchEnd = function (e) {
      // 获取滑动范围
      _this.state.currentAngle = _this.state.angle;
    }, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }
  // 滑动方向

  // 页码

  // 手势滑动最小距离


  _createClass(Mine, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Mine.prototype.__proto__ || Object.getPrototypeOf(Mine.prototype), "_constructor", this).call(this, props);
      this.state = {
        list: [],
        moveVioew: {
          transform: 'translate(0,0)'
        },
        appNameColor: 'whiteName',
        pageNum: 0,
        distance: 0,
        winWidth: 0,
        currentAngle: 243,
        angle: 243,
        isShow: true,
        selectList: [{
          img: _oss_consts.select_poker_bg1,
          selectIng: false
        }, {
          img: _oss_consts.select_poker_bg1,
          selectIng: false

        }]
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var that = this;
      var sourceName = this.$router.params.sourceName;
      this.state.answerType = this.$router.params.type;

      this.state.questions = this.$router.params.questions;
      this.state.questions_id = this.$router.params.questions_id;
      this.state.orderId = this.$router.params.orderId;
      var type = void 0;
      var selectList = this.state.selectList;
      if (sourceName == 'find') {
        //发现
        type = 1;
      } else {
        //测
        type = 0;
        selectList.push({
          img: _oss_consts.select_poker_bg1,
          selectIng: false
        });
      }
      (0, _request_action.getShuffles)({ type: type }, function (data) {
        that.state.majorArcanas = data.majorArcanas; //22
        that.state.minorArcanas = data.minorArcanas; //22

        that.state.divinationCards = data.divinationCards; //78
        that.state.tarotGroupId = data.tarotGroupId;
        that.state.urlBase = data.urlBase;
        that.rotationPosition(selectList, sourceName);
      });
      setTimeout(function () {
        that.setState({ isShow: false });
      }, 3000);
    }
  }, {
    key: "hideGif",
    value: function hideGif() {
      var that = this;
      that.setState.isShow = false;
    }
    // 计算旋转定位

  }, {
    key: "rotationPosition",
    value: function rotationPosition(selectList, sourceName) {
      var winWidth = _index2.default.getSystemInfoSync().windowWidth;
      this.state.winWidth = winWidth;

      var list = [];
      var length = 78;
      for (var _i = 0; _i < length; _i++) {
        list.push({
          index: _i
        });
      }

      var rotateList = [];
      var i = 1;
      list.map(function (e, index) {
        rotateList.push(i * 3);
        i++;
      });

      this.setState({
        list: list,
        rotateList: rotateList,
        sourceName: sourceName,
        selectList: selectList
      });
    }

    /*手接触屏幕*/

    /*手在屏幕上移动*/

    /*手离开屏幕*/

  }, {
    key: "selectOnclick",
    value: function selectOnclick(index, e) {
      var _this2 = this;

      var divinationCards = this.state.divinationCards; //78
      var majorArcanas = this.state.majorArcanas; //22
      var minorArcanas = this.state.minorArcanas;
      var urlBase = this.state.urlBase;

      var selectList = this.state.selectList;

      if (selectList[selectList.length - 1].selectIng) {
        return;
      }
      var list = this.state.list;

      var oldIndex = this.state.oldIndex;
      list[index].height = 165;
      if (oldIndex && oldIndex > -1) {
        list[oldIndex].height = 160;
      }

      //选中
      for (var i = 0; i < selectList.length; i++) {
        if (!selectList[i].selectIng) {
          if (this.state.sourceName == 'find') {
            //发现
            selectList[i].img = _oss_consts.select_poker;
            if (i == 0) {
              var majorIndex = index % majorArcanas.length;
              selectList[i].majorCardIndex = majorArcanas[majorIndex].cardIndex;
              selectList[i].isMajorInversion = majorArcanas[majorIndex].isInversion;
              selectList[i].MajorCardUrl = urlBase.concat(majorArcanas[majorIndex].picName);
            } else {
              var minorIndex = index % minorArcanas.length;
              selectList[i].minorCardIndex = minorArcanas[minorIndex].cardIndex;
              selectList[i].isMinorInversion = minorArcanas[minorIndex].isInversion;
              selectList[i].MinorCardUrl = urlBase.concat(minorArcanas[minorIndex].picName);
            }
          } else {
            selectList[i].img = urlBase.concat(divinationCards[index].picName);
            selectList[i].cardIndex = divinationCards[index].cardIndex;
            selectList[i].cardName = divinationCards[index].cardName;
            selectList[i].isInversion = divinationCards[index].isInversion;
          }

          selectList[i].selectIng = true;
          selectList[i].selectPoker = list[index];
          break;
        }
      }

      list.splice(index, 1);
      if (this.state.sourceName == 'find') {
        //发
        if (index == 0) {
          majorArcanas.splice(index, 1);
        } else {
          minorArcanas.splice(index, 1);
        }
      } else {
        divinationCards.splice(index, 1);
      }
      index = -1;

      // if (index == oldIndex) {
      //
      // }
      debugger;

      this.setState({
        list: list,
        oldIndex: index,
        selectList: selectList
      }, function () {
        if (selectList[selectList.length - 1].selectIng) {
          _this2.commit();
        }
      });
    }
  }, {
    key: "commit",
    value: function commit() {
      var that = this;
      var map = {};
      var selectList = this.state.selectList;
      var urlBase = this.state.urlBase;

      if (this.state.sourceName == 'find') {
        //发现

        var major = selectList[0];
        var minor = selectList[1];

        debugger;
        map = {
          "cardGroupId": this.state.tarotGroupId,
          "majorCardIndex": major.majorCardIndex,
          "isMajorInversion": major.isMajorInversion,
          MajorCardUrl: major.MajorCardUrl,

          "minorCardIndex": minor.minorCardIndex,
          "isMinorInversion": minor.isMinorInversion,
          MinorCardUrl: minor.MinorCardUrl
        };

        (0, _request_action.postDailyOrders)(map, '?id=' + this.state.orderId, function (data) {
          _index2.default.redirectTo({
            url: '../find/result/result?id=' + data.id
          });
        });
      } else {

        map = {
          "hotTopicId": this.state.questions_id,
          "questions": this.state.questions,
          "firstCard": {
            "cardIndex": selectList[0].cardIndex,
            "cardName": selectList[0].cardName,
            "picUrl": selectList[0].img,
            "isInversion": selectList[0].isInversion
          },
          "secondCard": {
            "cardIndex": selectList[1].cardIndex,
            "cardName": selectList[1].cardName,
            "picUrl": selectList[1].img,
            "isInversion": selectList[1].isInversion
          },
          "thirdCard": {
            "cardIndex": selectList[2].cardIndex,
            "cardName": selectList[2].cardName,
            "picUrl": selectList[2].img,
            "isInversion": selectList[2].isInversion
          }
        };

        var paramStr = encodeURIComponent(JSON.stringify(map));
        debugger;
        _index2.default.redirectTo({
          url: '../question/question?paramStr=' + paramStr
        });
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _state = this.__state,
          list = _state.list,
          angle = _state.angle,
          rotateList = _state.rotateList,
          selectList = _state.selectList,
          sourceName = _state.sourceName,
          isShow = _state.isShow;

      var anonymousState__temp3 = (0, _index.internal_inline_style)({
        transform: ["rotate(" + angle + "deg)"]
      });
      var loopArray238 = selectList.map(function (item, index) {
        item = {
          $original: (0, _index.internal_get_original)(item)
        };
        var $loopState__temp2 = item.$original.selectIng && sourceName != 'find' ? item.$original.img.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(item.$original.isInversion)) : null;
        return {
          $loopState__temp2: $loopState__temp2,
          $original: item.$original
        };
      });
      var loopArray239 = list.map(function (item, i) {
        item = {
          $original: (0, _index.internal_get_original)(item)
        };
        var $loopState__temp5 = (0, _index.internal_inline_style)({ transform: ["rotate(" + rotateList[i] + "deg)"], height: item.$original.height + "px" });
        return {
          $loopState__temp5: $loopState__temp5,
          $original: item.$original
        };
      });
      this.$$refs.pushRefs([{
        type: "dom",
        id: "bcjzz",
        refName: "taroPop",
        fn: null
      }]);
      Object.assign(this.__state, {
        anonymousState__temp3: anonymousState__temp3,
        loopArray238: loopArray238,
        loopArray239: loopArray239,
        donghua2: _oss_consts.donghua2,
        poker_bg: _oss_consts.poker_bg,
        sourceName: sourceName,
        select_poker: _oss_consts.select_poker
      });
      return this.__state;
    }
  }]);

  return Mine;
}(_index.Component), _class.$$events = ["handleTouchStart", "handleTouchMove", "handleTouchEnd", "selectOnclick"], _class.$$componentPath = "pages/select_poker/select_poker", _temp2);
exports.default = Mine;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Mine, true));