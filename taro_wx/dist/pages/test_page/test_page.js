"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;
// import "taro-ui/dist/style/index.scss"


var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _enum_consts = require("../../constants/enum_consts.js");

var _oss_consts = require("../../constants/oss_consts.js");

var _app_tool = require("../../util/app_tool.js");

var _request_action = require("../../action/request_action.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var tabList = [{ title: '感情' }, { title: '婚姻' }, { title: '事业' }, { title: '财运' }];
var pageIndex = 1;

var Test_page = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Test_page, _BaseComponent);

  function Test_page() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Test_page);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Test_page.__proto__ || Object.getPrototypeOf(Test_page)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["loopArray3", "$compid__2", "$compid__3", "$compid__4", "$compid__5", "$compid__6", "isPublish", "undefined", "mLoginInfo", "typeItems", "anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "anonymousState__temp4", "home1_04", "list", "HEAD_A", "current", "noMore", "hasMore", "scrollTop", "noAut", "noData", "hasMoreText"], _this.config = {
      navigationBarTitleText: '塔罗测测',
      enablePullDownRefresh: true,
      backgroundTextStyle: 'dark',
      onReachBottomDistance: 50
    }, _this.getData = function () {
      var pIndex = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : pageIndex;
      var callback = arguments[1];

      var that = _this;
      (0, _request_action.getQuestionsList)({ 'PageIndex': pIndex }, function (data) {
        var list = data.items;

        callback({ list: list });
      });
    }, _this.refresh = function () {
      var that = _this;
      pageIndex = 0;
      _this.setState({
        refreshStatus: 1,
        hasMore: true,
        noMore: false
      });

      that.getData(0, function (res) {
        pageIndex++;
        if (res.list.length < 20) {
          that.loadMore();
        }
        that.setState(_extends({}, res, {
          noData: res.list.length < 1,
          refreshStatus: 2
        }));
      });
    }, _this.loadMore = function () {
      var that = _this;
      that.getData(pageIndex, function (res) {
        pageIndex++;
        if (res.list.length > 0) {
          that.setState({
            list: that.state.list.concat(res.list),
            hasMore: true,
            noMore: false
          });
        } else {
          that.setState({
            hasMore: false,
            noMore: true,
            hasMoreText: '没有更多数据了'
          });
        }
      });
    }, _this.handleScrollEnd = function (e) {
      _this.setState({
        scrollTop: e.detail.scrollTop
      });
    }, _this.customComponents = ["AtTabs", "AtTabsPane"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Test_page, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Test_page.prototype.__proto__ || Object.getPrototypeOf(Test_page.prototype), "_constructor", this).call(this, props);
      this.state = {
        mLoginInfo: {},
        current: 0,
        noMore: false,
        hasMore: true,
        scrollTop: 0,
        list: [],
        noAut: false,
        noData: false,
        hasMoreText: '稍安勿躁,加载中...',
        isPublish: false
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var mLoginInfo = _index2.default.getStorageSync(_enum_consts.MLoginInfo);

      this.setState({
        isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish)
      });
      // this.refresh();

      var list1 = [];
      for (var i = 0; i < 3; i++) {
        list1.push({
          text: '123' + i
        });
      }

      this.setState({
        mUserInfo: mLoginInfo.userInfo,
        mLoginInfo: mLoginInfo,
        list1: list1
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {
      this.onPullDownRefresh();
    }
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "handleClick",
    value: function handleClick(value) {
      this.setState({
        current: value
      });
    }
  }, {
    key: "onPullDownRefresh",
    value: function onPullDownRefresh() {
      var that = this;
      that.getData(0, function (res) {
        that.setState(_extends({}, res, {
          noData: res.list.length < 1,
          refreshStatus: 2
        }));
        _index2.default.stopPullDownRefresh();
      });
    }
  }, {
    key: "onReachBottom",
    value: function onReachBottom() {
      console.log("页面上拉触底事件的处理函数");
    }
  }, {
    key: "detailBtn",
    value: function detailBtn(item, e) {
      if (item.state == 1) {
        _index2.default.navigateTo({
          url: '/pages/select_poker/select_poker?type=answer&sourceName=test&questions=' + item.question + '&orderId=' + item.id
        });
      } else {
        _index2.default.navigateTo({
          url: '/pages/answer/detail/detail?id=' + item.id
        });
      }
    }
  }, {
    key: "startTest",
    value: function startTest() {
      var item = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var questions = item.question || '';
      var id = item.id || '';

      if (this.state.isPublish) {
        _index2.default.navigateTo({
          url: '../select_poker/select_poker?sourceName=test&questions=' + questions + '&id=' + id
        });
      } else {
        _index2.default.navigateTo({
          url: '../select_poker/select_poker?sourceName=find'
        });
      }
    }
  }, {
    key: "_createInitListViewData",
    value: function _createInitListViewData(_$uid) {
      return function (list) {

        if (list) {}
        return {
          list: list,
          arrow_right: _oss_consts.arrow_right
        };
      };
    }
  }, {
    key: "receiveCouponsBtn",
    value: function receiveCouponsBtn() {
      _index2.default.navigateTo({
        url: '/pages/find/activitys/activitys'
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__2"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__2 = _genCompid2[0],
          $compid__2 = _genCompid2[1];

      var _genCompid3 = (0, _index.genCompid)(__prefix + "$compid__3"),
          _genCompid4 = _slicedToArray(_genCompid3, 2),
          $prevCompid__3 = _genCompid4[0],
          $compid__3 = _genCompid4[1];

      var _genCompid5 = (0, _index.genCompid)(__prefix + "$compid__4"),
          _genCompid6 = _slicedToArray(_genCompid5, 2),
          $prevCompid__4 = _genCompid6[0],
          $compid__4 = _genCompid6[1];

      var _genCompid7 = (0, _index.genCompid)(__prefix + "$compid__5"),
          _genCompid8 = _slicedToArray(_genCompid7, 2),
          $prevCompid__5 = _genCompid8[0],
          $compid__5 = _genCompid8[1];

      var _genCompid9 = (0, _index.genCompid)(__prefix + "$compid__6"),
          _genCompid10 = _slicedToArray(_genCompid9, 2),
          $prevCompid__6 = _genCompid10[0],
          $compid__6 = _genCompid10[1];

      debugger;
      var _state = this.__state,
          list1 = _state.list1,
          hasMoreText = _state.hasMoreText,
          noMore = _state.noMore,
          hasMore = _state.hasMore,
          refreshStatus = _state.refreshStatus,
          scrollTop = _state.scrollTop,
          list = _state.list,
          isPublish = _state.isPublish,
          _state$mLoginInfo = _state.mLoginInfo,
          mLoginInfo = _state$mLoginInfo === undefined ? {} : _state$mLoginInfo;


      var hotTopics = mLoginInfo.hotTopics || {};
      var typeItems = hotTopics.typeItems || [];
      var tabList = [];
      for (var i = 0; i < typeItems.length; i++) {
        tabList.push({
          title: typeItems[i].typeName
        });
      }
      var refresherConfig = {
        recoverTime: 300,
        refreshTime: 1000
      };
      var anonymousState__temp = typeItems.length > 3 ? this._createInitListViewData(__prefix + "bzzzzzzzzz")(typeItems[0].items, 0) : null;
      var anonymousState__temp2 = typeItems.length > 3 ? this._createInitListViewData(__prefix + "czzzzzzzzz")(typeItems[1].items, 1) : null;
      var anonymousState__temp3 = typeItems.length > 3 ? this._createInitListViewData(__prefix + "dzzzzzzzzz")(typeItems[2].items, 2) : null;
      var anonymousState__temp4 = typeItems.length > 3 ? this._createInitListViewData(__prefix + "ezzzzzzzzz")(typeItems[3].items, 3) : null;
      var loopArray3 = list.map(function (item, index) {
        item = {
          $original: (0, _index.internal_get_original)(item)
        };
        var $loopState__temp6 = isPublish ? (0, _app_tool.formatNumber)(item.$original.priced || 0) : null;
        var $loopState__temp8 = (0, _app_tool.timeAgo)(item.$original.createTime);
        return {
          $loopState__temp6: $loopState__temp6,
          $loopState__temp8: $loopState__temp8,
          $original: item.$original
        };
      });
      typeItems.length > 3 && _index.propsManager.set({
        "current": this.__state.current,
        "tabList": tabList,
        "onClick": this.handleClick.bind(this)
      }, $compid__2, $prevCompid__2);
      typeItems.length > 3 && _index.propsManager.set({
        "current": this.__state.current,
        "index": 0
      }, $compid__3, $prevCompid__3);
      typeItems.length > 3 && _index.propsManager.set({
        "current": this.__state.current,
        "index": 1
      }, $compid__4, $prevCompid__4);
      typeItems.length > 3 && _index.propsManager.set({
        "current": this.__state.current,
        "index": 2
      }, $compid__5, $prevCompid__5);
      typeItems.length > 3 && _index.propsManager.set({
        "current": this.__state.current,
        "index": 3
      }, $compid__6, $prevCompid__6);
      Object.assign(this.__state, {
        loopArray3: loopArray3,
        $compid__2: $compid__2,
        $compid__3: $compid__3,
        $compid__4: $compid__4,
        $compid__5: $compid__5,
        $compid__6: $compid__6,
        undefined: undefined,
        typeItems: typeItems,
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        anonymousState__temp4: anonymousState__temp4,
        home1_04: _oss_consts.home1_04,
        HEAD_A: _oss_consts.HEAD_A
      });
      return this.__state;
    }
  }]);

  return Test_page;
}(_index.Component), _class.$$events = ["startTest", "receiveCouponsBtn", "startTest", "detailBtn"], _class.$$componentPath = "pages/test_page/test_page", _temp2);
exports.default = Test_page;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Test_page, true));