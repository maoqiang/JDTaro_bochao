"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _request_action = require("../../../action/request_action.js");

var _oss_consts = require("../../../constants/oss_consts.js");

var _enum_consts = require("../../../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Activitys = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Activitys, _BaseComponent);

  function Activitys() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Activitys);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Activitys.__proto__ || Object.getPrototypeOf(Activitys)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["data", "huodong_bg", "inviteImgUrl"], _this.config = {
      navigationBarTitleText: '活动'
    }, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Activitys, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Activitys.prototype.__proto__ || Object.getPrototypeOf(Activitys.prototype), "_constructor", this).call(this, props);
      this.state = {
        data: {},
        inviteImgUrl: ''
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var that = this;
      (0, _request_action.getActivities)({}, function (data) {
        that.setState({
          data: data,
          inviteImgUrl: data.inviteImgUrl
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "goButton",
    value: function goButton(e) {
      _index2.default.switchTab({
        url: '../../mine/mine'
      });
    }
  }, {
    key: "onShareAppMessage",
    value: function onShareAppMessage(res) {
      var inviteImgUrl = this.state.inviteImgUrl;
      var shareTitle = _index2.default.getStorageSync(_enum_consts.MLoginInfo).shareTitle;
      debugger;
      return {
        title: shareTitle,
        path: '/pages/login/login?id=' + _index2.default.getStorageSync(_enum_consts.UserId),
        imageUrl: inviteImgUrl + '?time=' + Date.now()
      };
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var data = this.__state.data;

      Object.assign(this.__state, {
        huodong_bg: _oss_consts.huodong_bg
      });
      return this.__state;
    }
  }]);

  return Activitys;
}(_index.Component), _class.$$events = ["goButton"], _class.$$componentPath = "pages/find/activitys/activitys", _temp2);
exports.default = Activitys;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Activitys, true));