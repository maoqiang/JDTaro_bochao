"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;
//import TaroPop from '@components/taroPop'
//import TaroPop from 'components/taroPop'

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../constants/oss_consts.js");

var _app_tool = require("../../util/app_tool.js");

var _enum_consts = require("../../constants/enum_consts.js");

var _request_action = require("../../action/request_action.js");

var _request_consts = require("../../constants/request_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HOME = (_temp2 = _class = function (_BaseComponent) {
  _inherits(HOME, _BaseComponent);

  function HOME() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, HOME);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = HOME.__proto__ || Object.getPrototypeOf(HOME)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["$compid__29", "home_surpris", "isPublish", "home_bg", "find_bg", "home_mine", "home_anwser", "home_title", "home_freeicon", "one", "two", "three", "four", "home_lqzx", "mUserInfo", "userId", "isWxAuthed", "data", "swiperList", "isOpened", "isNewUser"], _this.config = {
      navigationBarTitleText: '塔罗测测'
    }, _this.customComponents = ["AtModal", "AtModalHeader", "AtModalContent", "AtModalAction"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(HOME, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(HOME.prototype.__proto__ || Object.getPrototypeOf(HOME.prototype), "_constructor", this).call(this, props);
      this.state = {
        userId: '0',
        isWxAuthed: true,
        data: {},
        swiperList: [],
        isPublish: false,
        isOpened: false,
        isNewUser: false
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var that = this;
      var mUserInfo = _index2.default.getStorageSync(_enum_consts.MuserInfo);
      (0, _request_action.getDailyTitles)({}, function (res) {
        var activities = (0, _app_tool.interceptArray)(res.activities, 2);
        that.setState({
          data: res,
          swiperList: activities,
          mUserInfo: mUserInfo,
          isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish)
        });
      });

      var shareId = this.$router.params.id || 0;
      var version = _request_consts.versionCode.replace(/\./g, '');
      (0, _app_tool.wxLogin)({ userId: shareId, version: version }, function (res) {
        //debugger
        if (res && !res.isNewUser) {
          that.setState({
            //isNewUser:res.isNewUser
            isNewUser: true
          });
        }
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var that = this;
      //let taroPop = this.refs.taroPop;
    }
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "getUserInfo",
    value: function getUserInfo(e) {
      var that = this;
      (0, _app_tool.getUserInfoTool)(e.detail, function (res) {
        that.setState({
          mUserInfo: _index2.default.getStorageSync(_enum_consts.MuserInfo)
        }, function () {
          that.everyDayTest();
        });
      });
    }
  }, {
    key: "everyDayTest",
    value: function everyDayTest() {
      (0, _request_action.getDailyTitles)({}, function (data) {
        var dailyResultId = data.dailyResultId;
        if (dailyResultId && dailyResultId.length > 10) {
          _index2.default.navigateTo({
            url: '../find/result/result?id=' + dailyResultId
          });
        } else {
          _index2.default.navigateTo({
            url: '../select_poker/select_poker?sourceName=find'
          });
        }
      });
    }
  }, {
    key: "goodsButton",
    value: function goodsButton(indexItem, indexLi, e) {
      var swiperList = this.state.swiperList;
      var goodItem = {};
      if (indexItem == -1 && indexLi == -1) {
        goodItem = swiperList[0];
      } else {
        goodItem = swiperList[indexItem][indexLi];
      }

      _index2.default.navigateTo({
        url: './activitys/activitys'
      });
    }
  }, {
    key: "openMineBtn",
    value: function openMineBtn() {
      _index2.default.navigateTo({
        url: '../mine/mine'
      });
    }
  }, {
    key: "openAnswerBtn",
    value: function openAnswerBtn() {
      _index2.default.navigateTo({
        url: '../mine/mine_answer/mine_answer'
        //url: '../test/test'
      });
    }
  }, {
    key: "openLQZXBtn",
    value: function openLQZXBtn() {
      _index2.default.navigateTo({
        url: '../find/activitys/activitys'
      });
    }
  }, {
    key: "openTestBtn",
    value: function openTestBtn() {
      _index2.default.navigateTo({
        url: '../test/test'
      });
    }
  }, {
    key: "hideSurpris",
    value: function hideSurpris() {
      var that = this;
      //this.refs.taroPop.close()
      that.setState({
        isNewUser: false
        //isNewUser:true
      });
    }
  }, {
    key: "openList",
    value: function openList() {
      var that = this;
      //this.refs.taroPop.close()
      that.setState({
        isNewUser: false
        //isNewUser:true
      });
      _index2.default.navigateTo({
        url: '../test/test'
      });
    }
  }, {
    key: "settingClick",
    value: function settingClick(index, e) {
      switch (index) {
        case 0:
          this.setState({ isOpened: true });
          break;
        case 1:
          this.setState({ isOpened: false });
          break;
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__29"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__29 = _genCompid2[0],
          $compid__29 = _genCompid2[1];

      var _state = this.__state,
          mUserInfo = _state.mUserInfo,
          isPublish = _state.isPublish,
          isOpened = _state.isOpened,
          isNewUser = _state.isNewUser;

      _index.propsManager.set({
        "isOpened": isOpened
      }, $compid__29, $prevCompid__29);
      this.$$refs.pushRefs([{
        type: "dom",
        id: "ddzzz",
        refName: "taroPop",
        fn: null
      }]);
      Object.assign(this.__state, {
        $compid__29: $compid__29,
        home_surpris: _oss_consts.home_surpris,
        home_bg: _oss_consts.home_bg,
        find_bg: _oss_consts.find_bg,
        home_mine: _oss_consts.home_mine,
        home_anwser: _oss_consts.home_anwser,
        home_title: _oss_consts.home_title,
        home_freeicon: _oss_consts.home_freeicon,
        one: _oss_consts.one,
        two: _oss_consts.two,
        three: _oss_consts.three,
        four: _oss_consts.four,
        home_lqzx: _oss_consts.home_lqzx,
        mUserInfo: mUserInfo
      });
      return this.__state;
    }
  }]);

  return HOME;
}(_index.Component), _class.$$events = ["openList", "hideSurpris", "settingClick", "openMineBtn", "openAnswerBtn", "getUserInfo", "openTestBtn", "openLQZXBtn", "everyDayTest"], _class.$$componentPath = "pages/find/home", _temp2);
exports.default = HOME;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(HOME, true));