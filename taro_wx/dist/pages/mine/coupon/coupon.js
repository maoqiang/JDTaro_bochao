"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;
// import "taro-ui/dist/style/index.scss"

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../../constants/oss_consts.js");

var _request_action = require("../../../action/request_action.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Mine = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Mine, _BaseComponent);

  function Mine() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Mine);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Mine.__proto__ || Object.getPrototypeOf(Mine)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["$compid__7", "$compid__8", "$compid__9", "$compid__10", "anonymousState__temp5", "anonymousState__temp6", "anonymousState__temp7", "current", "list"], _this.config = {
      navigationBarTitleText: '优惠券',
      disableScroll: true
    }, _this.customComponents = ["AtTabs", "AtTabsPane"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Mine, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Mine.prototype.__proto__ || Object.getPrototypeOf(Mine.prototype), "_constructor", this).call(this, props);
      this.state = {
        current: 0,

        list: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var that = this;
      (0, _request_action.getCoupons)({}, '', function (data) {
        var usableList = data.usableList;
        var usedList = data.usedList;
        var expiredList = data.expiredList;
        that.setState({
          usableList: usableList,
          usedList: usedList,
          expiredList: expiredList
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "_createInitListViewData",
    value: function _createInitListViewData(_$uid) {
      return function (list, type) {
        var _$url;

        var loopArray8 = void 0;

        if (list) {
          _$url = '';

          if (type == 0) {
            _$url = _oss_consts.coupon_icon;
          } else if (type == 1) {
            _$url = _oss_consts.coupon_used;
          } else {
            _$url = _oss_consts.coupon_used;
          }

          loopArray8 = list.map(function (item, index) {
            item = {
              $original: (0, _index.internal_get_original)(item)
            };
            var $loopState__temp2 = item.$original.usableStartTime.substring(5, 10).replace('-', '.');
            var $loopState__temp4 = item.$original.usableEndTime.substring(5, 10).replace('-', '.');
            return {
              url: _$url,
              $loopState__temp2: $loopState__temp2,
              $loopState__temp4: $loopState__temp4,
              $original: item.$original
            };
          });
        }
        return {
          loopArray8: loopArray8,
          list: list
        };
      };
    }
  }, {
    key: "handleClick",
    value: function handleClick(value) {
      this.setState({
        current: value
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__7"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__7 = _genCompid2[0],
          $compid__7 = _genCompid2[1];

      var _genCompid3 = (0, _index.genCompid)(__prefix + "$compid__8"),
          _genCompid4 = _slicedToArray(_genCompid3, 2),
          $prevCompid__8 = _genCompid4[0],
          $compid__8 = _genCompid4[1];

      var _genCompid5 = (0, _index.genCompid)(__prefix + "$compid__9"),
          _genCompid6 = _slicedToArray(_genCompid5, 2),
          $prevCompid__9 = _genCompid6[0],
          $compid__9 = _genCompid6[1];

      var _genCompid7 = (0, _index.genCompid)(__prefix + "$compid__10"),
          _genCompid8 = _slicedToArray(_genCompid7, 2),
          $prevCompid__10 = _genCompid8[0],
          $compid__10 = _genCompid8[1];

      var tabList = [{ title: '可使用' }, { title: '已使用' }, { title: '已过期' }];
      var _state = this.__state,
          usableList = _state.usableList,
          usedList = _state.usedList,
          expiredList = _state.expiredList;


      var anonymousState__temp5 = this._createInitListViewData(__prefix + "fzzzzzzzzz")(usableList, 0);

      var anonymousState__temp6 = this._createInitListViewData(__prefix + "gzzzzzzzzz")(usedList, 1);

      var anonymousState__temp7 = this._createInitListViewData(__prefix + "hzzzzzzzzz")(expiredList, 2);

      _index.propsManager.set({
        "current": this.__state.current,
        "tabList": tabList,
        "onClick": this.handleClick.bind(this)
      }, $compid__7, $prevCompid__7);
      _index.propsManager.set({
        "current": this.__state.current,
        "index": 0
      }, $compid__8, $prevCompid__8);
      _index.propsManager.set({
        "current": this.__state.current,
        "index": 1
      }, $compid__9, $prevCompid__9);
      _index.propsManager.set({
        "current": this.__state.current,
        "index": 2
      }, $compid__10, $prevCompid__10);
      Object.assign(this.__state, {
        $compid__7: $compid__7,
        $compid__8: $compid__8,
        $compid__9: $compid__9,
        $compid__10: $compid__10,
        anonymousState__temp5: anonymousState__temp5,
        anonymousState__temp6: anonymousState__temp6,
        anonymousState__temp7: anonymousState__temp7
      });
      return this.__state;
    }
  }]);

  return Mine;
}(_index.Component), _class.$$events = [], _class.$$componentPath = "pages/mine/coupon/coupon", _temp2);
exports.default = Mine;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Mine, true));