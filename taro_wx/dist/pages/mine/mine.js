"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../constants/oss_consts.js");

var _app_tool = require("../../util/app_tool.js");

var _enum_consts = require("../../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Mine = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Mine, _BaseComponent);

  function Mine() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Mine);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Mine.__proto__ || Object.getPrototypeOf(Mine)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["me_bg1", "mUserInfo", "HEAD_A", "isPublish", "me_icon1", "me_icon2", "me_icon3", "arrow_right", "mine_huodong"], _this.config = {
      navigationBarTitleText: '塔罗测测',
      disableScroll: true
    }, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Mine, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Mine.prototype.__proto__ || Object.getPrototypeOf(Mine.prototype), "_constructor", this).call(this, props);
      this.state = {
        isPublish: false
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var mUserInfo = _index2.default.getStorageSync(_enum_consts.MuserInfo);
      this.setState({
        mUserInfo: mUserInfo,
        isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish)
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "settingClick",
    value: function settingClick(index, e) {
      switch (index) {
        case 0:

          break;
        case 1:
          _index2.default.navigateTo({
            url: './info/info'
          });
          break;
        case 2:
          _index2.default.navigateTo({
            url: './address/address'
          });
          break;
        case 3:
          _index2.default.navigateTo({
            url: './feedback/feedback'
          });
          break;
        case 4:

          break;
      }
    }
  }, {
    key: "paymentClick",
    value: function paymentClick(index, e) {
      switch (index) {
        case 0:
          _index2.default.navigateTo({
            url: './mine_answer/mine_answer'
          });
          break;
        case 1:

          (0, _app_tool.showToast)('敬请期待~');
          break;
        case 2:
          _index2.default.navigateTo({
            url: './coupon/coupon'
          });
          break;

      }
    }
  }, {
    key: "goodsButton",
    value: function goodsButton(e) {
      _index2.default.navigateTo({
        url: '../find/activitys/activitys'
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _state = this.__state,
          mUserInfo = _state.mUserInfo,
          isPublish = _state.isPublish;

      Object.assign(this.__state, {
        me_bg1: _oss_consts.me_bg1,
        mUserInfo: mUserInfo,
        HEAD_A: _oss_consts.HEAD_A,
        me_icon1: _oss_consts.me_icon1,
        me_icon2: _oss_consts.me_icon2,
        me_icon3: _oss_consts.me_icon3,
        arrow_right: _oss_consts.arrow_right,
        mine_huodong: _oss_consts.mine_huodong
      });
      return this.__state;
    }
  }]);

  return Mine;
}(_index.Component), _class.$$events = ["paymentClick", "settingClick", "goodsButton"], _class.$$componentPath = "pages/mine/mine", _temp2);
exports.default = Mine;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Mine, true));