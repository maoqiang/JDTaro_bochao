"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../../constants/oss_consts.js");

var _request_action = require("../../../action/request_action.js");

var _app_tool = require("../../../util/app_tool.js");

var _enum_consts = require("../../../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var pageIndex = 1;

var Answer = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Answer, _BaseComponent);

  function Answer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Answer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Answer.__proto__ || Object.getPrototypeOf(Answer)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["loopArray12", "$compid__12", "list", "HEAD_A", "isPublish", "noMore", "hasMore", "scrollTop", "noAut", "noData", "hasMoreText"], _this.config = {
      navigationBarTitleText: "\u6211\u7684\u95EE\u7B54"
    }, _this.getData = function () {
      var pIndex = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : pageIndex;
      var callback = arguments[1];

      var that = _this;
      (0, _request_action.getUserQuestions)({ 'PageIndex': pIndex }, function (data) {
        var list = data.items;

        callback({ list: list });
      });
    }, _this.refresh = function () {
      var that = _this;
      pageIndex = 0;
      _this.setState({
        refreshStatus: 1,
        hasMore: true,
        noMore: false
      });

      that.getData(0, function (res) {
        pageIndex++;
        if (res.list.length < 20) {
          that.loadMore();
        }
        that.setState(_extends({}, res, {
          noData: res.list.length < 1,
          refreshStatus: 2
        }));
      });
    }, _this.loadMore = function () {
      var that = _this;
      that.getData(pageIndex, function (res) {
        pageIndex++;
        if (res.list.length > 0) {
          that.setState({
            list: that.state.list.concat(res.list),
            hasMore: true,
            noMore: false
          });
        } else {
          that.setState({
            hasMore: false,
            noMore: true,
            hasMoreText: '没有更多数据了'
          });
        }
      });
    }, _this.handleScrollEnd = function (e) {
      _this.setState({
        scrollTop: e.detail.scrollTop
      });
    }, _this.customComponents = ["EPage"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Answer, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Answer.prototype.__proto__ || Object.getPrototypeOf(Answer.prototype), "_constructor", this).call(this, props);
      this.state = {
        noMore: false,
        hasMore: true,
        scrollTop: 0,
        list: [],
        noAut: false,
        noData: false,
        hasMoreText: '稍安勿躁,加载中...',
        isPublish: false
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish)
      });
      this.refresh();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "detailBtn",
    value: function detailBtn(item, e) {
      if (item.state == 1) {
        _index2.default.navigateTo({
          url: '/pages/select_poker/select_poker?type=answer&sourceName=test&questions=' + item.question + '&orderId=' + item.id
        });
      } else {
        _index2.default.navigateTo({
          url: '/pages/answer/detail/detail?id=' + item.id
        });
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__12"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__12 = _genCompid2[0],
          $compid__12 = _genCompid2[1];

      var _state = this.__state,
          hasMoreText = _state.hasMoreText,
          noMore = _state.noMore,
          hasMore = _state.hasMore,
          refreshStatus = _state.refreshStatus,
          scrollTop = _state.scrollTop,
          list = _state.list,
          isPublish = _state.isPublish,
          wen_opacity = _state.wen_opacity;

      var refresherConfig = {
        recoverTime: 300,
        refreshTime: 1000
      };
      var loopArray12 = list.map(function (item, index) {
        item = {
          $original: (0, _index.internal_get_original)(item)
        };
        var $loopState__temp2 = isPublish ? (0, _app_tool.formatNumber)(item.$original.priced || 0) : null;
        var $loopState__temp4 = (0, _app_tool.timeAgo)(item.$original.createTime);
        return {
          $loopState__temp2: $loopState__temp2,
          $loopState__temp4: $loopState__temp4,
          $original: item.$original
        };
      });
      _index.propsManager.set({
        "onRefresh": this.refresh,
        "onLoadMore": this.loadMore,
        "noMore": noMore,
        "hasMore": hasMore,
        "hasMoreText": hasMoreText,
        "refresherConfig": refresherConfig,
        "refreshStatus": refreshStatus,
        "scrollTop": scrollTop,
        "onScrollEnd": this.handleScrollEnd,
        "className": "EPage-view"
      }, $compid__12, $prevCompid__12);
      Object.assign(this.__state, {
        loopArray12: loopArray12,
        $compid__12: $compid__12,
        HEAD_A: _oss_consts.HEAD_A
      });
      return this.__state;
    }
  }]);

  return Answer;
}(_index.Component), _class.$$events = ["detailBtn"], _class.$$componentPath = "pages/mine/mine_answer/mine_answer", _temp2);
exports.default = Answer;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Answer, true));