"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../../constants/oss_consts.js");

var _enum_consts = require("../../../constants/enum_consts.js");

var _request_action = require("../../../action/request_action.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Mine = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Mine, _BaseComponent);

  function Mine() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Mine);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Mine.__proto__ || Object.getPrototypeOf(Mine)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "$compid__11", "mUserInfo", "me_bg1", "HEAD_A", "arrow_right", "sexList", "constellationList", "phoneText", "birthday", "constellation", "gender", "openAtFloatLayout", "show_btn", "count", "code_ts", "nickName", "isOpened", "openAtFLSex"], _this.config = {
      navigationBarTitleText: '个人信息',
      disableScroll: true
    }, _this.onChange = function (type, e) {
      if (type == 'sex') {
        _this.setState({
          gender: e.detail.value
        }, function () {
          _this.commitUser();
        });
      } else {
        _this.setState({
          constellation: e.detail.value
        }, function () {
          _this.commitUser();
        });
      }
    }, _this.onDateChange = function (e) {
      _this.setState({
        birthday: e.detail.value
      }, function () {
        _this.commitUser();
      });
    }, _this.customComponents = ["AtModal", "AtModalHeader", "AtModalContent", "AtModalAction"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Mine, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Mine.prototype.__proto__ || Object.getPrototypeOf(Mine.prototype), "_constructor", this).call(this, props);
      this.state = {
        birthday: undefined,
        constellation: '',
        constellationList: ['未设置', '白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '室女座', '天秤座', '天蝎座', '人马座', '摩羯座', '宝瓶座', '双鱼座'],
        sexList: ['未设置', '男', '女'],
        gender: '',
        openAtFloatLayout: false,
        show_btn: true,
        count: 60,
        code_ts: '获取验证码',
        nickName: undefined,
        isOpened: false,

        openAtFLSex: false,
        mUserInfo: {},
        phoneText: '请绑定手机号'
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var mUserInfo = _index2.default.getStorageSync(_enum_consts.MuserInfo);
      this.setState({
        mUserInfo: mUserInfo,
        phoneText: _index2.default.getStorageSync(_enum_consts.IsPublish) ? '完善信息可获优惠券一张' : '请绑定手机号'
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "settingClick",
    value: function settingClick(index, e) {
      switch (index) {
        case 0:
          this.setState({ isOpened: true });
          break;
        case 1:
          this.setState({ isOpened: false });
          break;
        case 2:
          //修改昵称
          this.commitUser();
          break;
        case 4:
          this.setState({
            openAtFloatLayout: true
          });
          break;

      }
    }
  }, {
    key: "phoneCommit",
    value: function phoneCommit() {}
  }, {
    key: "inputName",
    value: function inputName(e) {
      this.setState({
        nickName: e.currentTarget.value
      });
    }
  }, {
    key: "getPhoneNumber",
    value: function getPhoneNumber(e) {
      var that = this;
      var result = e.detail;
      if (!result.encryptedData) {
        return;
      }
      var map = {
        encryptedData: result.encryptedData,
        IV: result.iv
      };

      (0, _request_action.getPhone)(map, function (data) {
        that.setState({
          phone: data.phoneNumber
        }, function () {
          that.commitUser();
        });
      });
    }
  }, {
    key: "commitUser",
    value: function commitUser() {
      var that = this;
      var _state = this.state,
          nickName = _state.nickName,
          phone = _state.phone,
          gender = _state.gender,
          birthday = _state.birthday,
          constellation = _state.constellation;

      var map = {
        nickName: nickName,
        phone: phone,
        gender: gender,
        birthday: birthday,
        constellation: constellation
      };
      (0, _request_action.wxUserPut)(map, '?id=' + _index2.default.getStorageSync(_enum_consts.UserId), function (data) {
        _index2.default.setStorageSync(_enum_consts.MuserInfo, data);
        that.setState({
          isOpened: false,
          mUserInfo: data
        }, function () {
          var pages = _index2.default.getCurrentPages();
          var beforePage = pages[pages.length - 2];
          beforePage.setData({
            mUserInfo: data
          });
        });
      });
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__11"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__11 = _genCompid2[0],
          $compid__11 = _genCompid2[1];

      var _state2 = this.__state,
          constellationList = _state2.constellationList,
          sexList = _state2.sexList,
          isOpened = _state2.isOpened,
          mUserInfo = _state2.mUserInfo,
          phoneText = _state2.phoneText;

      var anonymousState__temp = mUserInfo.birthday ? mUserInfo.birthday.substring(0, 10) : "\u672A\u8BBE\u7F6E";
      _index.propsManager.set({
        "isOpened": isOpened
      }, $compid__11, $prevCompid__11);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        $compid__11: $compid__11,
        me_bg1: _oss_consts.me_bg1,
        HEAD_A: _oss_consts.HEAD_A,
        arrow_right: _oss_consts.arrow_right
      });
      return this.__state;
    }
  }]);

  return Mine;
}(_index.Component), _class.$$events = ["inputName", "settingClick", "onDateChange", "onChange", "getPhoneNumber"], _class.$$componentPath = "pages/mine/info/info", _temp2);
exports.default = Mine;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Mine, true));