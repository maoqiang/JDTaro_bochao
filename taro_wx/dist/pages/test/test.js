"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../constants/oss_consts.js");

var _enum_consts = require("../../constants/enum_consts.js");

var _request_action = require("../../action/request_action.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Test = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Test, _BaseComponent);

  function Test() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Test);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Test.__proto__ || Object.getPrototypeOf(Test)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["home_bg", "test_title_bg", "input_bg", "list", "HEAD_A", "isPublish"], _this.config = {
      navigationBarTitleText: '塔罗测测'
    }, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Test, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Test.prototype.__proto__ || Object.getPrototypeOf(Test.prototype), "_constructor", this).call(this, props);
      this.state = {
        list: [],
        isPublish: false

      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var that = this;
      this.setState({
        isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish)
      });
      (0, _request_action.getHots)({}, function (res) {
        var list = [];
        if (res && res.length > 0) {
          for (var i = 0; i < res.length; i++) {
            var map = that.judgmentImg(res[i].questionType);
            list.push(_extends({}, res[i], map));
          }
        }
        that.setState({
          list: list
        });
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "startTest",
    value: function startTest() {
      var item = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var questions = item.question || '';
      var id = item.id || '';

      if (this.state.isPublish) {
        _index2.default.navigateTo({
          url: '../select_poker/select_poker?sourceName=test&questions=' + questions + '&id=' + id
        });
      } else {
        _index2.default.navigateTo({
          url: '../select_poker/select_poker?sourceName=find'
        });
      }
    }
  }, {
    key: "judgmentImg",
    value: function judgmentImg(type) {
      //debugger
      var test_item = '';
      var border = '';
      var icon = '';
      var btn = '';

      switch (type) {
        case 1:
          test_item = _oss_consts.test_item3;
          border = _oss_consts.border3;
          icon = _oss_consts.icon3;
          btn = _oss_consts.btn3;
          break;
        case 2:
          test_item = _oss_consts.test_item1;
          border = _oss_consts.border1;
          icon = _oss_consts.icon1;
          btn = _oss_consts.btn1;
          break;
        case 3:
          test_item = _oss_consts.test_item4;
          border = _oss_consts.border4;
          icon = _oss_consts.icon4;
          btn = _oss_consts.btn4;
          break;
        default:
          test_item = _oss_consts.test_item5;
          border = _oss_consts.border5;
          icon = _oss_consts.icon5;
          btn = _oss_consts.btn5;
          break;
      }

      return {
        test_item: test_item,
        border: border,
        icon: icon,
        btn: btn
      };
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var list = this.__state.list;


      Object.assign(this.__state, {
        home_bg: _oss_consts.home_bg,
        test_title_bg: _oss_consts.test_title_bg,
        input_bg: _oss_consts.input_bg,
        HEAD_A: _oss_consts.HEAD_A
      });
      return this.__state;
    }
  }]);

  return Test;
}(_index.Component), _class.$$events = ["startTest"], _class.$$componentPath = "pages/test/test", _temp2);
exports.default = Test;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Test, true));