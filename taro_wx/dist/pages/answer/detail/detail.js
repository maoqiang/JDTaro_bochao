"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _oss_consts = require("../../../constants/oss_consts.js");

var _request_action = require("../../../action/request_action.js");

var _app_tool = require("../../../util/app_tool.js");

var _enum_consts = require("../../../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var inner = null;
var playStatus = false;

var Detail = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Detail, _BaseComponent);

  function Detail() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Detail);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Detail.__proto__ || Object.getPrototypeOf(Detail)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "anonymousState__temp4", "anonymousState__temp5", "anonymousState__temp6", "data", "HEAD_A", "isPublish", "firstCard", "secondCard", "thirdCard", "display", "animationList", "isSelf"], _this.config = {
      navigationBarTitleText: '问答详情'
    }, _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Detail, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(Detail.prototype.__proto__ || Object.getPrototypeOf(Detail.prototype), "_constructor", this).call(this, props);
      this.state = {
        display: 'none',
        animationList: [],
        isPublish: false
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.$router.params.type == 'pay') {}
    }
  }, {
    key: "subTap",
    value: function subTap() {
      var that = this;
      _index2.default.requestSubscribeMessage({
        tmplIds: ['__u1wMBCDxL4vXhVViwe2gnrDvqMWeDU7p8zNnApf8U'],
        success: function success(res) {},
        fail: function fail(res) {}
      });
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var _this2 = this;

      var that = this;
      inner = _index2.default.createInnerAudioContext();

      inner.onEnded(function () {
        _this2.playStatus = false;
        _this2.setState({
          display: 'none'
        });
      });

      var animationList = [];
      for (var i = 0; i < 8; i++) {
        animationList.push({
          animationP: i
        });
      }

      (0, _request_action.getQuestions)({ id: this.$router.params.id }, function (data) {
        that.setState({
          animationList: animationList,
          data: data,
          isPublish: _index2.default.getStorageSync(_enum_consts.IsPublish),
          answerUrl: data.answerUrl,
          isSelf: data.isSelf
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // inner.destroy();
    }
  }, {
    key: "componentDidShow",
    value: function componentDidShow() {}
  }, {
    key: "componentDidHide",
    value: function componentDidHide() {}
  }, {
    key: "play",
    value: function play() {
      if (!this.state.isSelf) {
        (0, _app_tool.showToast)('只可以阅读自己的测测问答哦！');
        return;
      }

      if (this.playStatus) {
        this.playStatus = false;
        inner.stop();
        this.setState({
          display: 'none'
        });
      } else {
        this.playStatus = true;
        inner.src = this.state.answerUrl;
        inner.obeyMuteSwitch = false;
        inner.play();
        this.setState({
          display: 'block'
        });
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _state = this.__state,
          display = _state.display,
          animationList = _state.animationList,
          _state$data = _state.data,
          data = _state$data === undefined ? {} : _state$data,
          isPublish = _state.isPublish,
          isSelf = _state.isSelf;

      var questionCards = data.questionCards || {};

      var firstCard = questionCards.firstCard || {};
      var secondCard = questionCards.secondCard || {};
      var thirdCard = questionCards.thirdCard || {};
      var anonymousState__temp = isPublish ? (0, _app_tool.formatNumber)(data.payable || 0) : null;
      var anonymousState__temp2 = (0, _app_tool.timeAgo)(data.createTime) || '';
      var anonymousState__temp3 = firstCard.picUrl ? firstCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(firstCard.isInversion)) : null;
      var anonymousState__temp4 = secondCard.picUrl ? secondCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(secondCard.isInversion)) : null;
      var anonymousState__temp5 = thirdCard.picUrl ? thirdCard.picUrl.concat(_oss_consts.resize).concat((0, _oss_consts.rotateFun)(thirdCard.isInversion)) : null;
      var anonymousState__temp6 = data.state == 3 ? (0, _app_tool.timeAgo)(data.answerTime) || '' : null;
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        anonymousState__temp4: anonymousState__temp4,
        anonymousState__temp5: anonymousState__temp5,
        anonymousState__temp6: anonymousState__temp6,
        data: data,
        HEAD_A: _oss_consts.HEAD_A,
        firstCard: firstCard,
        secondCard: secondCard,
        thirdCard: thirdCard,
        isSelf: isSelf
      });
      return this.__state;
    }
  }]);

  return Detail;
}(_index.Component), _class.$$events = ["play", "subTap"], _class.$$componentPath = "pages/answer/detail/detail", _temp2);
exports.default = Detail;

Component(require('../../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Detail, true));