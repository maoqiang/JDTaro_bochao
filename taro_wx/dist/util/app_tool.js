"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timeAgo = exports.formatNumber = exports.interceptArray = exports.getUser = exports.wxLogin = exports.getUserInfoTool = exports.authorized = exports.fmoney = exports.url2param = exports.hidePhoneNumber = exports.hideToast = exports.showToast = undefined;

var _index = require("../npm/@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../npm/query-string/index.js");

var _index4 = _interopRequireDefault(_index3);

var _request_action = require("../action/request_action.js");

var _enum_consts = require("../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _this = undefined;

var showToast = exports.showToast = function showToast() {
  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var icon = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';

  _index2.default.showToast({
    title: title,
    icon: icon,
    duration: 2000,
    mask: false
  });
};

var hideToast = exports.hideToast = function hideToast() {
  _index2.default.hideToast();
};

/**
 * 隐藏手机号中间四位
 * @param cellValue
 * @returns {*}
 */
var hidePhoneNumber = exports.hidePhoneNumber = function hidePhoneNumber(cellValue) {
  if (Number(cellValue) && String(cellValue).length === 11) {

    var mobile = String(cellValue);

    var reg = /^(\d{3})\d{4}(\d{4})$/;

    return mobile.replace(reg, '$1****$2');
  } else {

    return cellValue;
  }
};

/**
 * map转换为url
 * @param params
 */
var url2param = exports.url2param = function url2param(params) {
  var paramsStr = _index4.default.stringify(params);
  return '?' + paramsStr;
};

/*
  * 参数说明：
  * s：要格式化的数字
  * n：保留几位小数
  * */
var fmoney = exports.fmoney = function fmoney(s, n) {
  if (!s) {
    return 0;
  }
  n = n > 0 && n <= 20 ? n : 2;
  s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
  var l = s.split(".")[0].split("").reverse(),
      r = s.split(".")[1];
  var t = "";
  for (var i = 0; i < l.length; i++) {
    t += l[i] + ((i + 1) % 3 == 0 && i + 1 != l.length ? "," : "");
  }
  return t.split("").reverse().join("") + "." + r;
};

var authorized = exports.authorized = function authorized() {
  var that = _this;
  _index2.default.getSetting().then(function (res) {
    if (res.authSetting["scope.userInfo"]) {
      return true;
    } else {
      throw new Error('没有授权');
    }
  }).then(function (res) {
    return _index2.default.getUserInfo();
  }).then(function (data) {
    _index2.default.login({
      success: function success(res) {
        var map = {
          code: res.code, //--授权码
          encryptedData: data.encryptedData,
          iv: data.iv,
          sessionId: '',
          userInfo: data.userInfo //微信用户信息
        };
        wx_authorize(map, function (data2) {
          data.sessionKey = data2.data.sessionKey;
          data.openId = data2.data.openId;
          data.token = data2.data.token;

          _index2.default.setStorageSync('wxAuthorized', data);
          if (!_index2.default.getStorageSync('token')) {
            _index2.default.setStorageSync('token', data.token);
          }
        });
      }
    });
  }).catch(function (err) {
    console.log(err);
  });
};

var getUserInfoTool = exports.getUserInfoTool = function getUserInfoTool(data, callback) {
  var userInfo = data.userInfo;
  if (data.userInfo) {
    var map = {
      encryptedData: data.encryptedData,
      iv: data.iv,
      signature: data.signature,

      headImg: userInfo.avatarUrl,
      city: userInfo.city,
      country: userInfo.country,
      gender: userInfo.gender,
      nickName: userInfo.nickName,
      province: userInfo.province,
      isWxAuthed: true
    };
    _index2.default.setStorageSync(_enum_consts.WxWarrant, data);
    (0, _request_action.wxUserPut)(map, '?id=' + _index2.default.getStorageSync(_enum_consts.UserId), function (data) {
      _index2.default.setStorageSync(_enum_consts.MuserInfo, data);
      callback(data);
    });
  } else {
    //拒绝
    showToast('拒绝授权,将获取不到相关数据~');
  }
};

var wxLogin = exports.wxLogin = function wxLogin() {
  var map = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callback = arguments[1];

  _index2.default.login({
    success: function success() {
      var res = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      (0, _request_action.WxOpen)({ code: res.code, userId: map.userId, version: map.version }, function (res) {
        _index2.default.setStorageSync(_enum_consts.Token, res.token);
        _index2.default.setStorageSync(_enum_consts.UserId, res.userId);
        _index2.default.setStorageSync(_enum_consts.MuserInfo, res.userInfo);
        _index2.default.setStorageSync(_enum_consts.IsPublish, res.isPublish);
        _index2.default.setStorageSync(_enum_consts.MLoginInfo, res);
        callback & callback(res);
      });
    }
  });
};

var getUser = exports.getUser = function getUser() {
  (0, _request_action.wxUserGet)({}, '?id=' + _index2.default.getStorageSync(_enum_consts.UserId), function (data) {
    _index2.default.setStorageSync(_enum_consts.MuserInfo, data);
  });
};

/**
 * 将数组array分成长度为subGroupLength的小数组并返回新数组
 * @param array 原始数组
 * @param subGroupLength 每个数组长度
 * @returns {Array}
 */
var interceptArray = exports.interceptArray = function interceptArray(array, subGroupLength) {
  if (array && array.length >= 1) {
    var index = 0;
    var newArray = [];
    while (index < array.length) {
      newArray.push(array.slice(index, index += subGroupLength));
    }
    return newArray;
  }
  return array;
};

/**
 * 格式化数字成0,000.00
 * @param value
 * @returns {string}
 */
var formatNumber = exports.formatNumber = function formatNumber(value) {
  value = (Number(value) / 100).toFixed(2);
  //value=String(value);
  if (isNaN(value)) {
    value = "0.00";
  }
  var result = "";
  var idx = value.indexOf('-');
  if (idx !== -1) {
    value = value.substring(idx + 1);
  }
  ;
  var valueParts = value.split(".");
  var mostSignificationDigit = valueParts[0].length - 1; // 最高有效数字位，默认为个位
  var intervalOfDigit = 0; // 逗号之间的位数
  var digit, countOfSignificationDigit;
  for (var i = valueParts[0].length - 1; i >= 0; i--) {
    digit = valueParts[0][i];
    result = digit + result;
    if (digit != "0") {
      mostSignificationDigit = i;
    }
    if (3 == ++intervalOfDigit) {
      result = "," + result;
      intervalOfDigit = 0;
    }
  }
  if (mostSignificationDigit == -1) {
    result = "0";
  } else {
    countOfSignificationDigit = valueParts[0].length - mostSignificationDigit;
    if (countOfSignificationDigit > 3) {
      result = result.substring(result.length - (countOfSignificationDigit % 3 == 0 ? countOfSignificationDigit / 3 - 1 : countOfSignificationDigit / 3) - countOfSignificationDigit);
    } else {
      result = result.substring(result.length - countOfSignificationDigit);
    }
  }
  if (valueParts.length == 2) {
    result += ".";
    var temp = 2 - valueParts[1].length; // 是否需要补0
    for (var i = 0; i < temp; i++) {
      valueParts[1] += "0";
    }
    result += valueParts[1].substring(0, 2);
  } else {
    result += ".00";
  }
  if (idx !== -1) {
    return '-' + result;
  }
  return result;
};

/**
 * 计算发表时间, xx分钟以前
 * 调用,参数为时间戳
 */
var timeAgo = exports.timeAgo = function timeAgo(str) {
  if (!str) {
    return '';
  }
  var o = new Date(str).getTime();
  var n = new Date().getTime();
  var f = n - o;
  var bs = f >= 0 ? '前' : '后'; //判断时间点是在当前时间的 之前 还是 之后
  f = Math.abs(f);
  if (f < 6e4) {
    return '刚刚';
  } //小于60秒,刚刚
  if (f < 36e5) {
    return parseInt(f / 6e4) + '分钟' + bs;
  } //小于1小时,按分钟
  if (f < 864e5) {
    return parseInt(f / 36e5) + '小时' + bs;
  } //小于1天按小时
  if (f < 2592e6) {
    return str.substring(5, str.length - 3);
    // return parseInt(f / 864e5) + '天' + bs
  } //小于1个月(30天),按天数
  if (f < 31536e6) {
    return str.substring(5, str.length - 3);
    // return parseInt(f / 2592e6) + '个月' + bs
  } //小于1年(365天),按月数
  return str.substring(5, str.length - 3);
  // return parseInt(f / 31536e6) + '年' + bs; //大于365天,按年算
};

exports.default = {
  showToast: showToast,
  hideToast: hideToast,
  hidePhoneNumber: hidePhoneNumber,
  fmoney: fmoney,
  authorized: authorized,
  getUserInfoTool: getUserInfoTool,
  wxLogin: wxLogin,
  getUser: getUser,
  interceptArray: interceptArray,
  formatNumber: formatNumber,
  timeAgo: timeAgo
};