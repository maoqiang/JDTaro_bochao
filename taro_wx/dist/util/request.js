"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.postFormApi = exports.deleteApi = exports.putApi = exports.postApi = exports.getApi = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _index = require("../npm/query-string/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../npm/@tarojs/taro-weapp/index.js");

var _index4 = _interopRequireDefault(_index3);

var _app_tool = require("./app_tool.js");

var _enum_consts = require("../constants/enum_consts.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var METHOD_GET = 'GET';
var METHOD_DELETE = 'DELETE';
var METHOD_POST = 'POST';
var METHOD_PUT = 'PUT';
var TRY_AGAIN_COUNT = 0;

var SHOW_LOADING_TAG = true;
var requestApi = function requestApi(cfg) {
  var tryAgainCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : TRY_AGAIN_COUNT;
  var showLoading = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

  if (showLoading) {
    showLoadingTime();
  }
  var header = Object.assign({}, cfg.header, {
    token: _index4.default.getStorageSync(_enum_consts.Token)
  });
  var config = _extends({}, cfg, {
    header: header
  });
  return _index4.default.request(config).then(function (res) {
    if (res.statusCode === 200) {
      SHOW_LOADING_TAG = false;
      _index4.default.hideLoading();
      return res.data;
    } else {
      if (tryAgainCount) {
        return requestApi(cfg, tryAgainCount - 1);
      } else {
        (0, _app_tool.showToast)('数据异常,请刷新页面');
      }
      _index4.default.hideLoading();
    }
  }).catch(function (e) {
    if (tryAgainCount) {
      return requestApi(cfg, tryAgainCount - 1);
    } else {
      (0, _app_tool.showToast)('数据异常,请刷新页面');
    }
    _index4.default.hideLoading();
  });
};

var getApi = exports.getApi = function getApi(url, params) {
  var thisParams = _extends({}, params);
  // if (!thisParams._timestamp) {
  //     thisParams._timestamp = Date.now()
  // }
  var newParams = publicParam(thisParams);
  var requestUrl = genUrl(url, newParams);
  var config = {
    method: METHOD_GET,
    url: requestUrl
  };
  return requestApi(config, TRY_AGAIN_COUNT, !params.current);
};

var genUrl = function genUrl(url, params) {
  var paramsStr = _index2.default.stringify(params);
  var splitChar = url.indexOf('?') === -1 ? '?' : '&';
  return url + splitChar + paramsStr;
};

var postApi = exports.postApi = function postApi(url, thisParams) {
  var newParams = publicParam(thisParams);
  var config = {
    method: METHOD_POST,
    header: {
      'Content-Type': 'application/json'
    },
    data: newParams,
    url: url
  };
  return requestApi(config, TRY_AGAIN_COUNT);
};

var putApi = exports.putApi = function putApi(url, thisParams) {
  var newParams = publicParam(thisParams);
  var config = {
    method: METHOD_PUT,
    header: {
      'Content-Type': 'application/json'
    },
    data: newParams,
    url: url
  };
  return requestApi(config, TRY_AGAIN_COUNT);
};

var deleteApi = exports.deleteApi = function deleteApi(url, params) {
  var thisParams = _extends({}, params);
  var newParams = publicParam(thisParams);
  var requestUrl = genUrl(url, newParams);
  var config = {
    method: METHOD_DELETE,
    url: requestUrl
  };
  return requestApi(config, TRY_AGAIN_COUNT, !params.current);
};

var postFormApi = exports.postFormApi = function postFormApi(url, thisParams) {
  var newParams = publicParam(thisParams);
  var config = {
    method: METHOD_POST,
    header: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: encodeParam(newParams),
    url: url
  };
  return requestApi(config, TRY_AGAIN_COUNT);
};

var encodeParam = function encodeParam(params) {
  var paramArr = [];
  Object.keys(params).forEach(function (key) {
    if (typeof params[key] !== 'undefined') {
      paramArr.push(key + '=' + encodeURIComponent(params[key]));
    }
  });
  return paramArr.join('&');
};

var publicParam = function publicParam(thisParams) {
  // if (Taro.getStorageSync(Token)) {
  //     let token = Taro.getStorageSync(Token);
  //     let userId = Taro.getStorageSync(UserId);
  //     let newParams = {...thisParams};
  //     newParams.userId = userId
  //     return newParams
  // }
  return thisParams;
};

var showLoadingTime = function showLoadingTime() {
  SHOW_LOADING_TAG = true;
  setTimeout(function () {
    if (SHOW_LOADING_TAG) {
      _index4.default.showLoading({
        title: '加载中...',
        mask: true,
        success: function success() {
          setTimeout(function () {
            _index4.default.hideLoading();
          }, 30000);
        }
      });
    }
  }, 300);
};