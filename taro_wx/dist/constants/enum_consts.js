'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var WxWarrant = exports.WxWarrant = 'wxWarrant';
var Token = exports.Token = 'token';
var UserId = exports.UserId = 'userId';
var MuserInfo = exports.MuserInfo = 'mUserInfo';
var MLoginInfo = exports.MLoginInfo = 'mLoginInfo';
var IsPublish = exports.IsPublish = 'isPublish';