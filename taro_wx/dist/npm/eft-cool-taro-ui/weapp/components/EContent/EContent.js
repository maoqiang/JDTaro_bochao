"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../../@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../../../prop-types/index.js");

var _index4 = _interopRequireDefault(_index3);

var _index5 = require("../../utils/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var windowHeight = _index2.default.getSystemInfoSync().windowHeight;

/**
 * 监听 EContent 的事件
 * 针对刷新、内容高度、加载状态的控制
 * ESetHeader、ESetFooter、ERefreshStart、ERefreshEnd、
 */

var EContent = (_temp2 = _class = function (_BaseComponent) {
  _inherits(EContent, _BaseComponent);

  function EContent() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, EContent);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = EContent.__proto__ || Object.getPrototypeOf(EContent)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "$compid__15", "noMore", "hasMore", "noMoreText", "hasMoreText", "refresherConfig", "loadMoreThreshold", "scrollTop", "focus", "textStatus", "dragStyle", "downDragStyle", "dragState", "dragComplete", "scrollY", "footerHeight", "headerHeight", "isRefreshing", "__fn_on", "loading", "refreshStatus", "children"], _this.onScrollToLower = function () {
      (0, _index5.throttle)({
        method: function method() {
          console.log('滑动到底部');
          !_this.props.loading && _this.props.hasMore && _this.props.onScrollToLower && _this.props.onScrollToLower();
        },
        ahead: true
      });
    }, _this.onScrollToUpper = function () {//滚动到顶部事件
      // console.log('滚动到顶部事件')
    }, _this.onScroll = function (e) {
      var scrollTop = e.detail.scrollTop;
      var _this$props = _this.props,
          onScrollUp = _this$props.onScrollUp,
          onScrollDown = _this$props.onScrollDown,
          onScroll = _this$props.onScroll,
          onScrollEnd = _this$props.onScrollEnd;

      _this.isTop = scrollTop <= 60; // 滚动到了顶部
      // deltaY在微信小程序适用
      if (scrollTop > 200) {
        onScrollUp && _this.props.onScrollUp();
      } else {
        onScrollDown && _this.props.onScrollDown();
      }

      (0, _index5.throttle)({
        method: function method() {
          onScrollEnd && _this.props.onScrollEnd(e);
        },
        delay: 100,
        type: 'scrollEnd'
      });

      onScroll && _this.props.onScroll(e);

      // throttle({
      //   method: () => {
      //     console.log('开始滚动')
      //     Taro.eventCenter.trigger('scrollStart', {})
      //   },
      //   ahead: true,
      //   delay: 1000
      // })
      // throttle({
      //   method: () => {
      //     console.log('滚动结束')
      //     Taro.eventCenter.trigger('scrollEnd', {})
      //   },
      //   delay: 500
      // })
    }, _this.header = function (rect) {
      // 优化 Content 渲染频率
      (0, _index5.throttle)({
        method: function method() {
          if (_this.cacheHeader !== rect.height) {
            // console.log('计算header')
            windowHeight = _index2.default.getSystemInfoSync().windowHeight;
            _this.cacheHeader = rect.height;
            _this.setState({
              headerHeight: rect.height
            });
          }
        },
        type: 'header'
      });
    }, _this.footer = function (rect) {
      // 优化 Content 渲染频率
      (0, _index5.throttle)({
        method: function method() {
          if (_this.cacheFooter !== rect.height) {
            // console.log('计算footer')
            windowHeight = _index2.default.getSystemInfoSync().windowHeight;
            _this.cacheFooter = rect.height;
            _this.setState({
              footerHeight: rect.height
            });
          }
        },
        type: 'footer'
      });
    }, _this.focus = function () {
      _this.setState({
        focus: true
      });
    }, _this.blur = function () {
      _this.setState({
        focus: false
      });
    }, _this.touchStart = function (e) {
      _this.start_p = e.touches[0];
    }, _this.touchmove = function (e) {
      if (_this.refresherConfig.disabled) {
        return;
      }
      if (_this.state.isRefreshing) {
        e.preventDefault(); //阻止默认的处理方式(阻止下拉滑动的效果)
        e.stopPropagation();
        return;
      }
      if (!_this.isTop) {
        _this.start_p = e.touches[0];
        return;
      }
      var start_x = _this.start_p.clientX;
      var start_y = _this.start_p.clientY;

      var move_p = e.touches[0]; // 移动时的位置
      var move_x = move_p.clientX;
      var move_y = move_p.clientY;
      var deviationX = 0.30; // 左右偏移量(超过这个偏移量不执行下拉操作)

      //得到偏移数值
      var dev = Math.abs(move_x - start_x) / Math.abs(move_y - start_y);
      if (dev < deviationX) {
        // 当偏移数值大于设置的偏移数值时则不执行操作
        var pY = move_y - start_y;
        pY = Math.pow(10, Math.log10(Math.abs(pY)) / 1.3); // 拖动倍率
        var dragComplete = parseInt(pY / _this.refresherConfig.threshold * 100);
        if (dragComplete > 100) {
          dragComplete = 100;
        }
        _this.setState({
          dragComplete: dragComplete
        });
        if (move_y - start_y > 0) {
          // 下拉操作
          if (_this.needPrevent) {
            e.preventDefault(); //阻止默认的处理方式(阻止下拉滑动的效果)
            e.stopPropagation();
          }
          if (pY >= _this.refresherConfig.threshold) {
            if (_this.state.dragState === 0) {
              (0, _index5.vibrateShort)();
              _this.setState({ dragState: 1, textStatus: 1 });
            }
          } else {
            _this.setState({ dragState: 0, textStatus: 0 });
          }
          if (pY >= _this.refresherConfig.maxHeight) {
            pY = _this.refresherConfig.maxHeight;
          }
          _this.setState({
            dragStyle: {
              top: pY + 'px'
            },
            downDragStyle: {
              height: pY + 'px'
            },
            scrollY: false //拖动的时候禁用
          });
        }
      }
    }, _this.touchEnd = function (e) {
      if (_this.isTop) {
        _this.needPrevent = true;
      } else {
        _this.needPrevent = false;
      }
      if (_this.state.dragState === 1) {
        // 触发下拉刷新
        _this.showRefresh();
        !_this.props.loading && _this.props.onRefresh && _this.props.onRefresh();
      }
      _this.recover();
    }, _this.doRecover = function (force) {
      if (_this.props.refreshStatus !== 1 || force) {
        _this.setState({
          dragState: 0,
          dragStyle: {
            top: "0px",
            transition: "all " + _this.refresherConfig.recoverTime + "ms"
          },
          downDragStyle: {
            height: "0px",
            transition: "all " + _this.refresherConfig.recoverTime + "ms"
          },
          scrollY: true,
          isRefreshing: false,
          textStatus: 0
        });
      }
    }, _this.showRefresh = function () {
      (0, _index5.throttle)({
        method: function method() {
          // console.log('显示刷新')
          _this.startTime = new Date();
          var time = 0.2;
          _this.setState({
            dragStyle: {
              top: _this.refresherConfig.height + 'px',
              transition: "all " + time + "s"
            },
            downDragStyle: {
              height: _this.refresherConfig.height + 'px',
              transition: "all " + time + "s"
            },
            dragComplete: 100,
            isRefreshing: true,
            textStatus: 2
          });
        },
        ahead: true,
        type: 'showRefresh'
      });
    }, _this.hideRefresh = function () {
      (0, _index5.throttle)({
        method: function method() {
          // console.log('隐藏刷新')
          _this.recover();
        },
        ahead: true,
        type: 'hideRefresh'
      });
    }, _this.customComponents = ["ERefresher"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(EContent, [{
    key: "_constructor",
    value: function _constructor() {
      _get(EContent.prototype.__proto__ || Object.getPrototypeOf(EContent.prototype), "_constructor", this).apply(this, arguments);
      this.state = {
        dragStyle: { //下拉框的样式
          top: "0px"
        },
        downDragStyle: { //下拉图标的样式
          height: "0px"
        },
        textStatus: 0,
        dragState: 0, //刷新状态 0不做操作 1刷新
        dragComplete: 0, // 拖拽状态的完成度
        scrollY: true,
        footerHeight: 0,
        headerHeight: 0,
        isRefreshing: false,
        focus: false
      };
      this.isTop = true;
      this.needPrevent = false;
      this.refresherConfig = _extends({
        recoverTime: 300, // 回弹动画的时间时间 ms
        refreshTime: 500, // 刷新动画至少显示的时间 ms （用来展示刷新动画）
        threshold: 70, // 刷新的阈值 px  拉动长度（低于这个值的时候不执行）
        maxHeight: 200, // 可拉动的最大高度 px
        height: 60, // 刷新动画占的高度 px
        showText: true, // 显示文字
        refreshText: ['下拉刷新', '释放刷新', '加载中'] }, this.props.refresherConfig);
      this.scrollTop = this.props.scrollTop || 0;
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      console.log('加载Content——————————————');
      _index2.default.eventCenter.on('ESetHeader', this.header);
      _index2.default.eventCenter.on('ESetFooter', this.footer);
      _index2.default.eventCenter.on('ERefreshStart', this.showRefresh);
      _index2.default.eventCenter.on('ERefreshEnd', this.hideRefresh);
      _index2.default.eventCenter.on('focus', this.focus);
      _index2.default.eventCenter.on('blur', this.blur);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      console.log('卸载Content——————————————');
      _index2.default.eventCenter.off('ESetHeader', this.header);
      _index2.default.eventCenter.off('ESetFooter', this.footer);
      _index2.default.eventCenter.off('ERefreshStart', this.showRefresh);
      _index2.default.eventCenter.off('ERefreshEnd', this.hideRefresh);
      _index2.default.eventCenter.off('focus', this.focus);
      _index2.default.eventCenter.off('blur', this.blur);
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.scrollTop != nextProps.scrollTop) {
        this.scrollTop = nextProps.scrollTop;
      }
      if (nextProps.refreshStatus === 2) {
        this.doRecover(true);
      }
      if (nextProps.refreshStatus === 1) {
        this.showRefresh();
      }
    }

    /**
    * 触发加载更多
    *
    * @memberof Content
    */

  }, {
    key: "recover",
    value: function recover() {
      var _this2 = this;

      //还原初始设置
      var refreshLimit = this.refresherConfig.refreshTime - (new Date() - this.startTime);
      if (refreshLimit <= 0) {
        this.doRecover();
      } else {
        setTimeout(function () {
          _this2.doRecover();
        }, refreshLimit);
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      var refresherConfig = this.refresherConfig,
          scrollTop = this.scrollTop;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__15"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__15 = _genCompid2[0],
          $compid__15 = _genCompid2[1];

      var _state = this.__state,
          dragStyle = _state.dragStyle,
          downDragStyle = _state.downDragStyle,
          dragComplete = _state.dragComplete,
          textStatus = _state.textStatus,
          footerHeight = _state.footerHeight,
          headerHeight = _state.headerHeight,
          isRefreshing = _state.isRefreshing,
          focus = _state.focus;
      var _props = this.__props,
          loading = _props.loading,
          hasMore = _props.hasMore,
          noMore = _props.noMore,
          onScrollToLower = _props.onScrollToLower,
          children = _props.children,
          loadMoreThreshold = _props.loadMoreThreshold,
          hasMoreText = _props.hasMoreText,
          noMoreText = _props.noMoreText;


      var tabBarBottom = 0;

      var anonymousState__temp = (0, _index.internal_inline_style)({ height: windowHeight - footerHeight - headerHeight - tabBarBottom + "px" });
      var anonymousState__temp2 = (0, _index.internal_inline_style)(downDragStyle);
      var anonymousState__temp3 = (0, _index.internal_inline_style)(dragStyle);
      _index.propsManager.set({
        "complete": dragComplete,
        "isRefreshing": isRefreshing
      }, $compid__15, $prevCompid__15);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3,
        $compid__15: $compid__15,
        noMore: noMore,
        hasMore: hasMore,
        noMoreText: noMoreText,
        hasMoreText: hasMoreText,
        refresherConfig: refresherConfig,
        loadMoreThreshold: loadMoreThreshold,
        scrollTop: scrollTop
      });
      return this.__state;
    }
  }]);

  return EContent;
}(_index.Component), _class.$$events = ["touchmove", "touchEnd", "touchStart", "onScrollToUpper", "onScrollToLower", "onScroll"], _class.options = {
  addGlobalClass: true
}, _class.$$componentPath = "E:/jason_mao/svn_repoistory/bochao_JDTaro/tarot_wx/node_modules/eft-cool-taro-ui/weapp/components/EContent/EContent", _temp2);


EContent.propTypes = {
  onRefresh: _index4.default.func,
  onScrollToLower: _index4.default.func
};
exports.default = EContent;

Component(require('../../../../@tarojs/taro-weapp/index.js').default.createComponent(EContent));