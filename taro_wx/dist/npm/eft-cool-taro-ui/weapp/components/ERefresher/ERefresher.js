"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../../@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Refresher = (_temp2 = _class = function (_BaseComponent) {
  _inherits(Refresher, _BaseComponent);

  function Refresher() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Refresher);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Refresher.__proto__ || Object.getPrototypeOf(Refresher)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "anonymousState__temp2", "anonymousState__temp3", "baseSize", "complete", "isRefreshing"], _this.customComponents = [], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Refresher, [{
    key: "_constructor",
    value: function _constructor() {
      _get(Refresher.prototype.__proto__ || Object.getPrototypeOf(Refresher.prototype), "_constructor", this).apply(this, arguments);
      this.state = {
        baseSize: 0.4
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var baseSize = this.__state.baseSize;

      var start = 70;
      var centerSize = {
        transform: this.__props.complete > start ? "scale(" + ((this.__props.complete - start) * 0.045 + .25) + ")" : "scale(0.25)",
        "-webkit-transform": this.__props.complete > start ? "scale(" + ((this.__props.complete - start) * 0.045 + .25) + ")" : "scale(0.25)"
      };
      var anonymousState__temp = (0, _index.internal_inline_style)({
        transform: "translate3d(" + this.__props.complete * baseSize + "px,0,0) scale(0.25)",
        "-webkit-transform": "translate3d(" + this.__props.complete * baseSize + "px,0,0) scale(0.25)" });
      var anonymousState__temp2 = (0, _index.internal_inline_style)(centerSize);
      var anonymousState__temp3 = (0, _index.internal_inline_style)({
        transform: "translate3d(" + -this.__props.complete * baseSize + "px,0,0) scale(0.25)",
        "-webkit-transform": "translate3d(" + -this.__props.complete * baseSize + "px,0,0) scale(0.25)" });
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        anonymousState__temp2: anonymousState__temp2,
        anonymousState__temp3: anonymousState__temp3
      });
      return this.__state;
    }
  }]);

  return Refresher;
}(_index.Component), _class.$$events = [], _class.options = {
  addGlobalClass: true
}, _class.$$componentPath = "E:/jason_mao/svn_repoistory/bochao_JDTaro/tarot_wx/node_modules/eft-cool-taro-ui/weapp/components/ERefresher/ERefresher", _temp2);
exports.default = Refresher;

Component(require('../../../../@tarojs/taro-weapp/index.js').default.createComponent(Refresher));