"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _class, _temp2;

var _index = require("../../../../@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../../../prop-types/index.js");

var _index4 = _interopRequireDefault(_index3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EPage = (_temp2 = _class = function (_BaseComponent) {
  _inherits(EPage, _BaseComponent);

  function EPage() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, EPage);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = EPage.__proto__ || Object.getPrototypeOf(EPage)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["anonymousState__temp", "$compid__14", "className", "onRefresh", "onLoadMore", "onScroll", "onScrollEnd", "hasMore", "noMore", "loadMoreThreshold", "noMoreText", "hasMoreText", "refreshStatus", "scrollTop", "refresherConfig", "renderHeader", "children", "renderFooter"], _this.customComponents = ["EHeader", "EContent", "EFooter"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(EPage, [{
    key: "_constructor",
    value: function _constructor(props) {
      _get(EPage.prototype.__proto__ || Object.getPrototypeOf(EPage.prototype), "_constructor", this).call(this, props);

      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__14"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__14 = _genCompid2[0],
          $compid__14 = _genCompid2[1];

      var anonymousState__temp = this.__props.refresherConfig || {};
      _index.propsManager.set({
        "onRefresh": this.__props.onRefresh,
        "onScrollToLower": this.__props.onLoadMore,
        "onScroll": this.__props.onScroll,
        "onScrollEnd": this.__props.onScrollEnd,
        "hasMore": this.__props.hasMore,
        "noMore": this.__props.noMore,
        "loadMoreThreshold": this.__props.loadMoreThreshold,
        "noMoreText": this.__props.noMoreText,
        "hasMoreText": this.__props.hasMoreText,
        "refreshStatus": this.__props.refreshStatus,
        "refresherConfig": anonymousState__temp,
        "scrollTop": this.__props.scrollTop
      }, $compid__14, $prevCompid__14);
      Object.assign(this.__state, {
        anonymousState__temp: anonymousState__temp,
        $compid__14: $compid__14
      });
      return this.__state;
    }
  }]);

  return EPage;
}(_index.Component), _class.$$events = [], _class.options = {
  addGlobalClass: true
}, _class.multipleSlots = true, _class.$$componentPath = "E:/jason_mao/svn_repoistory/bochao_JDTaro/tarot_wx/node_modules/eft-cool-taro-ui/weapp/components/EPage/EPage", _temp2);


EPage.propTypes = {
  className: _index4.default.string,
  renderHeader: _index4.default.element,
  renderFooter: _index4.default.element,
  onRefresh: _index4.default.func,
  onLoadMore: _index4.default.func,
  onScroll: _index4.default.func,
  onScrollEnd: _index4.default.func,
  scrollTop: _index4.default.number,
  loadMoreThreshold: _index4.default.number,
  hasMore: _index4.default.bool,
  noMore: _index4.default.bool,
  noMoreText: _index4.default.string,
  hasMoreText: _index4.default.string,
  refreshStatus: _index4.default.number,
  refresherConfig: _index4.default.shape({
    recoverTime: _index4.default.number,
    refreshTime: _index4.default.number,
    threshold: _index4.default.number,
    maxHeight: _index4.default.number,
    height: _index4.default.number
  })
};
exports.default = EPage;

Component(require('../../../../@tarojs/taro-weapp/index.js').default.createComponent(EPage));