"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.routerGoIn = exports.routerGoBack = exports.vibrateShort = exports.throttle = undefined;

var _index = require("../../../@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var throttleTimer = {};
// TODO 多处地方同时调用throttle时，如果第一个传的是ahead = true，另外一个传的是ahead = false, 有可能造成两个的method方法都不会被调用
var throttle = exports.throttle = function throttle(_ref) {
  var method = _ref.method,
      _ref$delay = _ref.delay,
      delay = _ref$delay === undefined ? 300 : _ref$delay,
      _ref$ahead = _ref.ahead,
      ahead = _ref$ahead === undefined ? false : _ref$ahead,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? 'common' : _ref$type;

  // console.log('已阻止频繁触发..........', ahead)
  if (ahead && !throttleTimer[type]) {
    method();
  }
  clearTimeout(throttleTimer[type]);
  throttleTimer[type] = setTimeout(function () {
    if (!ahead) {
      method();
    }
    clearTimeout(throttleTimer[type]);
    throttleTimer[type] = undefined;
  }, delay);
};

var vibrateShort = exports.vibrateShort = function vibrateShort() {
  {
    _index2.default.vibrateShort();
  }
};

var routerGoBack = exports.routerGoBack = function routerGoBack() {};

var routerGoIn = exports.routerGoIn = function routerGoIn(url) {};